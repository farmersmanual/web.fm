
# ____1


generate and test (gt), the farmersmanual publishing hub and channel umbrella. name taken from the tower of [generate and test](https://web.archive.org/web/20230608112522/http://alanwinfield.blogspot.com/2007/04/walterian-creatures.html)


## [self-publishing](https://farmersmanual.bandcamp.com/music) farmersmanual and member projects

## fm subnet and associated [artists](https://farmersmanual.bandcamp.com/artists) constantly creating wild and weird beauty

### [fals.ch](https://f4lsch.bandcamp.com/) revitalized (one of the first online only MP3 labels in the 1990s. back catalog was put on bandcamp in 2018)

## [bruitversum](https://bruitversum.bandcamp.com/) consolidated


# [Internet Archive](https://archive.org/search.php?query=%28farmersmanual%29+AND+mediatype%3A%28audio%29)

![](/i/aa265fe7-aa9e-48a4-ba07-54fd3c0f17aa.png)

## stay tuned for updates - make sure to check these sites for fun and profit

## subchannels - discogs - soundcloud - audiomack

# generate
- [farmersmanual — bandcamp](https://farmersmanual.bandcamp.com/)

# test
- [musicbrainz](https://musicbrainz.org/label/ff3e0710-3c6c-4e2f-82fc-eb2b95f6b66e)
- [discogs](https://www.discogs.com/label/1269106-generate-and-test)

# release data. etc
- [cata.meta.etc+](/generate-and-test/gt_xxx) 



```
#slonkocytes #dynatropes #boundarypushers #chronicoverperformers
```
