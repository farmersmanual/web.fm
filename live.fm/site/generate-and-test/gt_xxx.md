
| ________/  | **artist** | **title** | format | cat# | date  |
|---  |---  |---  |---  |---  |--- |
| ![]() | Farmers Manual | [sorted](https://farmersmanual.bandcamp.com/album/sorted-2)  |(143xFile, FLAC, Album) | gt101 | 2023 | 
| ![](https://archive.org/download/mbid-39c1ddf7-1ec5-4522-9a5a-9c0f852aa42b/mbid-39c1ddf7-1ec5-4522-9a5a-9c0f852aa42b-35749861130_thumb250.jpg) | Farmers Manual - fmoto ‎| (4xFile, MP3, 320) | gt0 | 2017 |
| ![]() | Epy | Schall Und Rauch | ‎(File, FLAC, Album, RE) | gt1 | 2019 |
| ![](https://archive.org/download/mbid-af81ba83-b195-4b60-845f-5b1f1a225c41/mbid-af81ba83-b195-4b60-845f-5b1f1a225c41-36478557752_thumb250.jpg) | TSX | recur | ‎(4xFile, FLAC) | gt-1 | 2016 |
| ![]() | Farmers Manual | fsck less | (Album) gt2 | 2018 |
| ![]() | inaud1bl3 | Qian | ‎(13xFile, FLAC, Album) | gt-2 | 2021 |
| ![]() | dprmkr | Seven Long Minutes |  ‎(8xFile, WAV, MiniAlbum) | gt3 | 2015 |
| ![]() | Pxp | while(p){print while(p){print"."," "x$p++} | ‎(File, FLAC, Album, RE) | gt4 | 2018 |
| ![]() | Calimba De Luna | Lastout |  ‎(File, FLAC, Album) | gt5 | 2019 |
| ![]() | Calimba de Luna | Test code for the measurement of airborne noise |  ‎(5xFile, FLAC, EP, RE, RM) | gt6 | 2018 |
| ![]() | haarriss | Matsch Moor |  ‎(File, FLAC, Album) | gt7 | 2019 |
| ![]() | Farmers Manual | No Backup | ‎(File, FLAC, Album, RE) | gt8 | 2018 |
| ![]() | dprmkr | flowers a sun hazy | ‎(10xFile, FLAC, Album) | gt9 | 2020 |
| ![]() | 2o4 | squimble | ‎(File, FLAC, MiniAlbum) | gt10 | 2019 |
| ![]() | Farmers Manual | The Singles |  ‎(27xFile, FLAC, Album, Comp) | gt12 | 2019 |
| ![]() | inaud1bl3 | weltformel |  ‎(13xFile, FLAC, Album) | gt13 | Unknown |
| ![]() | Epy | Autoepy |  (Album) | gt16 | 2019 |
| ![]() | Farmers Manual | fm |  ‎(10xFile, FLAC, RE) | gt17 | 2018 |
| ![]() | Farmers Manual | does not compute still | ‎(3xFile, FLAC) | gt18 | 2019 |
| ![]() | Farmers Manual | does not compute | ‎(3xFile, FLAC, EP, RE) | gt21 | 2019 |
| ![]() | Farmers Manual | rla 1996 ultraschall | ‎(3xFile, FLAC, Album) | gt22 | 2019 |
| ![]() | Epy | Trepy | ‎(8xFile, ALAC, Album) | gt23 | 2019 |
| ![]() | Pxp | nada |  ‎(13xFile, FLAC, Album, RE) | gt24 | 2019 |
| ![]() | Farmers Manual | Szwong |  ‎(File, MP3, Single, 320) | gt25 | 2017 |
| ![](https://archive.org/download/mbid-fac5a3ea-16cf-493d-8fe0-8bdbfa20f313/mbid-fac5a3ea-16cf-493d-8fe0-8bdbfa20f313-36149875794_thumb250.jpg) | Farmers Manual | glague_general_gen (UNSUPERVISED_LEARNING_AND_REFINEMENT_OF_RHYTHMIC_PATTERNS) part_1 | ‎(File, MP3, 320) | gt26 | 2016 |
| ![]() | TSX, Sue Tompkins | recur³ |  ‎(5xFile, FLAC, EP) | gt27 | 2019 |
| ![]() | Plazma (12) | nervechords | ‎(7xFile, FLAC, Album, RE) | gt28 | 2020 |
| ![]() | Farmers Manual | proto fsck | ‎(8xFile, FLAC, MiniAlbum) | gt30 | 2020 |
| ![]() | Farmers Manual | no residual |  ‎(5xFile, FLAC, EP) | gt31 | 2020 |
| ![]() | Pxp | neba |  ‎(6xFile, FLAC, EP) | gt32 | 2019 |
| ![]() | Pxp | tats​=​3D94chliche​-​chaos | ‎(9xFile, FLAC, Album) | gt40 | 2020 |
| ![]() | TSX | recur² |(9xFile, FLAC, Album) | gt41 | 2020 |
| ![]() | inaud1bl3 | om und unten | ‎(13xFile, FLAC, Album) | gt42 | 2019 |
| ![]() | V93r | YAFR​-​AutoEdit | ‎(4xFile, FLAC, MiniAlbum) | gt43 | 2019 |
| ![]() | General Magic | The Bratislava File |  ‎(File, FLAC, Album) | gt45 | 2020 |
| ![]() | inaud1bl3 | doppelsterne E​.​P. |  ‎(6xFile, FLAC, Album) | gt46 | 2020 |
| ![]() | V93r | Down Deep South | ‎(File, FLAC) | gt47 | 2019 |
| ![]() | Matthew Thomas | Nostalgia​:Dystopia |  ‎(4xFile, FLAC) | gt49 | 2020 |
| ![]() | inaud1bl3 | Zahlen Spiele E.P. | ‎(6xFile, FLAC) | gt54 | 2019 |
| ![]() | TSX X Psygod | ʇlǝɯs | (4xFile, FLAC) | gt55 | 2020 |
| ![]() | V93r | Schlirtp | ‎(File, FLAC) | gt59 | 2018 |
| ![]() | inaud1bl3 | das universum | (13xFile, FLAC, Album) | gt61 | 2020 |
| ![]() | Epy | epydemic | ‎(File, FLAC, MP3, Album) | gt71 | 2021 |
| ![](https://archive.org/download/mbid-bac3cd7b-ad6e-40f5-9d29-b2e7cf8c8b14/mbid-bac3cd7b-ad6e-40f5-9d29-b2e7cf8c8b14-35040388007_thumb250.jpg) | Farmers Manual | [;;;;//;;;;;;///////;;;;;;;;;;;;;,,.....,,,,,,;////;/////IIIIOOOOI/IIIOOOOOOOOOOOOOOOOIIIIIIOIII///;;;;;;////;;;;;;;;;;;;;;;;,,,,,,,,,;/////;;;;;;;,,;;;,,,,,;;;;;;,,,;;;;;;;;;;;;;;;;;,,;;;;;;;;;;;;;;;;,,;;;;,,,,,,;;,,,,,,,,,,,,;;,,,,;;,,,,,,,,;;,,;;;;,,,,,](https://farmersmanual.bandcamp.com/album/iiiiooooi-iiiooooooooooooooooiiiiiioiii) | ‎(10xFile, FLAC, Album) | gt73 | 2022 |
| ![]() | Epy | Clang |  ‎(File, WAV, Maxi) | gt81 | 2022 |
| ![]() | General Magic | Softbop | ‎(7xFile, FLAC, EP) | gt123 | 2022 |
| ![]() | TSX | Nachawa | ‎(File, MP3, 317) | none | 2020 |
| ![]() | CD_slopper | Pfanscotcoe | ‎(10xFile, FLAC, Album) | SP37 | 2020

see also [musicbrainz](https://musicbrainz.org/label/ff3e0710-3c6c-4e2f-82fc-eb2b95f6b66e) and [discogs](https://www.discogs.com/label/1269106-generate-and-test)

