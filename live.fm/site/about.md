<post-metadata>
  <post-title>farmersmanual -- slonkocytes -- dynatropes</post-title>
  <post-tags>fm, slonkocytes, dynatropes, boundary pushers, chronic over-performers</post-tags>
</post-metadata>

# farmersmanual -- slonkocytes -- dynatropes


> Thanks for keeping electronic music interesting!
>
> <cite>[blitstikler, private communication, 2023]</cite>

![wonkslonker](/i/fm-signal-2023-03-30-210025_002.png)

> farmersmanual is a self-generating memeplex lazily self-organised
> around the enterprise of doped-out electronics. Since 1996 it has been
> evolving as a subsurface autopoetic process with erratic overt
> activity consistently transmitting a unique quality of pathology and
> beauty skimming the bow-wave of mainstream aesthetics.
>
> <cite>\[[bandcamp](https://web.archive.org/web/20230611055136/https://farmersmanual.bandcamp.com/)\]</cite>


> welcome to the universe of farmersmanual. we love universes that
> doesn't fall apart two days later. est. 95, applied variantology, no
> backup, exploders we. ㎙᪲


![fm at berlin atonal 2016](https://web.archive.org/web/20230611055136im_/https://live.fm/wp-content/uploads/2022/04/fm_berlin_atonal_14124454_1209322852422448_5354162325464504889_o.jpg)
<caption>fm at berlin atonal 2016</caption>

## :::

> farmersmanual.
> 1990年代の活動開始以来、電子音楽のカッティングエッジな美学を体現してきたアート・コレクティブ。そのパンクなリリース群は、音楽の可能性を拡張し、歴史を変えた。既成概念をハックし続けるアティチュードは、現在に至るまで、多くのアーティストに強大な影響を与え続けている。
>
> farmersmanual. Since its inception in the 1990s, the art collective
> has embodied the cutting edge aesthetics of electronic music. Its punk
> releases have expanded the possibilities of music and changed history.
> Their attitude of continuously hacking away at preconceived notions
> has continued to exert a powerful influence on many artists to this
> day.
>
> Bauernhandbuch. Seit seiner Gründung in den 1990er Jahren verkörpert
> das Kunstkollektiv die innovative Ästhetik der elektronischen Musik.
> Seine Punk-Veröffentlichungen haben die Möglichkeiten der Musik
> erweitert und die Geschichte verändert. Die Haltung, die immer wieder
> auf vorgefasste Meinungen einhackt, übt bis heute einen starken
> Einfluss auf viele Künstler aus.
>
> <cite>[[Tweet by tokinogake](https://web.archive.org/web/20230611055136/https://twitter.com/tokinogake/status/1392079603968225282)\]</cite>


## :::

![brunel shaft durational](https://web.archive.org/web/20230611055136im_/https://live.fm/wp-content/uploads/2021/08/fm-brunel-cafeoto-Screenshot-from-2021-08-08-16-52-18.png)

<caption>Brunel shaft durational 2018</caption> 

> ... one of the most important and radical electronic and visual arts
> groups in Europe ...
>
> <cite>[[cafe oto](https://web.archive.org/web/20230611055136/https://www.cafeoto.co.uk/archive/2018/07/18/farmers-manual-strategies-exploration-and-mapping-/)]</cite>


> Farmers Manual is an electronic music and visual art group, founded in Vienna in the beginning of the 1990s.
>
> <cite>\[...\]</cite>


> Farmers Manual were successfully crossing the boundaries between electronic music, live visuals, experimental graphics, and web design for Zeta Industries.
>
> <cite>\[[Wikipedia](https://web.archive.org/web/20230611055136/https://en.wikipedia.org/wiki/Farmers_Manual)\]</cite>


> Thus far Farmers Manual has been used as a group name, track title, album title in FM, and label generate and test.
> 
> <cite> \[[discogs](https://web.archive.org/web/20230611055136/https://www.discogs.com/artist/356-Farmers-Manual)\]</cite>

> Totally Fantastic. Intruiging stasis -- within textures of sound. Beatsmashing techno -- evolvement. And love -- for the surroundings!
>
> <cite>\[RadarBlue August 26, 2002, Comment on discogs\]</cite>


> Austrian electronic experimentalists Farmer Manual comprise a casual
> collective of musicians, DJs, computer geeks, and net freaks who count
> music as only one of a number of ongoing projects. The group's
> identity is as mysterious as the periodic transmissions from their
> Vienna studios (which began appearing in 1996), but their releases
> have managed to capture an international audience for their strident
> bird-flip to dance-based electronic music convention. Released by
> labels such as Mego, Ash, and Ash sublabel Tray, titles such as "FM,"
> "fsck," and No Backup (a CD+ title and Mego's first full-length
> production) have pushed the envelope of musical intelligibility well
> off the map, spraying burbling, obverse rhythmic structures with
> blasts of noise and distortion, closely paralleling the approach of
> [Panasonic](https://web.archive.org/web/20230611055136/spotify:artist:7qk18OMPkaekOJt2QUva9G)
> or more recent
> [Autechre](https://web.archive.org/web/20230611055136/spotify:artist:6WH1V41LwGDGmlPUhSZLHO).
> The group's full-length debut, No Backup, is a collection of drawn-out
> discombobulations of standard rhythmic and melodic structures, while
> the 12-inch only "FM" (meant as the vinyl alternative to the CD, but
> featuring none of the same tracks) contains more than a dozen brief
> sketches of minimal electro, many ending in lock-grooves (the tracks'
> bare repetition tend to beg the question of why they bothered with
> anything beyond a lock in the first place, but...). The group have
> also dabbled in drum'n'bass-style rhythmic programming (most
> prominently on their Tray 12-inch "Fsck") and have conducted a number
> of live Internet broadcasts via their handsome website (at
> [http://www.farmersmanual.co.at](https://www.farmersmanual.co.at)).
>
> <cite>[Sean Cooper, Rovi]</cite>


> Farmersmanual is a distinguished, pan-European, multisensory
> disturbance conglomerate. musical and technological instruments for
> improvisation, network visualization and sonification. performances
> and remote collaboration and forms of documentation that prolong the
> idea of openness and reinterpretation. continuously expanding. TOTAL
> AUTOMATION vs. HUMAN INTERACTION. seeking to shift the local
> atmosphere from dissolution and clumsiness through manual change and
> ecstatic fiddling into an imaginative state of complex monotony,
> structured calm and chill, or endless associative babbling. so that
> towards the end the authors become the audience and the audience can
> be confronted with a stage populated by machines infinitely rendering
> a stream of slight semantic madness. with the help of extreme
> frequencies and distorted, flickering images. 3. Extracting a shadow
> from the skeleton of network flow. A layer of technological reality
> usually hidden becomes accessible through mediation into sound and
> visual flicker. Erratically shifting from chittering machines into the
> human realm. Replay in different Farmersmanual. multisensory
> disturbance communication, electromagnetism, nuclear fusion and
> ecstatic ification, tools for near instantaneous endless possible
> through local atmospace delay. It is thetic and politich cant global
> Scalishifting from chittext? the cause of all polycause. towards the
> echo of the hypopulated only which move away from frequencies and
> stage created at the end. the title for the fractures. linear shifts
> of the localing wave.
>
> <cite>[Biography for Waves exhibition 2006]</cite>

```
=================================================
  FARMERSMANUAL [AT] / FRI 26 / 7.30pm - Late 
=================================================
```

> Farmersmanual is a pan-European, multisensory disturbance conglomerate
> creating new instruments for improvisation, networked performances and
> remote collaboration, developing ways of recording that encourage
> openness and reinterpretation.
>
> Farmersmanual have created a series of performances and installations
> since 1996, continuously expanding their performance and publishing
> practice from music concerts to interdisciplinary cultural, aesthetic
> and political experiments, which move away from static representations
> of art and culture, towards environments where the public becomes an
> essential part of the work. Their latest experiments focus on
> integrating virtual and physical space to create synaesthetical
> objects, presenting an interconnected, holistic view of the world by
> layering sensual experiences.
>
> Extracting a shadow from the skeleton of network flow. A layer of
> technological reality usually hidden becomes accessible through
> mediation into sound and visual flicker. Erratically shifting from
> chittering machines into the human realm. Replay in different
> timescales. Different times. Different speeds. Useless information ­
> still unpredictable.
>
> During Ultrasound, Farmersmanual will produce a networked concert
> between Huddersfield and elsewhere.
>
><cite>[Ultrasound Festival 2004](https://rhizome.org/community/31938/)</cite>


# constituted 94 -- vienna

**members:** hog smith, yuri per nesusipratima, smet van hoek plus occasional temporary external virtual contributors

:::

![1996](https://web.archive.org/web/20230611055136im_/https://live.fm/wp-content/uploads/2022/05/fm_hog_smet_yuri.jpg)
<caption>Hog, Smet, and Yuri 1996</caption>

:::

![2020](https://web.archive.org/web/20230711163603im_/https://live.fm/wp-content/uploads/2021/10/hog-smet-yuri-679.jpg)
<caption>Hog, Smet, and Yuri 2020</caption>


:::

#  homebase: badlands

> "Formed in Vienna in the mid-1990's, the farmersmanual collective
represents media art at its most anarchistic. While being best-known for
their recordings, in recent years the group have shifted their emphasis
towards extensive live performances, in which imaginative computer
animation, synchronized with 'chaos-particle-accelerating' music, and
the overall ambience of uncontrollable technology form a seamless
whole." 
>
><cite>(anton nikkilä,
[http://www.avantofestival.com](https://web.archive.org/web/20230611055136/http://www.avantofestival.com/))</cite>

> "there were the cacophonous bleeps and burps from Austrian group
farmersmanual's "ship of fools" sailing the canals." 
>
><cite>(TIME mag.,07/2001)</cite>

> with the aid of electronic computers, the composer becomes a sort of pilot: pressing buttons, introducing coordinates, and supervising the controls of a cosmic vessel sailing in the space of sound, across sonic constellations and galaxies that could formerly be glimpsed only in a distant dream. 
>
><cite>(Iannis Xenakis 1971)</cite>

# Manuela Framer

![Manuela](https://web.archive.org/web/20230711163603im_/https://live.fm/wp-content/uploads/2022/05/manuela.gif)
<caption>Manuela Farmers</caption>

:::

This is Manuela, another identity rewrite emerging from pure wordplay on the project name. The precise origin of the picture is unknown. Reverse image search did not yield a result. If you are that person, please be in touch. Chat with Manu on [Facebook](https://web.archive.org/web/20230611055136/https://www.facebook.com/manuela.farmers)


# Three gas cartridges (MEGO 017)

!["Three gas cartridges"](https://web.archive.org/web/20230711163603im_/https://live.fm/wp-content/uploads/2022/05/fm-presspict-gas-cartridges-crop-1.jpg) 
<caption>fm press picture for fm MEGO 017</caption>

:::

A picture of three gas cartridges of different shape and color shade. The first press foto going along with the MEGO 017 [fm](https://web.archive.org/web/20230611055136/https://farmersmanual.bandcamp.com/album/fm) album release.

![fm](https://web.archive.org/web/20230711163603im_/https://live.fm/wp-content/uploads/2022/05/fm-presspict-gas-cartridges-crop-2.jpg)
<caption>fm press picture for fm MEGO 017 (reverse)</caption>

