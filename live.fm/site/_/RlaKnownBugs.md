<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\> **RlaKnownBugs**** \${
[**Edit**](/_/RlaKnownBugs?t=1469037740)
}}+-C67

there are some issues with certain files that won\'t play on itunes /
mac but play fine on other decoders.

here is the list of affected files:

-   19951013_wien,phonotaktik
-   19960401_wien,flex - all three dont work
-   19960403_wien,flex-cafe - both dont work
-   19970500_germany,mego-tour-01 - Frankfurt_7.5.96.mp3,
    Mannheim_HD800_6.5.96.mp3
-   19970700_wien,riesenrad
-   19980000_F5K - Hamburg_fsk\_#1.mp3, Hamburg_fsk\_#3.mp3,
    Hamburg_fsk\_#4.mp3, Hamburg_fsk\_#5.mp3
-   19980619_barcelona,sonar
-   19981009_hull,time.based.arts
-   19981107_hamburg,fundbuero

(reported by caleb)

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Known%20*Bugs%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaKnownBugs) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaKnownBugs?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaKnownBugs)


