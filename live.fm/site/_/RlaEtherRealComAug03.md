<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaEtherRealComAug03**** \${
[**Edit**](/_/RlaEtherRealComAug03?t=1469037837)
}}+-C67

Farmers Manual -
[RLA](/_/RLA) - (Mego /
Chronowax)

[RLA](/_/RLA) n\'est pas un
disque tout à fait comme les autres. Son emballage ne trompe d\'ailleurs
pas puisque le disque prend place dans un boîtier DVD, mais mettre le
disque dans sa platine de salon ne va pas donner grand chose. Tout juste
une petite vidéo d\'une quinzaine de minute pendant laquelle les membres
du groupe défoncent une cloison et découvrent une nouvelle pièce à leur
studio, après quoi il se reposent et caressent un chat. Etrange\... Mais
c\'est dans le lecteur DVD de votre ordinateur que ce disque dévoilera
tous ses secrets puisque sont compilés l\'intégralité des concerts du
groupe entre 1995 et mai 2002. Ce DVD étant édité sous licence Copyleft,
son contenu est intégralement téléchargeable sur le site du groupe, mais
il faudra être patient pour arriver au bout des 7Go de MP3, photos de
concerts et scans de tickets des dits concerts, vidéos, interviews et
bien sur la suite de ces
[RLA](/_/RLA) (Recent Live
Archive), soit les concerts qui ont eu lieu depuis le mastering du DVD
et ceux à venir.

Pour ceux qui ont déjà vu Farmers Manual en concert, ce sera l\'occasion
de revivre ceux-ci avec notamment la première soirée Büro en mai 1998 au
Garage, et au Centre Pompidou deux ans plus tard. Mais ce genre
d\'archive est aussi l\'outil idéal pour apprécier d\'un coup de clic
l\'évolution d\'un groupe. Expérimental mais encore très facile
d\'écoute à Salzbourg en 1996 avec un drone en guise d\'introduction,
puis une rythmique répétitive faite de cymbales et de clics avant que
des bruits viennent s\'y glisser, des drones saturés, puis un passage
presque dansant, le groupe s\'amuse avec les codes de la \"dance
music\". En 1997 le groupe semble avoir trouvé sa voie dans
l\'abstraction et le collage de micro boucles de bruits ou de courtes
mélodies, mais proposera toujours quelque chose de différent, atypique
comme les 5 heures de concerts à Stockholm pour une performance
intitulée Nature is Perverse. Chacun parcourra ce DVD comme bon lui
semble, dans l\'ordre chronologique ou en choisissant une ville au
hasard. Quel genre de concert ont-ils bien pu faire à Chicago en 2001 ?
Etait-il très différent de celui qu\'ils ont donné à Sydney quelques
mois plus tôt ? La réponse est oui, et les deux étaient très très bien.
Et en bonus vous pourrez même écouter l\'intégralité du set de Pan Sonic
qui jouait le même soir. En effet, parfois, en guise de bonus, on trouve
d\'autres artistes qui partageaient l\'affiche des Farmers Manual comme
CD_Slopper, Discom et Voicecrack ou des curiosités comme
l\'enregistrement audio d\'une course de Formule 1. On vous laisse
chercher dans quel(s) concert(s) ils ont bien pu utiliser ces bruits de
moteurs.

Ce n\'est là qu\'une infime partie du contenu de ce DVD qu\'il vous
faudra près de 4 jours non stop pour en venir à bout. En dehors d\'un
fabuleux document, c\'est aussi une fantastique banque de sons puisque
les auditeurs sont invités à utiliser et modifier le contenu de ce DVD
dans les limites décrites par la licence Copyleft.

Fabrice Allard

[http://www.etherreal.com/magazine/disques/?file=farmersmanual_rla](http://www.etherreal.com/magazine/disques/?file=farmersmanual_rla) 

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Ether%20*Real%20*Com%20*Aug%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaEtherRealComAug03)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaEtherRealComAug03?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaEtherRealComAug03)


