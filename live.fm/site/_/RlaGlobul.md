<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\> **RlaGlobul**** \${
[**Edit**](/_/RlaGlobul?t=1469037737)
}}+-C67

### []{#demangled_and_retroglobulated} demangled and retroglobulated

sparingly dense view of the interconnections/extensions between the
dataclumps on the
[RecentLiveArchive](/_/RecentLiveArchive).
each node is an individual file (.ogg/.mp3/.jpg/.txt/.etc) each edge
shows a link between the files. blurred and condensed from a distance.
easy to comprehend in a suggestion of an overview or just another
[MetaGulz[^?^](/_/MetaGulz?topicparent=Fmext.RlaGlobul)]{.twikiNewLink
style="background : #5f2e20;"}.\_??

images

-   [less dense](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_log-100.jpg)
    (315k)
-   [more dense](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_glob.gif)
    (97k)
-   [details](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_glob1.gif)
    (165k)
-   [details](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_glob2.gif)
    (154k)

posters (a0 size, .ai files)

-   [6pt helvetica
    version](/i/RlaGlobul/rla_log_a0-type.ai.sit)
    (3175k)
-   [sub pixel text
    version](/i/RlaGlobul/rla_log_a0.ai.sit)
    (3220k)

![rla_glob.gif](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_glob.gif)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Globul%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaGlobul) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaGlobul?template=oopsmore&param1=1.6&param2=1.6)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaGlobul)


