<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaBrainWashedAug03**** \${
[**Edit**](/_/RlaBrainWashedAug03?t=1469037837)
}}+-C67

[http://www.brainwashed.com/brain/brainv06i35.html](http://www.brainwashed.com/brain/brainv06i35.html) 

Farmers Manual, \"RLA\" MEGO

As principal early players in the 90\'s computer music scene,
fetishising digital audio and austere CGI graphics with the best of
them, it\'s fitting that Farmers Manual should be behind
[RLA](/_/RLA), the latest
and most uncompromising expression so far of the Merzbox mentality.
Technically, [RLA](/_/RLA)
is a dual layer DVD9, NTSC, region-free DVD with a running time of three
days, 21 hours and 38 seconds. It\'s as exhaustive as possible a
collection of everything to do with Farmers Manual\'s live shows since
1995: nearly four days of MP3s (from \"several artists, but mostly FM
and related folks\") along with accompanying JPEGs of the shows, their
locations, gig flyers, stage passes, press reviews, mailing list
write-ups, and even a few movie files. The icing on the cake is that,
inserted in a computer\'s DVD drive, it auto-plays some baffling and
unexplained handheld camcorder footage of FM knocking a wall down in a
basement studio, then later hanging around in an apartment with a cat.

The MP3s on [RLA](/_/RLA)
(the abbreviation stands for \"recent live archive\") were created from
whatever sources FM could find, so the sound quality varies, and some
recordings are incomplete. But this is part of the collection\'s charm,
and with around 150 MP3s on the disc, you can forgive the occasional
imperfection.

With FM being a largely improvisational group,
[RLA](/_/RLA) doesn\'t
feature the same music again and again: each collaboratively improvised
performance is sonically and to some extent structurally unique. Though
their sound has much in common with the noise-influenced digital crunch
producers who followed them, their source material and influences are
often broader. Their music is more straight-faced surrealism than
aleatoric(¹) violence.

It\'s impossible to review this release without fixating on the format,
but in the packaging-obssessed world of MEGO, that doesn\'t seem
inappropriate. Obviously, being a DVD, and a data DVD at that,
[RLA](/_/RLA) is only usable
if you have a computer DVD drive, and not a domestic DVD player. But the
entire project is available for free download from FM\'s web site, and
new recordings have been added to the site over time (currently around
10 more hours\' worth). And if you don\'t have a decent net connection,
or any at all (in which case where are you reading this?), you can
legally obtain the music under a \"copyleft\"-style license, meaning
that at least in principle you can ask someone to make you conventional
data CDs of the approximately 8GB of data here. I suppose you\'d have to
ask them very nicely.

[RLA](/_/RLA) is more of an
ongoing project than a single artefact. It\'s an essential resource for
existing fans, and as good a place as any for newcomers to learn about
this most unique of laptop supergroups. I still haven\'t finished
exploring it. - Andrew Shires

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Brain%20*Washed%20*Aug%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaBrainWashedAug03) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaBrainWashedAug03?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaBrainWashedAug03)


