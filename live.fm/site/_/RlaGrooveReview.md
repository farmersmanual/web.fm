<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaGrooveReview**** \${
[**Edit**](/_/RlaGrooveReview?t=1469037741)
}}+-C67

#### []{#20030522_mego777_Groove_preview} 20030522: mego777 - Groove preview

Farmers Manual

RFA

Mego/ A-Musik/ Hausmusik

Auf dieser DVD des Mego-Acts Farmers Manual sind neben einem Video
dreißig Konzerte als mp3 enthalten, die die Gruppe zwischen 1995 und
2002 gegeben hat: 47 Stunden, 58 Minuten und 59 Sekunden Musik.
Gleichzeitig wird der Anlass und Ort jedes Konzertes dokumentiert, in
manchen Foldern findet man Fotos immer gleich und doch immer anders
aussehender Konzert-Setups oder die komplette Website der jeweiligen
Veranstaltung einschließlich php-Scripts. Dazu gibt es Anmerkungen wie
„Eine kalte und dunkle November-Nacht in Stockholm, uns wird während des
Konzerts Pizza gebracht." „RFA" hat natürlich etwas Gigantomanisches,
deshalb ist es erstaunlich wie integrativ und anschlussfähig die Musik
von Farmers Manual wirkt. Die Tracks bestehen immer aus komplett
abstrakten Sounds, beeindrucken schon durch ihre Begrenztheit,
produzieren aber immer einen Groove, der als minimales Club-Feeling
funktionieren kann, aber auch als die Raum-Installation, als die Sets
etwa im Wiener Riesenrad oder auf Venezianischen Kanälen stattgefunden
haben.

Ist es nicht gewagt das Geheimnis der vergangenen Konzerte komplett
offen zu legen und Bewertungen a la „Farmers Manual klingen immer
gleich." oder „Jene Minuten 1997 in Amsterdam waren ihre besten."
herauszufordern? Am Schluss werden jedenfalls die sieben Konzerte in
Berlin, Linz und Tokyo genannt, von denen keine Aufnahmen existieren.

AW

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Groove%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaGrooveReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaGrooveReview?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaGrooveReview)


