

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwDebugReview2**** \${
[**Edit**](/_/EwDebugReview2?t=1469037844)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Rviewed by: DE:BUG , pita\
Date: \~ 1998\
Additional Link:
[http://www.de-bug.de/](http://www.de-bug.de/) \

------------------------------------------------------------------------

Nicht, daß das jetzt besonders wichtig wäre, aber man sieht natürlich
nur das, was die Augen sehen möchten. Einmal mehr krachte eine dieser
silbrigen Scheiben von einer höheren, geheiligten Ebene mit einer
Bruchlandung auf den Tisch, so dass die ohnehin klapprige Verpackung
restlos zerbrach und zum Vorschein kam nicht nur eine, sondern gleich
zwei dieser verdammten Teile. Wie viele auf dem Holzboden festgenagelte
Festplatten braucht man eigentlich heutzutage um beachtet zu werden?

OK, fangen wir an zu wirbeln, schlagen wir los oder wie auch immer man
sagen mag. Wann ist das Ziel erreicht, bzw. um akkurater zu sein: Hat es
überhaupt schon begonnen?

\"C\'est ne pas fin\" behauptet er müde während er sich die Vorgänge
unten in der abgedunkelten Kammer ansieht. Sollte man jedoch die sich
fortwährend verändernden Level der Interaktion einmal leid werden, gibt
es ja immer noch den guten, staubigen Shuffle-Knopf. Man teile es durch
60, multipliziere es noch einmal mit wieder anderen 24 Einheiten und
sammele so lange bis die Zahl 1000 erreicht ist um noch einen Bonus
draufzubekommen.

Eine der Silberplatten lebt und die andere ist tot. Man hat immer
irgendeine Wahl. Zum einen gibt es den mit Computerinterventionen
versehenen three-in, 6 Track live-Jam mit von menschlichen Wesen
herbeigeführten Parameteränderungen, aber man kann sich auch für die
computergenerierte null-in, 2 Track Zufallsversion ohne menschliches
Dazutun entscheiden. Das führt natürlich unweigerlich zur 6 Millionen
Dollar-Frage, ob sie dieses Boot wirklich unter der Brücke durch
bekommen haben und nun nach darker Passage im Hafen der industriellen
Abenteuer gelandet sind. Man muss es einfach gesehen haben um es zu
glauben, selbst wenn der Kapitän sich am Rauchen von Zigarren und dem
Trinken von obskuren, an der Bar ohne Wiederkehr gemixten Cocktails
stört.

Um die Ecke wird in einer Gruppe von Denkern und Möbelpackern
diskutiert, ob das kommende Margareten Sinewave Massacre einen Einfluss
auf den weltweiten Telefonkartenmarkt haben wird. Als wäre es nicht
genug, dass schon die vorigen zwei Inkarnationen so viele Leute
verärgert wie zufriedengestellt haben. Was aber immer ein gutes Zeichen
ist, wie man sagen muss.

Jegliches Tanzen sowie das Auf- und Ablaufen entlang der Gänge ist nicht
erlaubt. Sollten Sie jemand dabei erwischen, notieren Sie bitte Namen,
Adresse und Telefonnummer. Boom, bumpf, umpfa umpfa ad infinitum.

The \"start\" function is called automatically (like \-- \"main\" in C
programs); it usually runs the \-- instrument. In this case, it is the
instrument. var osc, outval; \-- Declare variable names (with no type
information) \-- for the oscillator object and output sample buffer. osc
= Asinosc(220, 0); \-- Create a 220 Hz sine oscillator object. \--
Asinosc()\'s arguments are (frequency, phase). \-- The oscillator object
is held in the variable \"osc.\"

\-- Now create a continuous loop to play the oscillator. dspAdd({ \--
Start the signal processing loop. outval = value(osc); \-- Get osc\'s
value (a buffer full of samples). out(outval, L); \-- Send it to the
left output. out(outval, R); \-- Send it to the right output. }); \--
End the DSP loop. } \-- That\'s it! (End the start function, and the
program.)

Das allerwichtigste bei dem ganzen ist die Tatsache, dass sie all das
mit ihren blossen Händen vollbracht haben.

Explorers_We. Squish Four. Jetzt draussen auf or. FM rule.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Debug%20*Review%20*2%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwDebugReview2) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwDebugReview2?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwDebugReview2)


