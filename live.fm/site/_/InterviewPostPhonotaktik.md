<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **InterviewPostPhonotaktik**** \${
[**Edit**](/_/InterviewPostPhonotaktik?t=1469037759)
}}+-C67

Im Rahmen des Wiener Elektronik-Musik-Festivals \"Phonotaktik\" gaben
Farmers Manual vor drei Jahren ihr Live-Debut: Sythetische Sounds,
elektronische Loops mit verqueren Anleihen aus dem Elektro- und
Ambientbereich mündeten auf ihrer CD \"Fsck\" in einem eigenwilligen
Drum´n Bass.

Wenn in ehemaligen Motorradwerkstände im 5.Wiener Gemeindebezirk die
Geräusche ihrer Macintoshs zusammenlaufen, merkt man, daß ihre Musik auf
keinem Dancefloor dieser Welt zu Hause möchte. Seitdem Oswald Berthold,
Gert Brantner und Mathias Gmachl ihre ersten Demotapes an \"Mego\"
geschickt haben, sind sie fixer Bestandteil im Künstler-Pool des Wiener
New Electronic-Labels. Ihre Releases \"FM\" und \"No Backup\",
CD-Rom-Track inklusive, sind mittlerweile vergriffen. Auch international
nimmt das Interesse an den Multimedia-Formation ständig zu. Vor kurzem
tourten sie in England mit DJ Martin Gore von Depeche Mode im
Vorprogramm, der Farmers Manual dem renommierten englischen Label Mute
wärmstens empfohlen haben soll. Die eigens gestaltete Homepage von
Farmers Manual folgt mit ihrem Web-Design dem Grundsatz ihrer
Musikproduktion - \"Subject it all to test\".

**Die Musik von Farmers Manual entsteht fast auschließlich am Computer,
d.h. die Sounds werden digital erzeugt und gesichert. Anschließend wird
das file auf CD gebrannt und ein neuer FM-release ist entstanden.**

Wir nutzen den Computer, um möglichst viel Output nach selbstdefinierten
Regeln zu produzieren. Beim Auswahlprozeß analysieren wir die eigenen
Entscheidungen, um in weiterer Folge den Computer seine eigenen
Entscheidungen ausführen zu lassen. Datenbanken und Computerprogramme
starten die Musik, ohne daß wir vorher Soundsamples hin- und herschieben
oder editieren. Wenn wir mit dem Output nicht zufrieden sind, verfeinern
wir einfach die Regeln. Die Basis unseres Produktionsprozesses aber
bleibt der Automationsgedanke.

**Farmers Manual ist derzeit auch ein international gefragter Live-Act.
Ist der Transfer von Studioequipment nicht extrem aufwendig?**

Früher haben wir uns noch mit großen Computern, Synthesizer, Mischpult
und Effektgeräten abgemüht. Bis wir draufgekommen sind, daß das Zeug nur
schwer und alt ist, einfach Jurassic Technology. Derzeit reisen wir mit
nur drei Notebooks im Gepäck. Seit ungefähr zwei Jahren ist es möglich,
sich nur mit einer mouse und einem keyboard am Computer ein Instrument
zu bauen. Wenn wir live spielen, beeinflußen wir den musikalischen
Erzeugungsprozeß händisch, manchmal aber auch gar nicht. Wir starten
dann nur unsere Laptops und lassen sie für uns arbeiten. Theorethisch
könnten wir auch nur die Computer zu einem Konzert schicken.

**Warum spielt Farmers Manual dann noch live?**

Weil wir nur bei Live-Auftritten wirklich zusammen Musik machen. Sonst
arbeitet jedes Mitglied für sich. Erst live treffen dann die einzelnen
Laptops aufeinander. Wir verwenden die selben Programme. Jeder macht
was, hört die anderen und entscheidet daraufhin, wie er reagiert. Oft
weiß der einzelne gar nicht, wofür er im Sound verantwortlich ist. Wenn
man glaubt herausgefunden zu haben, wer man in dem ganzen Ding ist, ist
alles schon wieder ganz anders. Oder man freut sich über einen
gelungenen Sound und kommt drauf, daß die eigene Lautstärke auf Null
gedreht ist. Das sind so die üblichen Sachen. Es gibt keine Absprachen
und alles ist improvisiert. Wir kennen das musikalische Ergebnis
genausowenig, wie die Leute im Publikum.

**Nach welchen Kriterien funktioniert dann soetwas wie Selbstkritik,
wenn man die meisten Entscheidungen dem Computer überläßt?**

Unsere Musik hat keine Referenz. Sie ist nicht richtig oder falsch, weil
wir sie vor dem Auftritt in der jeweiligen Form noch nie gehört haben.
Es geht mehr darum, neues zu entwickeln. Für uns nützt sich alles
schnell ab. Dann kommt eine neue Idee und eine neues Computerprogramm
und die Voraussetzungen ändern sich wieder. Insofern ist Farmers Manual
ständig in Bewegung. Der Umgang mit der Technik ist unsere eigentliche
Inspiration.

Alle FM releases sind auf Eurer Homepage über realaudio zu hören. Auch
der Verkauf der meisten FM-CDs erfolgt über das Mailordersystem M.DOS
von Mego.

Der früher notwendige und heute aufgesetzte Prozeß der Distribution,
Produktion und Verwaltung über einen Plattenkonzern wird durch das Netz
stark verkürzt. Die großen Konzerne versuchen dieses Problem in den
Griff zu bekommen, in dem sie mit Firmen, die im Internet federführend
sind , Exklusivverträge abzuschließen. Sie werden es aber nicht
verhindern können , daß Musiker auf ihrer Seite Vertrieb, Label und
Produktionsfirma in einem sind. Diese direkte Verbindung zum Konsumenten
versuchen wir auszunutzen. Sobald ein Track fertig ist, stellen wir ihn
on-line.

Farmers Manual ist ein Multimedia-Experiment, das seit seinem Bestehen
konsquent weiterentwickelt wird. Musik und Web Design sind Nachbarn auf
dem Computer. Auf www.farmersmanual.co.at findet man unter die Realaudio
live.fm, eine Art Radioshow. Da werden jeden Freitag mehrere Stunden
experimenteller Sound geschaffen, speziell für die Übertragung via
Internet.

Dafür versuchen wir als Host interessante Leute zu finden, um sie dann
zu featuren. Da spielen wir nie unsere eigene Musik. Höchstens dann ,
wenn wir wo live spielen, versuchen wir das Konzert nachzutragen und
gleich von vor Ort ins Netz zu streamen. In Lyon z.B. war unser Auftritt
als reiner Netzevent konzepiert, der live über Realaudio gebroadcastet
wurde. Wir sind auch für live.vbs, der Live-Netz-Übertragung aus dem
Wiener Club Flex, zuständig. Vor dort wird täglich ab 23 Uhr gesendet.

Der Gedanken, den user über den aktuellen Status von Farmers Manual zu
informieren, wird mit eurer Homepage auch auf visueller Ebene verfolgt.
Bis vorkurzem, hattet ihr eine Webcam im Studio installiert, die das was
in Eurem Studio passierte, live ins Internet abbildete.

Die Webcam gibt es zwar noch, aber derzeit ist im Netz ist nur ein
Standbild zu sehen. Wir haben damit aufgehört, weil das zu viele Leute
im Netz mitverfolgt haben, was enorm hohe Kosten verursacht hat. Nach
dem die Webcam drei Monate im Betrieb war, haben wir eine Rechnung über
40.000 Schilling bekommen. Und da wir unser eigener Server sind und nur
die Netzverbindung zur Verfügung gestellt bekommen, mußten wir den von
uns verursachten Internetverkehr selbst begleichen.

Auf eurer Homepage gibt es auch einen Chatroom. Man denkt, da könnte
sich der user nach längerem Herumnavigieren auf der eher kryptisch
gehaltenen FM-Page, endlich Antworten über das Farmers Manual Universum
verschaffen. Aber weit gefehlt.

Den Chatroom wir als eine Art Multitelefonverbindung für die Farmers
Manual Mitglieder und jene, die unserem Umfeld arbeiten. So können wir
untereinander in Verbindung treten und uns gegenseitig weiterhelfen,
wenn gerade einer von uns nicht vor Ort ist. Es gibt auch ein link von
der live.vbs Seite zu unserer Homepage. Von dort kommen immer öfter
Leute, die eigentlich in unser kleines Privatreich vor stossen und sich
über unsere Kommunikation wundern. Der Chat soll nicht die Hilfefunktion
auf unserer Hompage sein. Ehrlich gesagt, schätzen wir den
redaktionellen Teil auf unserer Homepage nicht wahnsinnig. Es ist nicht
unser Ding, über uns selbst dahinzulabbern. Die user müssen sich die
message selbst zusammentragen. Wir wollen niemanden Information
aufdrängen.

**Abgesehen von lokalen Verbindungen zwischen Farmers Manual und den
Wiener New-Electronic-Labels wie Mego, Sabotage, etc., gibt es
internationale Kontakte zu Gleichgesinnten, mit denen ihr Know-How
austauscht?**

Das australische Label „mindflux\`\` sind mit uns vergleichbar. Sie
arbeiten, wie auch wir, hauptsächlich am Computer und machen Musik. Wir
lernen immer mehr Leute über das Netz kennen , aber auch bei
Live-Auftritten. Unlängst haben wir die Mitglieder von „Autechre\`\`
kennengelernt, die über Realaudio-Möglichkeiten im Netz nicht besonders
informiert waren. So weckt man auch Interesse und schafft es, Leute zu
animieren und sich an Projekten zu beteiligen.

**Wo liegt eure zukünftigen Präferenzen im Webdesign?**

Derzeit arbeiten wir an einer neuen Version der FM-Page und daraus
könnte sich ein interressantes Experiment ergeben. Wir werden beginnen
mit Datenbankenlösungen zu arbeiten. D.h. Wir werden einen Pool von
Inhalten festlegen und selbstdefinierte Gesetzmäßigkeiten aufstellen. So
können Textschnippsel, hunderte Images und Sounds auf der Internetseite
vom user neu kombiniert werden. Die Fragestellung könnte lauten: Was
erkennt man als zusammenhängenden schlüssigen Inhalt und wie kann dieser
wieder auseinanderbrechen?

\"Subject it all to test\" ist ein Leitsatz von Farmers Manual. Ihr seid
Teil einer Generation, die mehr den Computer arbeiten läßt und zusehends
weniger selbst in den automatisierten Arbeitsprozeß einzugreifen
möchtet.

Ob im Graphik- oder im Musikbereich liegt die Unterstützung, die der
Computer bei der Arbeit liefert, weit unter seinem Potential. Wenn wir
z.b. eine schwarzweißes Linienmuster für unsere Homepage designen
wollen, fordern wir den Computer auf 10 Images mit Zufallsneigungen
dieser Linie zu errechnen. Dann suchen wir uns die aus, die uns am
bestengefällt. Wenn man nun den Computer beibringen kann, dann noch den
Auswahlprozeß selbst zu machen, ist das ideal und wir können uns in der
Zwischenzeit um andere Dinge kümmern. Wenn man am Computer arbeitet ist
er in meisten Fällen zu 0, 7 % ausgelastet, vielleicht eine Minute mal
zu 50% und die restlichen 24 Stunden wieder zu 0, 7 %. So entsteht eine
große Leerlaufzeit. Aus diesem Grund ist auch die Idee des
RSA-Key-Crackingbewerbs entstanden, wo tausende Rechner im Internet in
ihrer Freitzeit irgendwelche Schlüssel knacken. Anstelle Schlüssel zu
knacken, könnte der Computer auch Zufallsgraphiken, -musik oder -videos
erstellen.

------------------------------------------------------------------------

Interview with: Mathias Gmachl\
Interviewer: ?\
Date: /?/96\

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Interview%20*Post%20*Phonotaktik%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\|
[Attach](http://web.fm/twiki/bin/attach/Fmext/InterviewPostPhonotaktik)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/InterviewPostPhonotaktik?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.InterviewPostPhonotaktik)


