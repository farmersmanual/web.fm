<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


# second brew, same leaves:

as a partially formed world, multisensory disturbance prickles into
musical and technological instruments, slowly synthesized through the
interaction of barely comprehensible processes. oscillating.
tctwciching. chl tr e, as different music becomes sound, identifying
itself as operations on the organization of a subset of such processes,
disregarding boundaries of occlusion. endless improvisations, networked
occlusions in synthetic worlds. performances and remote collaboration
prolong the idea of openness and reinterpretation. continuously
expanding. TOTAL AUTOMATION vs. HUMAN INTERACTION. seeking to shift the
local atmosphere from dissolution and clumsiness through manual change
and ecstatic fiddling into an imaginative state of complex monotony,
structured calm and chill, or endless associative babbling. so that
towards the end the authors become the audience and the audience
stretches to the horizon, populated by machines infinitely rendering a
stream of slight semantic madness. further, extracting a shadow from the
skeleton of network flow. A layer of technological reality usually
hidden becomes accessible through mediation across sound and visual
flicker. Erratically shifting from chittering machines into the human
realm. reply in cojoined different. multisensory disturbance
communication, electromagnetism, nuclear fusion and ecstatic -ification,
tools for near instantaneous endless possible through local atmospace
delay. It is thetic and politich cant? fusion and nuclear decay. It is
the cause of 3-space solvents, of left-right-asymmetry, of anisotropy of
time. defect. all of these phenomena are physical effects which arise at
the transition from logarithmic into linear space. The standing wave in
logarithmic space now allows communication across astronomical distances
practically without time delay. How is this possible?

Revision r1.1 - 04 Jan 2007 - 11:50 -
