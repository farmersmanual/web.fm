<post-metadata>
  <post-title></post-title>
  <post-date>2003-10-18</post-date>
  <post-tags>test, fm</post-tags>
</post-metadata>


# This is a journey into sound...

-   Phuture, 17-18 October 2003, 2200 hrs - 0100 hrs:
-   Reviewed by: Hairul Amar bin Hamed
-   Audio Output Review:
-   [http://www.happening.com.sg/livehtml/features/0311audio.html](http://www.happening.com.sg/livehtml/features/0311audio.html) 

There comes a time when an audience has to shed all notions and
preconceptions of what music is. Time to throw away all societal
conventions on the accepted structure of beats and rhythms. Audio Output
aims to show that the time is now.

Presented as the \"Sound\" event in Spell#7\'s month-long line-up of
activities collectively known as The Year of Living Digitally, It aims
to bring to the forefront the importance of digital technology in the
creation of music today, and how the use of such technology breaks
musical boundaries and explores new frontiers in experiments with sound.

The line-up includes groups from all across the globe, emphasizing the
universal message of music in all its various forms. Groups such as
Farben from Germany and the UK\'s SND champion a funky style of techno,
making their music very accessible to clubbers and casuals alike. The
subtle layers of beats, rhythms and melodies proved irresistible to even
the most die-hard of detractors.

The big crowd favorite was Finnish performer Vladislav Delay. Beginning
his set with a teasing array of sounds, seemingly more interested in
creating soundscapes than pure music. With a deep bass and industrial
sounds, the audience was taken on a aural journey through an urban
landscape, bringing to mind Future Sound of London\'s work circa Dead
Cities (Astralwerks, 1996). The bass was used to especially good effect
in creating depth in the listeners\' surroundings, as it creeps through
the skin and thrums through the bones, imprinting a virtual spatial
concept in the audience\'s mind. As his performance grew, Delay started
to tease the crowd with snippets of vocals here, a little bit of melody
there. With the crowd perked enough (almost to the point of growing
restless), Vladislav Delay switched to the funk house style he plays
under the moniker of Luomo and dropped beats that ignited the
dancefloor. The interesting thing about his Luomo persona is that Delay
doesn\'t sacrifice on experimentation. Instead, it is an avenue for him
to educate the casual clubber with accessible amalgamations of beats,
vocals and melodies. Judging from the massive positive response, it
clearly works.

Other acts such as Miroque from Japan were more experimental. Miroque
played a chilled, blissed-out set, with many influences from her home
country. To a casual listener, the music seems easy and carefree, almost
child-like in some instances. Upon closer inspection, one discovers
little nuances in the sounds, little off-beats and discordant keys that
gives character to her pieces. It brings to mind the kind of music one
would find running around in the head of Delerium (the character from
The Sandman, not the band). Miroque\'s music gives pleasure to the one
who hears it, but rewards greatly one who listens to its message.

The true centerpiece of the Audio Output lineup however, was Austrian
group Farmers Manual. This trio employs a mixture of sound and visual
collage to express their strong, almost anti-establishment messages.
Their performance seemed more suited for a theatre than on the
dancefloor as they took sound by the jugular and made their computers
unleash a dirge. And what an exceptional dirge it was. Their chopped up
sound was superimposed upon the jarring images of an attack helicopter
swooping in for the kill. The image fizzes in and out of focus in tandem
with the noise, giving a sense of impending doom at the hands of
misguided technology. And their transitions are nothing if not a smooth,
mish-mash of sounds and images, spelling out the importance of correct
control over technology. Their performance finally ends in a focused
image of Chinese characters on the screen, coupled with a cacophony of
melodies. It implies how man has been buried under the spell of
technology for too long, lost in its mindless drone and rigid structure.
To prevent the collapse of humanity, man has to clear the debris around
him and take control of technology and mould it to build a better,
brighter future where man and machine is in symbiosis.

Perhaps that is the true message of digital music, and music as a whole.
Man must work with the technology available to him to explore new
frontiers and break new grounds. Only then can harmony be achieved and a
new wave of creativity and ingenuity be formed. Already, Spell#7\'s
initiative has shown us the early signs of this new wave. And judging
from the fairly good response from the regular clubbers, it shows that
the local audience is indeed open to experimentations in sound. Perhaps
this might spur our own local journey of musical creativity. Only time
will tell.

\~ Hairul

Revision r1.1 - 05 Jul 2005 - 13:17 -
