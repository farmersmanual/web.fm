<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaEuropeanFotographyReview**** \${
[**Edit**](/_/RlaEuropeanFotographyReview?t=1469037838)
}}+-C67

European Fotography Review

issue ? publication date ?

OK, so much for books. The other thing, the DVD that I dragged around at
the same time in cars, taxis, busses, trains, and on planes, was
farmersmanual\'s
[RLA](/_/RLA). Now who the
hell are Farmersmanual? I met them for the first time in Amsterdam in
1995. Laptops and non-compromising sounds. Same about the realtime
visuals. They were young. And they stayed young forever. The constant
output in shows and performances remained uncompromising. Despite the
label they are on (Mego is queen in art-sound-land) and despite the
venues they played. Since 1995 people came and went. Farmersmanual moved
along but never got mainstream. Likewise for the DVD: it is not easy to
get into. First it confronts you with some footage of the past: computer
images and a couple of boys smashing up some walls; it proves to be
farmersmanual in a former time-warped incarnation of adolescent honesty
about buildings and living. But then you find out it is actually a
cd-rom at the same time. The disk features a myriad of recordings,
photographs, movies and documents that Farmersmanual have collected over
the small decade of their controversial existence.

Call them last of the 20th century avant-garde or first of the 21st
new-garde. Doesn\'t matter. Just start up the disk and quit the DVD
player program. And find out for yourself. Believe it or not, suddenly I
was overwhelmed and charmed with the way this whole thing had imploded
into the idea of a DVD, that was nothing else but a failure or breakdown
of the format itself. It has been splintered into all the formats we
have known over the last decade, and therefore into all the concepts
that we dragged along culminating into the fake database hype nowadays.
This is the perfect cure to all fake art research and all fake format
sublimation that art is promoting these days. And that makes it the most
refreshing contemporary disk that ever was - up to now. And what is
more, just browse to
[http://rla.web.fm](http://rla.web.fm)  and you will find
out what I am talking about. Don\'t expect the fancy interface, go for
the data, and you will appreciate the interface!

guy van belle

\-- [HiazHhzz](http://web.fm/twiki/bin/view/Fm/HiazHhzz) -
14 Jun 2003

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*European%20*Fotography%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\|
[Attach](http://web.fm/twiki/bin/attach/Fmext/RlaEuropeanFotographyReview)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaEuropeanFotographyReview?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaEuropeanFotographyReview)


