

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****WebPreferences**** \${
[**Edit**](/_/WebPreferences?t=1469037736)
}}+-C67

## []{#TWiki_Fmext_Web_Preferences} TWiki.Fmext Web Preferences

The following settings are ***web preferences*** of the TWiki.Fmext web.
These preferences overwrite the ***site-level preferences*** in
[TWikiPreferences](http://web.fm/twiki/bin/view/TWiki/TWikiPreferences),
and can be overwritten by ***user preferences*** (your personal topic,
i.e.
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)
in the TWiki.Main web)

***Preferences:***

-   wikiweblist
    -   Set WIKIWEBLIST =
        [Fmext](/_/WebHome)
        \|
        [SukcSpit](http://web.fm/twiki/bin/view/Sukcspit/WebHome)
        \|
        [TWiki](http://web.fm/twiki/bin/view/TWiki/WebHome)

```{=html}
<!-- -->
```
-   List of topics of the TWiki.Fmext web:
    -   Set WEBTOPICLIST =
        [Home](/_/WebHome)
        \|
        [Changes](/_/WebChanges)
        \|
        [Index](/_/WebIndex)
        \|
        [Search](/_/WebSearch)
        \| Go

```{=html}
<!-- -->
```
-   Web specific background color: (Pick a lighter one of the
    [StandardColors](http://web.fm/twiki/bin/view/TWiki/StandardColors))
    -   Set WEBBGCOLOR = #7f4e40

```{=html}
<!-- -->
```
-   Background color of non existing topic:
    -   Set NEWTOPICBGCOLOR = #5f2e20
-   Font color of non existing topic: ( default #0000FF )
    -   Set NEWTOPICFONTCOLOR = #dd6611

```{=html}
<!-- -->
```
-   Exclude web from a `web="all"` search: (Set to `on` for hidden webs)
    -   Set NOSEARCHALL =

```{=html}
<!-- -->
```
-   List this web in the
    [SiteMap](http://web.fm/twiki/bin/view/TWiki/SiteMap):
    -   If yes, set SITEMAPLIST to `on`, do not set NOSEARCHALL, and add
        the \"what\" and \"use to\...\" description for the site map.
        Make sure to list only links that include the name of the web,
        e.g. Fmext.Topic links.
    -   Set SITEMAPLIST = on
    -   Set SITEMAPWHAT = Describe what this web does.
    -   Set SITEMAPUSETO = \...to do something.

```{=html}
<!-- -->
```
-   change the skin for this one ..
    -   Set SKIN = fmski

```{=html}
<!-- -->
```
-   Default template for new topics and form(s) for this web:
    -   [WebTopicEditTemplate[^?^](/_/WebTopicEditTemplate?topicparent=Fmext.WebPreferences)]{.twikiNewLink
        style="background : #5f2e20;"}: Default template for new topics
        in this web. (Site-level is used if topic does not exist)
    -   [TWiki.WebTopicEditTemplate](http://web.fm/twiki/bin/view/TWiki/WebTopicEditTemplate):
        Site-level default template
    -   [TWikiForms](http://web.fm/twiki/bin/view/TWiki/TWikiForms):
        How to enable form(s)
    -   Set WEBFORMS =

```{=html}
<!-- -->
```
-   Users or groups who ***are not*** / ***are*** allowed to ***view***
    / ***change*** / ***rename*** topics in the Fmext web: (See
    [TWikiAccessControl](http://web.fm/twiki/bin/view/TWiki/TWikiAccessControl))
    -   Set DENYWEBVIEW =
    -   Set ALLOWWEBVIEW =
    -   Set DENYWEBCHANGE =
    -   Set ALLOWWEBCHANGE =
    -   Set DENYWEBRENAME =
    -   Set ALLOWWEBRENAME =

```{=html}
<!-- -->
```
-   Users or groups allowed to change or rename this WebPreferences
    topic: (I.e.
    [TWikiAdminGroup](http://web.fm/twiki/bin/view/Main/TWikiAdminGroup))
    -   Set ALLOWTOPICCHANGE =
    -   Set ALLOWTOPICRENAME =

```{=html}
<!-- -->
```
-   Web preferences that are **not** allowed to be overridden by user
    preferences:
    -   Set FINALPREFERENCES = WEBTOPICLIST, DENYWEBVIEW, ALLOWWEBVIEW,
        DENYWEBCHANGE, ALLOWWEBCHANGE, DENYWEBRENAME, ALLOWWEBRENAME

***Notes:***

-   A preference is defined as:\
    `6 spaces * Set NAME = value`\
    Example:
    -   Set WEBBGCOLOR = #FFFFC0
-   Preferences are used as
    [TWikiVariables](http://web.fm/twiki/bin/view/TWiki/TWikiVariables)
    by enclosing the name in percent signs. Example:
    -   When you write variable `%WEBBGCOLOR%` , it gets expanded to
        `#7f4e40` .
-   The sequential order of the preference settings is significant.
    Define preferences that use other preferences first, i.e. set
    `WEBCOPYRIGHT` before `WIKIWEBMASTER` since `%WEBCOPYRIGHT%` uses
    the `%WIKIWEBMASTER%` variable.
-   You can introduce new preferences variables and use them in your
    topics and templates. There is no need to change the TWiki engine
    (Perl scripts).

```{=html}
<!-- -->
```
-   logo \...
    -   Set FMEXTLOGO = Fmext/fm_logo_fmext_01.png
-   dummy invisible image \...
    -   Set NIXIMGIF = Fmext/nix.gif
    -   Set NIXIMGNP = Fmext/nix.png

***Related Topics:***

-   [TWikiPreferences](http://web.fm/twiki/bin/view/TWiki/TWikiPreferences)
    has site-level preferences.
-   [TWikiUsers](http://web.fm/twiki/bin/view/Main/TWikiUsers)
    has a list of user topics. User topics can have optional user
    preferences.
-   [TWikiVariables](http://web.fm/twiki/bin/view/TWiki/TWikiVariables)
    has a list of common `%VARIABLES%`.
-   [TWikiAccessControl](http://web.fm/twiki/bin/view/TWiki/TWikiAccessControl)
    explains how to restrict access by users or groups.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Web%20*Preferences%5B%5EA-Za-z%5D)

Revision r1.1 - 16 Jul 2005 - 18:05 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/WebPreferences) \|
[More](http://web.fm/twiki/bin/oops/Fmext/WebPreferences?template=oopsmore&param1=1.6&param2=1.6)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.WebPreferences)


