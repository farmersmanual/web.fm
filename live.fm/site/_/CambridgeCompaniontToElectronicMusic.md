<post-metadata>
  <post-title> </post-title>
  <post-date>2007-01-11</post-date>
  <post-tags>fm, music, interview, artist statement, Cambridge Companion to Electronic Music</post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FM Esoterica](/_/FmEsoterica) \> **CambridgeCompaniontToElectronicMusic**** 


# The Cambridge Companion to Electronic Music

![CCEM-p78.png](/i/CCEM-p78.png)


    physical reality, as modeled by many temporal, local theories,  appears
    to be fundamentally linked to a zoo of oscillation processes.  our senses
    synthesize these cycles into experiences, be it sonic  events, phenomena
    of light, awareness of space, wireless  communication, plain matter and
    much more. as indicated by the order  given, the sonic poses a very
    accessible and rewarding way to explore  wave phenomena, but this is not
    where it ends.

    engineering activities have uncovered the electromagnetic within  certain
    limits, or to particular purposes, although to a great extent  in
    disregard or disinterest of secondary effects on physiology and
    consciousness, in other words, the medium is not neutral and on its  own
    commences sending messages of unintended content to unintended  receivers.

    indifferent music becomes sound as it identifies itself as operations  on
    the organization of weak processes, disregarding boundaries of
    occlusion. so that towards the end the authors become the audience  and
    the audience stretches to the horizon, populated by machines  infinitely
    rendering a stream of minute semantic lunacy.

    what is left is the establishement of a musical use of a broader band  of
    waves, both regarding frequency range and media.

    a system regulating frequency, duration, event-clustering and
    arbitrarily more parameters is at least a musical system. in a
    stochastic system this means choice of probabilities, in a process-
    coupled system this means filling the system with life by choice of
    governing events.

      i m sorry i couldnt elaborate on a geometric perspective due to  lack
    of time and misplacing the resonating compass.

 -> Cambridge University Press

------
Revision r1.1 - 11 Jan 2007 - 23:44
