

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\>
[BuckyMedia](/_/BuckyMedia)
\>
[BuckyMediaGhent](/_/BuckyMediaGhent)
\> **WorkshopAtVooruit**** \${
[**Edit**](/_/WorkshopAtVooruit?t=1469037737)
}}+-C67

## []{#Workshop_at_Vooruit_2004_02} Workshop at Vooruit 2004/02

          
                       ____..........___      __,,........._
                         _.-'                '`'''''              ``-._
                    _,-''                                              `--._
                  ,'                                                        `-.._
                ,'                                                               `-..
              ,'                                                                     `-..__
            ,'                                                                                   .-                                                                                
          |    Hi !                                                                          |

          |                                                                                  
          |    i'd like to thanks you all for the great four days spent in Gent. It was       |
          |    all so quiet and yet so many things intresting happens inside/outside my       |
          |    head.                                                                          /
          |                                                                                 ,'
          |    thanks again.                                                               /
          |    bye.                                                                       /
          \                                                                              /
          `.   pierre                                                                   '
           |.                                                                   _.--'
             `--._                                                     _,,...-''
                  `-.._                    _________,,...........---'''
                       `._           ,,-'''
                          `.       ,'
                            `-.                              `._                               `.`.
                                    `/
                                     '

![emiel1.png](/i/WorkshopAtVooruit/emiel1.png)

------------------------------------------------------------------------

Topics included:

-   3D geometry for a first understanding between our world and other
    universal citizens
-   the socializing potential of known and unknown physical models and
    relations
-   digital sensing devices in wireless networks
-   programming for realtime audiovisual performances

------------------------------------------------------------------------

DATA:

    36591 33859 42511
    35425 28384 41044
    40748 35710 38700
    31761 36811 42040
    38975 26901 36172
    42235 31429 34704
    38573 40440 35888
    33042 40589 37910
    27755 33431 40324
    29949 28097 39572
    30025 26570 33905
    35491 25773 31761
    35844 30232 27605
    41101 33004 29515
    39818 36762 29627
    38793 38195 30163
    33204 37855 28658
    30033 39351 32664
    26381 35083 34855
    32141 32739 27079

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Workshop%20*At%20*Vooruit%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/WorkshopAtVooruit) \|
[More](http://web.fm/twiki/bin/oops/Fmext/WorkshopAtVooruit?template=oopsmore&param1=1.4&param2=1.4)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.WorkshopAtVooruit)


