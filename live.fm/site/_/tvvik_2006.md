<post-metadata>
  <post-title>My first post</post-title>
  <post-date>2006-01-01</post-date>
  <post-tags>waTTT 3.2</post-tags>
</post-metadata>

# wATTT 3.1

.
                        '-._  `-.
                       ''-..i-._ `._                                  `--:._ `.    \ .-.
   '-.          _  _,.--------..,.::........,:.i.._
      `-.    _,.--'-::i'|    ,-'    | ``,i:.\.:..:::-.._
       _,;-:i          `+-,=i__,..--+''''`i\_\_|= -'\   `-.
 _,,-''     .:._       `P' "":.=P=P"\=- :''":,\'.    `.
'              `:..  \  `.     ,''----._ ''=P`8\|     '.
                  ``-.\   |   /      -----:.._`::'--.   +
                      `+:i   /',-'           `-|:P_    | ,b
                |    .._| `._;|                 \|\    |/|/
                \       `-+._`..                 |`L   8/, XcuiIo 
                 `..  ._| |/P'  `'               `. \ |\\|
      `            `..  ``+.'                     | '\ +O                   `
    `  `` `  `  `  ` `:-| |/`-._`    `  `  `  `  ` |`|+|/` `  `  `  `` ````````
 ` `  `  `    `  `  ` `:._| `````-:i``````````````.|.||X||.. ...   ............
  `  ` `   ``  ` ``  ` `|-`.`````````::``.`.......||\+|^|\....^^.......++++++++
 ,``````.`.`.....`. .....||':-.........:-......;,:JY+\|:|:\;;;;;;;;;;?????==???  </pre>
we are <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/WonkInGwonkinik">mere mortals</a> \\      \\ <strong><em>___</em></strong> ;;;;;;;; <strong><em>everything is becoming another layer of the <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/RecentLiveArchive">RecentLiveArchive</a></em></strong> <br>
<p/>
<hr/>
<div class="twikiToc">
<ul>
<li> <a href="#FrozenMusic">FrozenMusic</a>
</li>
<li> <a href="#GracefulDeprivation">GracefulDeprivation</a>
</li>
<li> <a href="#whats_cooking">whats cooking?</a>
</li>
</ul>
</div>
<hr/>
<h2><a name="FrozenMusic"> </a> <span class="twikiNewLink" style="background : #5f2e20;"><font color="#dd6611">FrozenMusic</font><a href="/web/20060103191704/http://web.fm/twiki-bin/edit/Fmext/FrozenMusic?topicparent=Fmext.WebHome"><sup>?</sup></a></span> </h2>
<p/>
<img src="https://web.archive.org/web/20060103191704im_//i/BuckyMediaWalsall/walsall3_pano.jpg" alt="walsall3_pano.jpg"/>
<img src="https://web.archive.org/web/20060103191704im_//i/BuckyMediaWalsall/0.00.06.19.jpg" alt="0.00.06.19.jpg"/>
<img src="https://web.archive.org/web/20060103191704im_//i/BuckyMediaWalsall/Parallel_soundview.jpg" alt="Parallel_soundview.jpg"/>
<img src="https://web.archive.org/web/20060103191704im_//i/BuckyMediaWalsall/0.00.20.15.jpg" alt="0.00.20.15.jpg"/>
<p/>
<pre>
...add text about frozen music
</pre>
<p/>
<strong>Jump: <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/BuckyMediaWalsall">BuckyMediaWalsall</a></strong>
<hr/>
<h2><a name="GracefulDeprivation"> </a> <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/GracefulDeprivation">GracefulDeprivation</a> </h2>
<p/>
<img src="https://web.archive.org/web/20060103191704im_//i/GracefulDeprivation/international3.png" alt="international3.png"/>
<p/>
<pre>
With a sensory apparatus that is constantly exceeded and that increasingly operates in drug-induced states of awareness,
this is also an acknowledgement of the expanding notion of the human body in its current position
as a somewhat destabilized sensorial interface.
</pre>
<p/>
<strong>Jump: <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/GracefulDeprivation">GracefulDeprivation</a></strong>
<hr/>
<h2><a name="whats_cooking"> </a><a name="whats_cooking_"> </a> whats cooking? </h2>
<p/>
<ul>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/WebStatistics">WebStatistics</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FooL1shEztravagant">FooL1shEztravagant</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/ZigZagiZ">ZigZagiZ</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/VertragsTemplateDe">VertragsTemplateDe</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/PinkLight">PinkLight</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmProjects">FmProjects</a> <br/>
</li>
<li>  <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/NealWhite">NealWhite</a> <br/>
</li>
</ul>
Number of topics: <b>7</b><p/>
<hr/>
<span class="txt">// <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmReleases">FmReleases</a> \\ <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmEsoterica">FmEsoterica</a> \ <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmArchive">FmArchive</a> // <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmData">FmData</a> / <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/FmDates">FmDates</a> / <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/BookFm">BookFm</a> // <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/WebChanges">WebChanges</a> <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/WebIndex">WebIndex</a> <a class="twikiLink" href="/web/20060103191704/http://web.fm/twiki-bin/view/Fmext/WebSearch">WebSearch</a></span>
<hr/>
<a href="https://web.archive.org/web/20060103191704/http://help.us.stay.a.live.fm/" target="_top">http://help.us.stay.a.live.fm/</a>


<!--
    FILE ARCHIVED ON 19:17:04 Jan 03, 2006 AND RETRIEVED FROM THE
    INTERNET ARCHIVE ON 14:22:57 Sep 21, 2023.
    JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

    ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
    SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 159.127
  exclusion.robots: 0.069
  exclusion.robots.policy: 0.059
  cdx.remote: 0.054
  esindex: 0.01
  LoadShardBlock: 100.865 (3)
  PetaboxLoader3.datanode: 135.116 (4)
  load_resource: 160.676
  PetaboxLoader3.resolve: 69.645
-->
