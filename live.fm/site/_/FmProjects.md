

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****FmProjects**** \${
[**Edit**](/_/FmProjects?t=1469037732)
}}+-C67

## []{#2006} 2006

-   [Elektrosmogfreien at Waves Festival, RIXC,
    Riga](/_/WavesExhibition)
-   [BuckyMedia at Faster Than Sound, Aldeburgh Festival, Bentwaters
    Airbase](/_/FasterThanSound)

## []{#2005} 2005

-   [GracefulDeprivation](/_/GracefulDeprivation)
    at 5 Days Off, Amsterdam
-   [GracefulDeprivation](/_/GracefulDeprivation)
    at Sensory Clinic, Manchester

## []{#2004} 2004

-   [BuckyMedia at New Art Gallery,
    Walsall](/_/BuckyMediaWalsall)
-   [Pink Light an XDV exhibition at Freiraum,
    Vienna](/_/PinkLight)
-   [BuckyMedia at Voruit,
    Ghent](/_/BuckyMediaGhent)

## []{#2003} 2003

-   [BuckyMedia at Abstraction Now, Künstlerhaus,
    Vienna](/_/BuckyMediaVienna)
-   [Recent Live Archive released
    on+offline](/_/RecentLiveArchive)

## []{#2002} 2002

## []{#2001} 2001

## []{#2000} 2000

## []{#1999} 1999

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Fm%20*Projects%5B%5EA-Za-z%5D)

Revision r1.1 - 16 Feb 2007 - 15:33 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/FmProjects) \|
[More](http://web.fm/twiki/bin/oops/Fmext/FmProjects?template=oopsmore&param1=1.5&param2=1.5)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.FmProjects)


