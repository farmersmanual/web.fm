<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

# Biodata

constituted 94 - vienna

members: hog smith, yuri per nesusipratima, smet van hoek plus occasional temporary external virtual contributors

homebase: badland

\"Formed in Vienna in the mid-1990\'s, the farmersmanual collective represents media art at its most anarchistic. While being best-known for their recordings, in recent years the group have shifted their emphasis towards extensive live performances, in which imaginative computer animation, synchronized with \'chaos-particle-accelerating\' music, and the overall ambience of uncontrollable technology form a seamless whole.\" (anton
nikkil⪠[http://www.avantofestival.com](http://www.avantofestival.com) )


\"there were the cacophonous bleeps and burps from Austrian group farmersmanual\'s \"ship of fools\" sailing the canals.\" (TIME mag., 07/2001)

with the aid of electronic computers, the composer becomes a sort of pilot: pressing buttons, introducing coordinates, and supervising the controls of a cosmic vessel sailing in the space of sound, across sonic constellations and galaxies that could formerly be glimpsed only in a distant dream. (iannis xenakis 1971)


Revision r1.1 - 05 Jul 2005 - 13:17
