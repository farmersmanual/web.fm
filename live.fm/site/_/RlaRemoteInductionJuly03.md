<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaRemoteInductionJuly03**** \${
[**Edit**](/_/RlaRemoteInductionJuly03?t=1469037742)
}}+-C67

Farmer\'s Manual are one of the key bands when I think of the Austrian
label Mego. But for all that they haven\'t exactly released a lot of
albums. Even so they have been playing extensively live, which seems to
be more of what they are about. Many of the events of the last several
years have been recorded, and have been available on a website. At the
time of this release they had almost 4 days worth of live material,
which using new DVD technology they have managed to get on to one disc.

On hearing about this DVD with so much material on it I was unsure as to
what to expect. How on earth do you deal with 3 days and 21 hours and so
many minutes worth of live material? Being a DVD the DVD software kicks
in as soon as I put this disc in my computer. A little music plays and I
expect a menu to come up. Strangely it doesn\'t, instead we have some
footage, where the members of Farmer\'s Manual are sitting around a
studio and then find a dividing wall which they decide to break through.
Okay, I watch this and do other stuff, waiting for the menu, waiting for
live stuff to come on. It doesn\'t. The film finishes and that\'s that.
Odd. Of course my next thought is to look into the directory, which is
where I find the meat of this release. In basic terms the website that
the material has been downloadable from has been put into these folders.
With which there are many external links for you to explore further. But
folder after folder of mp3s of live material, photos, interviews, web
pages. A fairly serious documentation of who Farmer\'s Manual are and
what they have been doing over the years.

Straight off there are downsides to this release. MP3 is a restrictive
format, limiting the listener to computer use - while some people have
MP3 players they aren\'t that common, besides which the last time I was
experimenting with moving MP3s around on a Mego disc they were locked to
prevent that. Another thing is that, as with that previous Mego disc,
the web pages don\'t always load properly, I don\'t know if they have
been programmed for a different platform or not, but there are certainly
issues.

However these are not necessarily a big deal. All of the material can be
accessed by just going into the folders as you would any other computer
file. With which there is a hell of a lot of material, and it will
certainly take me some time till I have worked through all of it - if I
ever quite do manage.

Working through the material it appears that some of the mp3s are either
very minimal, take a long while to build or complete silence. For now we
don\'t have the patience for those pieces - too much other material here
to worry about that. The material covers a range of electronic
manipulations - like when I saw Farmer\'s Manual perform, some of the
material is willfully difficult, other pieces are easier to appreciate.
Reading the interviews while listening helps, giving impressions of the
way in which the trio collect sounds and manipulate them ruthlessly to
provide the end result. A result which tends to be improvised, at least
enough that each performance is different. As they would need to be to
make this release worth while in the slightest. Many of the pieces are
quite long, being live recordings as they are, ranging from 30 minutes
to considerably longer. I tend to find that this means that I will
listen to one piece in a session, in much the same way as you would
witness only one performance at a time. Though there are some folders
which feature several tracks, which allow for more lee way if you are
only looking to listen to a little bit at any one point.

Overall this release is pretty impressive. A real chance to explore the
music of this influential groups. Brought together in a strong package,
one disc with so much on it, in a DVD case that has all the dates
detailed on it, in a card slip sleeve which is covered with concert
tickets, passes, airplane slips and the like. A unique release.

RVWR: PTR June 2003

------------------------------------------------------------------------

More Information

1: Mego

label site. 2: [RLA](/_/RLA)

release site, filled with all the material from the DVD and material
recorded since.

[http://www.remoteinduction.co.uk/](http://www.remoteinduction.co.uk/) 

\-- [HiazHhzz](http://web.fm/twiki/bin/view/Fm/HiazHhzz) -
31 Jul 2003

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Remote%20*Induction%20*July%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\|
[Attach](http://web.fm/twiki/bin/attach/Fmext/RlaRemoteInductionJuly03)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaRemoteInductionJuly03?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaRemoteInductionJuly03)


