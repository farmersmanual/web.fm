<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\> **OrMinidiscCompilation**** \${
[**Edit**](/_/OrMinidiscCompilation?t=1469037773)
}}+-C67

Out Now!

[http://www.itsaor.net/just13.html](http://www.itsaor.net/just13.html) 

------------------------------------------------------------------------

Various Artists Or MD Comp Or 2003 {7.1} Reviewed by: Nick Phillips
Reviewed on: July 1, 2003

From the frenetic digital splicing of the Mego label to Jean-Luc
Godard's cut-and-paste film narratives to William S. Burrough's work
with disjointed text edits, experimental art has long proved a fertile
breeding ground for the aesthetics of disruption. Jarring
discontinuities and oddball juxtapositions, if appropriately
sliced-and-diced with artistic flair, offer a fascinating simulation of
the frantic machinations of modern life -- and provide the viewer or
listener with a pleasant little mindfuck while they're at it.

Which, it appears, also works as a decent description of the modus
operandi of Russell Haswell's Or label, which has long operated with
blatant disregard to such niceties as stylistic continuity and release
schedules. Previous releases have swerved in style from the "ambient
metal" of Olympia, Washington's Earth (collected on Or, in typically
obtuse fashion, as a live session pressed onto one-sided 12" vinyl) to
the melted laptop screech of Farmers Manual's classic "Explorers We" to
the minimal techno of Stuzpunkt Wien 12. Now, after a fairly long period
of inactivity, Or has spluttered back to life with the long-delayed
release of their second minidisc compilation, featuring the likes of
Farmers Manual, Shirttrax, and Francisco Lopez. Except, to add the
confusion, the compilation arrives in compact-disc format, apparently
since the profusion of MP3 downloading has left minidiscs as the niche
market for internet music surfers.

No matter, because unlike Or's first minidisc release by Gescom, which
relied heavily on short loops brought to life by minidisc's shuffle
mode, this compilation works confusingly enough in the linear format of
the compact disc. Not too surprising for anything that bears the name of
Viennese laptop group Farmers Manual, as anyone who's made it through
the overwhelming irruptions of the "Recent Live Archive" DVD (featuring
three days (!) worth of live MP3 recordings) can attest. This time
around, however, Farmers Manual are the most cohesive of the bunch,
kicking off the proceedings with the surprisingly ear-pleasingly burbles
of "kTXt-1.sinemerge2" before getting down-and-dirty with the distorted
reverberations of "kTXt-3.6r4.in'. At two minutes apiece, it's a sweet
little capsule introduction to the digital hyperactivity dominating the
compilation; one that's let down only slightly by Francisco Lopez's
following "Untitled #101," a surprisingly weak piece that splices
together ambient environmental recordings with pinging sinewaves without
any of the concision and attention to detail that usually mark Lopez's
work.

Things get back on track with Shirttrax, nom-de-disque of snd's Mark
Fell in collaboration with Jeremy Potter, who offer seven remixes of
their own "Good News About Space." The concise, rhythmic minimalism of
their previous work is here refined with emphasis on a kind of
pointillist textural experimentation that yields surprisingly refreshing
results, bringing to mind a more restrained take on the digi-crackle of
Pita's "Seven Tons for Free." Sequenced together, the seven track flow
together seamlessly to form an impressively prismatic rethinking of the
original track. Gescom, on the other hand, abandon rhythm altogether in
favor of billowing clouds of dense, granulated sound cohering around the
spectral traces of a dusty synth loop: the echoic, near-industrial
results neatly triangulate restless experimentalism with the emotional
tug of its haunted melodies.

And then it's so long to the restraint: as befits anything bearing the
stamp of Russell Haswell (whose own Live Salvage disc on Mego throws up
a savage bout of lo-fi laptop distortion), the last half of the disc is
given over to good old-fashioned noise, though admittedly of the
new-fangled digital variety. Elusive Japanese duo Incapacitants provide
a ten-minute mix of "electronics and voice," though both are processed
down to such a molten scree that it's hard to uncover any trace of these
original elements. And MAZK (the duo of Zbigniew Karkowski and Merzbow)
lay down more of the same, plying a scuffed, throbbing drone with shards
of digital debris in a manner reminiscent of the Haswell/Merzbow
recordings as Satanstornade. Much like any other noise recording, if you
like this kind of stuff, you'll love these tracks, but there's not much
here to welcome the non-enthusiast. The same, however, can not be said
for the contributions of Florian Hecker, who may well be the most
exciting person working in experimental electronics at the moment. While
his compilation of eight short shock-tactic recordings were obviously,
unlike the others, designed with the minidisc format in mind, they work
perfectly in continuous play, offering a continuously evolving and
consistently engaging assortment of micro-edited digital filaments. Like
his mindblowing Sun Pandämonium release on Mego, Hecker's ear for the
hyperspeed cut-and-paste makes for wonderfully vibrant music, at once
humorous, alert and brutally ear-piercing. Xenakis, once again, would be
proud.

Topping Hecker's micro-masterpieces would be a pretty daunting problem,
one the compilation cleverly avoids by bringing the compilation to an
amusing anticlimax with two rather pointless live recordings by Farmers
Manual, in which the pops and crackles onstage are drowned out by the
drunk ramblings of a raver repeatedly exhorting the group to "get a
fucking bassline!" The perfect ending to a perfect mish-mash, really.
For those who appreciate their heads being smacked around by their music
every once in a while, this is essential listening.

from
[http://www.stylusmagazine.com/musicreviews/various_artists-or_md_compilation.shtml](http://www.stylusmagazine.com/musicreviews/various_artists-or_md_compilation.shtml) 

another review \>\>
[OrMDReview](/_/OrMDReview)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Or%20*Minidisc%20*Compilation%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/OrMinidiscCompilation)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/OrMinidiscCompilation?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.OrMinidiscCompilation)


