<post-metadata>
  <post-title>Hydrogen by inaud1bl3</post-title>
  <post-date>2023-01-23</post-date>
  <post-tags>generate and test, gt 97,inaud1bl3, release </post-tags>
</post-metadata>

##

| | |
|--- |--- |
| ![a2566077416_10](/i/a2566077416_10-1200x1080.jpg) | <iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=3238091492/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/" seamless><a href="https://inaud1bl3.bandcamp.com/album/hydrogen">Hydrogen by inaud1bl3</a></iframe> |

# Hydrogen by inaud1bl3

Out on [bandcamp](https://web.archive.org/web/20230608125320/https://inaud1bl3.bandcamp.com/album/hydrogen?label=3200328970&tab=artists) go get it.

The album is dedicated to the thing that we humans refer to as ‚hydrogen atom'. It's the most common element constituting three quarters of all visible matter and it's the basic building block in the universe.

The 12 psychedelic-math-dreampop & spacerock & idm & dubstep-drum'n'bass-symphonic-guitarheroing sung-in-english & rapped-in-german songs and tracks on this album are created, arranged, mixed, mastered and produced by inaud1bl3


## credits
- released January 13, 2023
- farmersmanual GT -- [generate and test](/generate-and-test) -- gt 97
- inaud1bl3 is inaudible

