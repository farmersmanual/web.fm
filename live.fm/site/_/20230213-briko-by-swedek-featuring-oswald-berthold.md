<post-metadata>
  <post-title>Briko by SWEDEK featuring Oswald Berthold</post-title>
  <post-date>2022-12-23</post-date>
  <post-tags>computermusic, generate and test, single, swedek, release, 2022</post-tags>
</post-metadata>

# Briko by SWEDEK featuring Oswald Berthold

![](/i/swedek-briko-a0359643482_10.jpg)

**Swedek** is Mattersdorfer / Kaplan / Lackner

Briko then, available on bandcamp

It’s no secret that making a great album takes a lot of hard work. But why is the resulting album so great?

There are a few reasons. First, the artists have put in the time and effort to create something they’re truly proud of. They’ve also worked with some of the best producers, engineers, and mixers in the business to ensure that every track is as good as it can be.

Second, the hard work has paid off in terms of sales. The album has been hugely successful, selling millions of copies worldwide. This success means that the artists have been able to recoup their investment and make a nice profit from their music.

Finally, and most importantly, the fans love the album! It’s become one of their all-time favorite records, thanks to its catchy tunes, great lyrics, and overall high quality. So next time you’re feeling down about how long it’s taking to make your own masterpiece, remember that even the biggest albums out there took a ton of hard work (and often years) to come together.

## credits

- released December 23, 2022
- Swedek is Helmut Kaplan, Wernfried Lackner, Dieter Mattersdorfer
- Additional noises by Oswald Berthold
- Written, performed, recorded, produced, mastered by Swedek
- farmersmxnual presents – generate and test – gt94

## Categories
computermusic, generate-and-test, single,swedek	


