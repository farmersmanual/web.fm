<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **InterviewLeMonde**** \${
[**Edit**](/_/InterviewLeMonde?t=1469037863)
}}+-C67

### []{#B_ro_s_duit_Paris_avec_les_nouve} Büro séduit Paris avec les nouvelles musiques électroniques

FARMERS MANUAL, TRIO O\'ROURKE, FENNESZ ET REHBERG, Le Garage, Paris
11e, dimanche 10 mai.

Alors que Vienne, Berlin ou Londres font preuve depuis longtemps d\`une
intense activité dans le domaine des nouvelles musiques électroniques et
expérimentales, Paris apparaissait singulièrement absent. D\'autant que
des pôles comme Grenoble, Nancy ou Marseille ont développé une scène
innovante dans un courant où se télescopent la musique, la vidéo, des
installations\... Büro, récente structure d \' organisation
d\'événements musicaux (regroupement du magasin de disques Wave,
spécialisé dans les musiques nouvelles, de la formation musicale
francaise Sister Iiodine et de la revue transmedia Episodic) veut faire
bouger la capitale où, hormis le travail de fond des Instants chavirés,
les occasions sont rares de suivre en direct cette scène expérimentale.
Une première soirée, dimanche 10 mai au Garage, a servi de test à Büro
pour vérifier sa capacité à interesser un public en dehors du cercle des
connaisseurs . L\'essor de la techno et du trip-hop avec ses sons
électroniques a amené la frange la plus fouineuse des amateurs jusqu\'au
Garage, un hangar avec d\'tinattendus balcons latéraux, qui fait le
plein. Public jeune, gai et avide de découvertes. Dans la salle une
table en bois sur laquelle est posé un appareillage de câblage et de
petites machines.

Farmers Manual, trio autrichien, s\'est installé derrière la table.
Chacun des musiciens a un micro-ordinateur portable. Dans la mémoire,
des sons: le clavier sert à les déclencher, les machines permettent
d\'en moduler les fréquences. C\'est léger, on peut amener la musique à
domicile avec quelques kilos de matériel. Si ses enregistrements
montrent un goût pour le ludique et le mouvement, Farmers Manual est, ce
soir, resté un peu à distance. Il utilise des séquences rythmiques, joue
sur la vitesse de battement, certains sons (balle de ping-pong,\"bip\"
d\'ordinateur\...) pourraient être exploités, mais restent en suspens.

Suit le trio qui réunit l\'Américain Jim O\'Rourke et les Autrichiens
Christian Fennesz et Peter Rehberg. Même matériel, presque même position
derrière la table. Par sa maturité et sa présence scénique qui laisse
passer le sentiment d\'une implication dans le spectacle, le trio montre
sa différence. Les musiciens utilisent des sons de guitare (on entend
une phrase en boucle de Led Zeppelin), des cordes symphoniques, un cor
anglais, brouillés, perturbés par des sonorités de grattements , des
traits fugitifs qui pourraient être de larges rayures de couleurs sur
une toile. Surtout, ils construisent ensemble un mouvement musical qui
évolue pour gagner en densité jusqu\'à une apothéose \"hollywoodienne\"
très bien amenée. Prochain événement Büro, fin mai.

Sylvain Sidier

Sites Internet: Buro www.icono.org/buro; Mego (label viennois)
www.icf.de/mego

------------------------------------------------------------------------

Interviewer: Sylvain Sidier\
Date: may 1998\
Additional Link:
[http://www.icono.org/buro/](http://www.icono.org/buro/) \

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Interview%20*Le%20*Monde%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/InterviewLeMonde) \|
[More](http://web.fm/twiki/bin/oops/Fmext/InterviewLeMonde?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.InterviewLeMonde)


