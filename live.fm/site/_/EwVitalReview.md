

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwVitalReview**** \${
[**Edit**](/_/EwVitalReview?t=1469037845)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Rviewed by: Vital\
Date: 1998\

------------------------------------------------------------------------

FARMERSMANUAL - EXPLODERS_WE (CD on OR)

Two CD\'s for the price of one; at least for the first thousand
purchasers. Included with this studio CD is a live disc with two tracks.
The press release (but not the packaging) suggest that listeners use the
Random/Shuffle knob on their players to get the most out of the \'dead\'
(studio) CD, which has 60 tracks and certainly benefits from this
instruction. I have listened to it a few times in as a linear
composition, and must say that I really like the way it starts off,
dragging itself, staggering, into unholy life. However, I found that it
lacked the structural surprises of earlier FM stuff, when played from
top to bottom, and it certainly works best by following the user\'s
instructions once the beats drop in. Random/Shuffle generates a wild
hysteria which I think is synonymous with
[FarmersManual](/_/FarmersManual)
music. As always, it\'s the crushed rhythm patterns that make this music
such a pleasure, plus seeing the horrified expression on your best
chum\'s face when his speakers rupture and disembowel themselves through
the bass ports like so many Ebola victims. It\'s a bit like connecting
two power cables to the same machine. Lovely, crunchy, over-excited
bursts of unrestrained machines enjoying the pain being inflicted on
them. Occasionally I had the feeling I was listening to the Residents
after they had taken the wrong kind of drugs, or perhaps the
[TeleTubbies[^?^](/_/TeleTubbies?topicparent=Fmext.EwVitalReview)]{.twikiNewLink
style="background : #5f2e20;"} when they\'ve had theirs taken away from
them. The \'live\' CD contains two tracks, which consist of masses of
digital stretches layered over each other and which come across more as
an electro-acoustic exercise in demolition with less attention paid to
the beats. Well worth getting and shuffling loud. (MP)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Vital%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwVitalReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwVitalReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwVitalReview)


