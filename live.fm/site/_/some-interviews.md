

# some interviews with farmers manual 

-   [InterviewEdicionsAbarna](/_/InterviewEdicionsAbarna)
-   [InterviewKraak](/_/InterviewKraak)
-   [InterviewLeMonde](/_/InterviewLeMonde)
-   [InterviewLivingDigitally](/_/InterviewLivingDigitally)
-   [InterviewNatureIsPerverse](/_/InterviewNatureIsPerverse)
-   [InterviewPostPhonotaktik](/_/InterviewPostPhonotaktik)
-   [InterviewTntCosmos](/_/InterviewTntCosmos)
-   [InterviewWozIvFm](/_/InterviewWozIvFm)

...and [see also](/_/FmInterviews/)
