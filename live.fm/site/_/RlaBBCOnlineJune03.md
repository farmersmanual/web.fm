<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **RlaBBCOnlineJune03**** \${
[**Edit**](/_/RlaBBCOnlineJune03?t=1469037838)
}}+-C67

Farmers Manual [RLA](/_/RLA)
(recent live archive) DVD (Mego)

It can seem like the narrator of Jorge Luis Borges\' Library of Babel
contemplating the infinite: \'..the formless and chaotic nature of
almost all the books\...\' For every line of straightforward statement,
there are leagues of senseless cacophonies, verbal jumbles and
incoherences.

Borges\' story, frequently cited as prescient of the ever-growing
networked data store via which you\'re reading this review, might also
be applied to Farmers Manual\'s DVD, website and other products.

Should strategies be developed to negotiate such volumes of data as
FM/RLA provides? Mapless, it\'s easy to lose one\'s way. Lost, one is
forced to survey the landscape in detail, to pay attention to both the
immediate surroundings and the horizon: one becomes an explorer, willing
or not.

It\'s tempting to describe the group\'s soundscapes in terms of terrain:
tundras, screes, forestation or weather patterns: fogbanks, rainsqualls,
cloudshapes. If nature is gnomic, is FM too? Trying to find meaning
could be like trying to interpret the shapes of sand dunes or waves.
Alternatively FM might be viewed as a Rosicrucian-like sect practicing a
machine coded gnosis, but it\'s too lazy to make do with these images.
Better to pay attention to a 1998 interview included on this disc which
reveals a likeably insouciant attitude; make your own sense of our work,
they seem to say:

Interviewer: First of all I\'d like you to explain the Farmers Manual
method for making sounds and music. Mathias Gmachl: It ís all
improvised. Oswald Berthold: No, it ís not. Stefan Possert: What a
strange question!

FM claim to work towards as pure a machinic extemporisation as may be
attained by a small core of operators. Unbalancing the Kraftwerk
equilibrium of man/machine, the spectral traces of the former scarcely
trouble the movements of the latter.

FM at times field the ingenuousness of free improvisation, at times the
generative output of Confield-era Autechre, the noisescapes of Oval, the
dadaisms of V/Vm.

There are sudden interruptions, hums, crackles, static, abrupt changes,
developments in geological time, samples interrogated (snatches of 1970s
Miles Davis, hardhouse, OMD), exhilarating noise-fests, there are beats
and there\'s beatlessness, sets by fellow travellers such as Pan Sonic,
Russell Haswell et al. All available on the DVD or downloadable from
their site. There\'s even some gloriously pointless (or deeply
symbolic?) video.

Each FM concert is an experiment. In their playfulness, their dedication
to research, their preparedness to walk tightropes without a net and to
batter your ears, FM deserve, even demand your attention.

Reviewer: Colin Buttimer

[http://www.bbc.co.uk/music/experimental/reviews/farmers_rla.shtml](http://www.bbc.co.uk/music/experimental/reviews/farmers_rla.shtml) 

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*BBCOnline%20*June%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaBBCOnlineJune03) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaBBCOnlineJune03?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaBBCOnlineJune03)


