

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****TotalAutomationVsHumanInteraction**** \${
[**Edit**](/_/TotalAutomationVsHumanInteraction?t=1469037742)
}}+-C67

### []{#TOTAL_AUTOMATION_vs_HUMAN_INTERA} TOTAL AUTOMATION vs. HUMAN INTERACTION

during the performance we seek to shift the local atmosphere from
dissolution and clumsiness through manual change and ecstatic fiddling
into an imaginative state of complex monotony, structured calm and
chill, or endless associative babbling. so that towards the end the
authors become the audience and the audience can be confronted with a
stage populated by machines only which cant get out of infinitely
rendering a stream of slight semantic madness. the setup then is what is
normally considered a sort of installation. all this with the help of
extreme frequencies and distorted, flickering images.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Total%20*Automation%20*Vs%20*Human%20*Interaction%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\|
[Attach](http://web.fm/twiki/bin/attach/Fmext/TotalAutomationVsHumanInteraction)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/TotalAutomationVsHumanInteraction?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.TotalAutomationVsHumanInteraction)


