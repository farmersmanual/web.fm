

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwGenericText**** \${
[**Edit**](/_/EwGenericText?t=1469037844)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Reviewed by: OR (generic squish txt)\
Date: 1998\

------------------------------------------------------------------------

or\
squish 4\
farmersmanual - \'Explorers_We\'\
CD - 1st 1000 with free live CD\
1 track: 60:04:00 \[60 PQ points\]\
Barcode number 5027803330320\
Release date: May 17th 1998\

available now from here for eleven pounds (UK) twelve (Europe) thirteen
(rest of World) by credit card

Following their highly acclaimed 2nd CD, \'fsck\' on Tray, farmersmanual
got to work with their bare hands and produced Explorers_We on or. One
constantly changing track with 60 PQ start points, making full use of
your dusty SHUFFLE button\...

Ost, one of the farmers called it \'a sinewave massacre\'. The first
1000 come with a free live CD (2 tracks).

\"a soprano voice sings notes that never arrange themselves into a
melody, that fall apart in the same way as dead proteins\....\"
(Gravity\'s Rainbow &emdash; Thomas Pynchon)

Track listing:

CD DEAD

1-60 Exploders_We. Duration: 60:04:00

CD LIVE

1 Recorded live @ ars electronica 09/1997 {event: mego loveboat series:
subtronic} during passage trough \"voest- alpine industriehafen\"
between approximately 3 and 4 am. Computer mediated three-in:six-track
live.jam human-triggered parameter changes. Duration: 36:20:00

2 Recorded live @ electronica potent ii \-- the final letdown {mego
night 97 at WUK, Vienna} regularly scheduled fm appearance computer
generated null-in:two-track random-piece no human interaction. Duration:
15:35:00

What they said about the farmers manual &emdash;

\"Points of comparison are completely elusive. Frenetic beats jostle
with shards of blistering electronic noise and all manner of high
strangeness\... \...alot of artists have a great deal of catching up to
do if they are to remain relevant. An awesome release.\" (John Everall,
The Wire)

\"Weird tracks, owing in some ways to d&b as much as it does to
industrial music. If I say a lot of this sounds fucked, believe me this
is truly fucked. The more accessible (if that is an appropiate word in
this context) are in the beginning of the CD, but it gradually grows
noiser and weirder throughout. The noisy bits and pieces at the end are
suitable for random play or looping mode. Again fun for home-DJs (as you
would be thrown out of the club if you play this on the dance floor) and
bedroom composers. Industrial music for the digital generation!\"
(VITAL, The Netherlands)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Generic%20*Text%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwGenericText) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwGenericText?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwGenericText)


