<post-metadata>
  <post-title>rla -- maerzmusik 2007</post-title>
  <post-date>2023-01-23</post-date>
  <post-tags>fm, lac, performance, RLA, Maerzmusik, 2007</post-tags>
</post-metadata>

# rla -- maerzmusik 2007


> ## "This is the best fm piece ever recorded." 
>
> Oswald \@x75

In 2007 farmersmanual performed at an event hosted by the Berlin avantgarde festival [Maerzmusik](https://web.archive.org/web/20230608124354/https://www.berlinerfestspiele.de/de/berliner-festspiele/programm/bfs-gesamtprogramm/programmdetail_6492.html) in cooperation with the Linux Audio Conference, which took place in Berlin simultaneously.


![](https://web.archive.org/web/20230608124354im_/https://live.fm/wp-content/uploads/2023/03/dsc03836-2K.jpg)

![](https://web.archive.org/web/20230608124354im_/https://live.fm/wp-content/uploads/2023/03/dsc03818-2K.jpg) 


It took us more than 10 years to follow up with the promoter, and we've now managed to get a hold of the recording that was promised on the night. It took another four years of email negotiations to locate the recording in the archive and some 600MB of wav data was eventually transferred. Many thanks to everyone who went through the operation with us; in particular the recording engineer Manfred Tiesler, the producers Ina Steffan and Ilse MÃ¼ller, and Franziska Hempel who was responsible for the final unarchiving.


![CD](https://web.archive.org/web/20230608124354im_/https://live.fm/wp-content/uploads/2023/03/CD.jpg)

In the recording you can hear a sonification of the electromagnetic spectrum. It is an hour of entirely data driven sound. Custom built broadband radio receivers listen across the spectrum from VLF to the microwave bands. Every sound heard corresponds to an event somewhere in the radio spectrum, mostly digital data packets being transmitted wirelessly (mobile telephony, bluetooth, WiFi) with some intermittent analog freakage. This complex piece was performed only once and has never been reproduced since. It feels pretty weird sonically and can take some time to get used to. Once you do, it becomes a sensory experience of technological realities usually hidden from our senses.


![](https://web.archive.org/web/20230608124354im_/https://live.fm/wp-content/uploads/2023/03/dsc03816-2K.jpg) 

![](https://web.archive.org/web/20230608124354im_/https://live.fm/wp-content/uploads/2023/03/dsc03815-2K.jpg) 


Maybe we can just train GPT on WiFi packets for our next Netflix. Get in touch, send feedback, upload. Compressing into audible range and expanding...

Either way, worth the wait and effort. Very happy to share.

[https://archive.org/details/farmersmanual-maerzmusik-20070324](https://web.archive.org/web/20230608124354/https://archive.org/details/farmersmanual-maerzmusik-20070324)

## Video snippet

https://www.youtube.com/watch?v=3G2yv1FpbiY

