

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\>
[BuckyMedia](/_/BuckyMedia)
\> **BuckyMediaWalsall**** \${
[**Edit**](/_/BuckyMediaWalsall?t=1469037734)
}}+-C67

::: twikiToc
-   -   [frozen
        music](/_/BuckyMediaWalsall#frozen_music)
    -   [liquid crystal
        display](/_/BuckyMediaWalsall#liquid_crystal_display)
    -   [assessing spatial and temporal dimension at
        once](/_/BuckyMediaWalsall#assessing_spatial_and_temporal_d)

-   [Index of
    /20040425-20040503_walsall,moma](/_/BuckyMediaWalsall#Index_of_20040425_20040503_walsa)
:::

------------------------------------------------------------------------

## []{#frozen_music} frozen music

-   walsall4_pano.jpg:\
    ![walsall4_pano.jpg](/i/BuckyMediaWalsall/walsall4_pano.jpg){width="800"
    height="128"}

```{=html}
<!-- -->
```
-   walsall3_pano.jpg:\
    ![walsall3_pano.jpg](/i/BuckyMediaWalsall/walsall3_pano.jpg){width="800"
    height="108"}

```{=html}
<!-- -->
```
-   0.00.01.25.jpg:\
    ![0.00.01.25.jpg](/i/BuckyMediaWalsall/0.00.01.25.jpg){width="800"
    height="73"} \*

```{=html}
<!-- -->
```
-   0.00.06.19.jpg:\
    ![0.00.06.19.jpg](/i/BuckyMediaWalsall/0.00.06.19.jpg){width="800"
    height="73"} \*

```{=html}
<!-- -->
```
-   0.00.09.26.jpg:\
    ![0.00.09.26.jpg](/i/BuckyMediaWalsall/0.00.09.26.jpg){width="800"
    height="73"} \*

```{=html}
<!-- -->
```
-   0.00.12.15.jpg:\
    ![0.00.12.15.jpg](/i/BuckyMediaWalsall/0.00.12.15.jpg){width="800"
    height="73"} \*

```{=html}
<!-- -->
```
-   0.00.20.15.jpg:\
    ![0.00.20.15.jpg](/i/BuckyMediaWalsall/0.00.20.15.jpg){width="800"
    height="73"} \*

```{=html}
<!-- -->
```
-   Parallel_soundview.jpg:\
    ![Parallel_soundview.jpg](/i/BuckyMediaWalsall/Parallel_soundview.jpg){width="800"
    height="118"}

------------------------------------------------------------------------

## []{#liquid_crystal_display} liquid crystal display

Our third presentation of
[BuckyMedia](/_/BuckyMedia)
involved the public on as many levels as possible. The geodesic sphere
was equipped with a wireless sound system and digital sensors. It was at
the same time musical instrument and concert hall. We provided geometry
toolkits and documentation of our past presentations and tried to give
access to all the information we had put together so far.

The project resonated through the entire museum, its curators, technical
staff and attendants, inducing its own frequencies into the people,
gallery spaces and technical infrastructure around it. The sphere worked
like a liquid crystal, reconfiguring the flow of energy in its
environment.

The stunning changes of daylight in the gallery space provided an
atmosphere of high-altitude, lucid dreaming, while the sonified
gravitational data of the structure added a narrow band of digital
squeals and screams to the deep vibrations of the shrink-wrap window
drums.

------------------------------------------------------------------------

## []{#assessing_spatial_and_temporal_d} assessing spatial and temporal dimension at once

# []{#Index_of_20040425_20040503_walsa} Index of /20040425-20040503_walsall,moma

     Name                                                         Last modified      Size  Description Parent Directory                                                                  -   
     BuckyMedia press images - 1.jpg                              02-May-2006 15:33  1.5M  
     BuckyMedia press images - 2.jpg                              02-May-2006 15:34  1.3M  
     BuckyMedia press images - 3.jpg                              02-May-2006 15:36  1.0M  
     BuckyMedia press images - 4.jpg                              02-May-2006 15:35  1.3M  
     BuckyMedia press images - 5.jpg                              02-May-2006 15:45  6.5M  
     BuckyMedia press images - 6.jpg                              02-May-2006 15:49  3.0M  
     BuckyMedia-Walsall-MPEG-4-progressive-1536kb-640x480.mp4     05-Apr-2006 18:11  131M  
     Frozen Music-MPEG-4-progressive-1536kb-100%25.mp4            30-May-2006 17:30   57M  
     FrozenMusic-FullRes.mp4                                      19-Feb-2007 12:13    0   
     FrozenMusic.mp4                                              19-Feb-2007 12:13    0   

Apache/2.2.9 (Debian) PHP/5.2.17-0.dotdeb.0 with Suhosin-Patch Server at
rla.web.fm Port 80

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Bucky%20*Media%20*Walsall%5B%5EA-Za-z%5D)

Revision r1.1 - 15 Feb 2006 - 17:57 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/BuckyMediaWalsall) \|
[More](http://web.fm/twiki/bin/oops/Fmext/BuckyMediaWalsall?template=oopsmore&param1=1.6&param2=1.6)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.BuckyMediaWalsall)


