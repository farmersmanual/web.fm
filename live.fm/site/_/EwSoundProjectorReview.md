

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwSoundProjectorReview**** \${
[**Edit**](/_/EwSoundProjectorReview?t=1469037844)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Rviewed by: The Sound Projector (UK)\
Date: \~ 1998\

------------------------------------------------------------------------

\"Dance music for the clinically insane. This is experimentation of such
mad extremes that you sometimes wonder why other lesser people in the
field are even bothering. War Arrow just heard it and sat here
transfixed, delighting in the fact that \'everything was going
completely wrong\' and that it was virtually impossible to connect the
machine-driven illogical noises with anything human beings might do. It
reminded him of the qualities he always admired in favourite industrial
music. Personally I love every minute of it, and I like the way Farmers
Manual can still annoy some people who consider themselves broadminded
musically, yet they will lose patience after only a few minutes with
what they hear as aimless and formless self-indulgence. Why not simply
surrender to the outrageous wit and be entertained by the sheer
daringness of the deranged adventures here catalogued?

On the other hand, it\'s not hard to see why this might be construed as
somehow alienating. As suggested above, its inhuman; at no point does
this music ever resemble anything like the sound of a drum or programmed
bass or even a comforting piano sample. Instead, rhythms (of a sort, at
any rate) are generated from weird loops, or volume knobs applied to
amplifier hum, skipping CDs, and great gobs of deadly reverb and echo
applied like fertiliser with fiendish glee by the Farmers boys. Verily,
it seems like every electronic available to the modern music-maker
becomes a plaything for mischief in their hands - all part of the
gigantic toy train-set that must be wrecked. The cumulative result of
this is frigtening - it will undoubtedly warp many a young mind. It is a
Chinese puzzle of unfathomable, evil, random-generated nonsense which
strands the hapless listener in the furthest reaches of the Back of
Beyond. Just right for a night of \'You Call That Music?!\' with the
Radio 3 Mixing It boys.

A double CD set, of which Explorers_We has been embedded with 60 index
points; if you have a random-play facility you can take part in the
general insanity by making even more chaos from the debris. The 2nd live
disc comprises two recordings from some obscure festivals in Europoe,
described as \'computer mediated\...live Jam human triggered parameter
changes\'. All I can say is I\'m surprised they got away with doing it
in a public place without being lynched, but then its comforting for me
to hear such far-flung eccentricity thriving in a world where the
Normals are gaining more ground every day.\"

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Sound%20*Projector%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwSoundProjectorReview)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/EwSoundProjectorReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwSoundProjectorReview)


