<post-metadata>
  <post-title>Underground Sea by a0n</post-title>
  <post-date>2023-03-18</post-date>
  <post-tags>generate and test, release, gt100, a0n0, Underground Sea</post-tags>
</post-metadata>


# Underground Sea by a0n0

![](https://web.archive.org/web/20230605002427im_/https://live.fm/wp-content/uploads/2023/03/cover-hires.jpg)

In spring we shed some memories and make room for new ones. The crust opens in places and we are allowed brief glimpses of the underground sea. Quiet on the outside, loud awash.

![](https://web.archive.org/web/20230605002427im_/https://live.fm/wp-content/uploads/2023/03/testimonial-yZ1q4kkN.jpeg)


[https://a0n0.bandcamp.com/album/underground-sea?label=3200328970&tab=music](https://web.archive.org/web/20230605002427/https://a0n0.bandcamp.com/album/underground-sea?label=3200328970&tab=music)

## farmersmxnual presents -- generate and test -- gt100

https://www.youtube.com/watch?v=c9V_hGlsI1o

:::

