<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaAvopolis**** \${
[**Edit**](/_/RlaAvopolis?t=1469037759)
}}+-C67

-   Reviews: Farmers Manual -
    [RLA](/_/RLA) (Recent
    Live Archive)
-   Label: Mego (7/2003)
-   Είδος: Electronica, Dance
-   Κείμενο: Τάκης Θανόπουλος
-   [http://www.avopolis.gr/reviews/default.asp?ID=1654](http://www.avopolis.gr/reviews/default.asp?ID=1654) 

Από όποια σκοπιά και αν το δει κανείς οι Farmers Manual αποτελούν μια
ιδιάζουσα περίπτωση για την Αυστριακή ηλεκτρονική σκηνή. Ξεκινώντας στα
μέσα της περασμένης δεκαετίας, επέλεξαν να κινηθούν στις πιο άναρχες
ηχητικές διαδρομές, διαρκώς πειραματιζόμενοι σε διαφορετικούς ήχους,
video προβολές και άλλες τεχνολογικές αυθαιρεσίες. Μάλιστα επέλεξαν ως
φυσικό τους χώρο αυτοσχεδιασμού και πειραματισμών, όχι το studio αλλά
τις μακροσκελείς και απρόβλεπτες ζωντανές τους εμφανίσεις.

Το να επιχειρήσει κανείς να ξεδιαλέξει κάποια από τα κομμάτια του
αχανούς αυτού συναυλιακού παζλ των Farmers Manual, είναι από δύσκολο ως
αδύνατο, αν μάλιστα θελήσει να οι επιλογές του να είναι και
αντιπροσωπευτικές. Οπότε οι τύποι της Mego έδωσαν την λύση του Γόρδιου
αυτού δεσμού, συγκεντρώνοντας όλες τις live ηχογραφήσεις των FM που
μπορούσαν να εντοπίσουν, μετατρέποντας τις σε mp3 και βάζοντας τις σε
ένα DVD rom !!!

Με αυτόν το τρόπο η κυκλοφορία με κωδικό 777 της Mego, όχι μόνο ξεπερνά
την τυπική 80λεπτη χωρητικότητα ενός cd, αλλά αγγίζει τα όρια του
ηχητικού overdose, φτάνοντας σε διάρκεια τις 3 μέρες, 21 ώρες, 38 λεπτά
και 3 δευτερόλεπτα! Σε αυτό το εντυπωσιακό μέγεθος, μπορούμε να
προσθέσουμε και το υπόλοιπο υλικό που βρίσκεται στο dvd rom, όπως τις
φωτογραφίες, τα video και κάποιες πληροφορίες και σχόλια, καταλήγοντας
σε μια κυκλοφορία-υπερπαραγωγή. Επηρεασμένοι μάλιστα από το φιλελεύθερο
στυλ του όλου project, οι παραγωγοί της Mego, διοχέτευσαν και όλο το
ογκώδες αυτό περιεχόμενο σε ένα website
([http://rla.web.fm/](http://rla.web.fm/) ) στο οποίο ήδη
έχουν αρχίσει τα updates με extra υλικό.

Ας περάσουμε όμως στο περιεχόμενο του dvd, στο βαθμό που είναι αυτό
εφικτό, μια και ακόμα για μια επιλεκτική μόνο ακρόαση χρειάζεσαι
βδομάδες. Όπως αναμενόταν στον ηχητικό κυκεώνα του
[RLA](/_/RLA) βρίσκεις τα
πάντα. Από ήρεμες σχεδόν σιωπηλές στιγμές μέχρι πλήρες θόρυβο και από
ερασιτεχνικές ηχογραφήσεις (βλέπε εμφάνιση στο Sonar στην οποία
ακούγονται μέχρι και οι συζητήσεις του κόσμου) μέχρι παραγωγές που θα
ζήλευε και το καλύτερο studio. Οι Farmers Manual χώνονται για τα καλά
πίσω από τα powerbooks τους και αφήνονται σε ένα διαρκές κυνήγι ήχων και
συχνοτήτων.

Εννοείται ότι οποιαδήποτε προσπάθεια ανακεφαλαίωσης και βαθμολόγησης
αυτής της κυκλοφορίας πέφτει στο κενό. Ο όγκος της είναι απαγορευτικός
για την διεξαγωγή οποιαδήποτε συμπερασμάτων, τη στιγμή μάλιστα που
έχουμε να κάνουμε με έναν διαρκή αυτοσχεδιασμό (που μην ξεχνάμε κρατά
σχεδόν τέσσερις μέρες). Το μόνο σίγουρο είναι ότι το
[RLA](/_/RLA) κατέχει τον
προδιαγεγραμμένο χαρακτηρισμό τού δίσκου αναφοράς για την πειραματική
μουσική ακόμα και αν κανένας μας (σας) δεν καταφέρει τελικά να το
ακούσει-μελετήσει σε όλη του τη διάρκεια.

Ηχογραφήστε τη μουσική σας εδώ:www.studio-bean.com

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Avopolis%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaAvopolis) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaAvopolis?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaAvopolis)


