<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaNeuralITSep03**** \${
[**Edit**](/_/RlaNeuralITSep03?t=1469037738)
}}+-C67

. dvd

Farmers Manual

[RLA](/_/RLA)

Mego

\'RLA\', the Recent Live Archive, collects the live performances of
Farmers Manual and other artists of their entourage, from 1995 to May
2003, in a chronology made of more than 72 hours of mp3 audio files and
a chaotic html \'electronic notepad\' made of flyers, reviews, digital
pictures, interviews and links to the clubs/museums/theaters which
hosted their performances. This dvd is a pocket database, a snapshot of
their large work which, with the lean interface of an archive site (also
viewable on their constantly updated mirror on the web), becomes some
sort of ultimate authorized bootleg. The meaning of such an operation in
the broadband era can be understood by considering the stability of a
recording on a physical medium, the portability of this archive and the
celebration of their own public appearances, which suggests another
possbile change in the relationship between the audience and the artist
caused by home production and distribution. Just like the introductory
video, where the opening of a hole in one of the walls of the recording
studio leads to the appropriation of new spaces, the sheer size of this
work makes possible to visit a small museum of the activities of one of
the most active and original groups among those who experiment with
electronic improvisation and sometimes use it as an artistic machine.

[http://www.neural.it/nnews/farmersmanualrlae.htm](http://www.neural.it/nnews/farmersmanualrlae.htm) 
\[english\]

[http://www.neural.it/nnews/farmersmanualrla.htm](http://www.neural.it/nnews/farmersmanualrla.htm) 
\[italian\]

------------------------------------------------------------------------

\> \-\-\-\-\-- Forwarded Message\
From: Alessandro Ludovico \@neural.it\
Date: Thu, 2 Sep 2004 00:49:57 +0200\
Subject: Neural.it, honorary mention in Net.Vision - Prix Ars
Electronica 2004\

Dear all,\

Neural.it received a Honorary Mention in the Net.Vision category of the
Prix Ars Electronica 2004.\
I thank you all for supporting and substaining Neural at various levels
in all these years.\
Nevertheless i have to say that i am not a fan of prizes. I think they
create more sensless concurrency, self celebration and envy, than real
support or actual public statements.\
But, on the other hand, this symbolic award in fact is not intended to
me personally but to the community of Neural, to all the contributors
and readers that read, wrote, linked, and much more usefully, took the
time to critique and suggest improvement during the last decade.

A very big thank to you all.\
\--

Alessandro Ludovico\
Neural.it - [http://neural.it/](http://neural.it/)  daily
updated news + reviews\
English.Neural.it -
[http://english.neural.it/](http://english.neural.it/) \
Neural printed magazine -
[http://neural.it/n/nultimoe.htm](http://neural.it/n/nultimoe.htm) \

------------------------------------------------------------------------

End of Forwarded Message

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Neural%20*ITSep%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaNeuralITSep03) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaNeuralITSep03?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaNeuralITSep03)


