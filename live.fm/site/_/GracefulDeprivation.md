

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\> **GracefulDeprivation**** \${
[**Edit**](/_/GracefulDeprivation?t=1469037733)
}}+-C67

![emailinbjud.jpg](/i/GracefulDeprivation/emailinbjud.jpg){width="567"
height="404"}

::: twikiToc
-   [Sensory
    Clinic](/_/GracefulDeprivation#Sensory_Clinic)
-   [Private
    View](/_/GracefulDeprivation#Private_View)
-   [Graceful
    Deprivation](/_/GracefulDeprivation#Graceful_Deprivation)
    -   [images](/_/GracefulDeprivation#images)
:::

------------------------------------------------------------------------

## []{#Sensory_Clinic} Sensory Clinic

-   Art Orient矯bjet
-   [Decosterd & Rahm](http://www.low-architecture.com/) 
-   Farmersmanual
-   Shane Waltener
-   [Neal White](http://www.nealwhite.org/) 

Time spent in therapeutic so-called sensory rooms has shown to increase
concentration, alertness, calmness, and general awareness of the
surrounding world. With a critical edge and a bit of humour, this events
based project provides for participants to explore, learn and experience
full sensorial impressions.

With a sensory apparatus that is constantly exceeded and that
increasingly operates in drug-induced states of awareness, this is also
an acknowledgement of the expanding notion of the human body in its
current position as a somewhat destabilized sensorial interface.

Sensory Clinic explores and analyses social constructions of the senses.
Participating artists are informed by medical and scientific research,
by architecture and design, by a variety of artistic practices and their
histories. Taking into consideration emerging aesthetic approaches,
these events serve as platforms for artistic expressions and experiences
that moves beyond the otherwise dominating sensorial organ, the eye.

Curated by Cecilia Andersson

------------------------------------------------------------------------

## []{#Private_View} Private View

The International 3 gallery:

![international3.png](/i/GracefulDeprivation/international3.png){width="800"
height="186"}

The experience:

![SensoryOpening-5.jpg](/i/GracefulDeprivation/SensoryOpening-5.jpg){width="360"
height="480"}
![SensoryOpening-4.jpg](/i/GracefulDeprivation/SensoryOpening-4.jpg){width="360"
height="480"}

![SensoryOpening-1.jpg](/i/GracefulDeprivation/SensoryOpening-1.jpg){width="480"
height="360"}
![SensoryOpening-2.jpg](/i/GracefulDeprivation/SensoryOpening-2.jpg){width="480"
height="360"}

------------------------------------------------------------------------

## []{#Graceful_Deprivation} Graceful Deprivation

The installation uses a device called Neurophone mounted inside a
helmet.\
Via two steel transducer touching the skin the Neurophone transmits
ultrasonic impulses to the brain that are decoded as sound information
and heard \"inside the head\". This means that the Neurophone stimulates
perception through a seventh or alternative sense.\
Many primitive organisms and animals can see, hear and smell with their
skin. Its the largest and most complex organ of the human body and every
organ of perception evolved from folding skin. The helmet is set to the
Fibonacci mode which uses a frequency set based on the Golden Ratio
distribution (Golden Noise) to balance meridians of the body, improve
sleep, and better concentration.\
The helmet uses a visor made from polarization film to blur the users
vision and create an intimate experience within the gallery space.
Addtionally ear plugs can be used to intensify the listening experience.

Related:
[NeuroPhone](http://web.fm/twiki/bin/view/Hiaz/NeuroPhone)

------------------------------------------------------------------------

### []{#images} images

-   helmet-1.jpg:\
    ![helmet-1.jpg](/i/GracefulDeprivation/helmet-1.jpg){width="480"
    height="640"}

```{=html}
<!-- -->
```
-   helmet-3.jpg:\
    ![helmet-3.jpg](/i/GracefulDeprivation/helmet-3.jpg){width="480"
    height="640"}

```{=html}
<!-- -->
```
-   helmet-6.jpg:\
    ![helmet-6.jpg](/i/GracefulDeprivation/helmet-6.jpg){width="480"
    height="640"}

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Graceful%20*Deprivation%5B%5EA-Za-z%5D)

Revision r1.1 - 16 Feb 2007 - 15:28 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/GracefulDeprivation) \|
[More](http://web.fm/twiki/bin/oops/Fmext/GracefulDeprivation?template=oopsmore&param1=1.5&param2=1.5)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.GracefulDeprivation)


