

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****ElectronicMusicArchive**** \${
[**Edit**](/_/ElectronicMusicArchive?t=1469037762)
}}+-C67

## []{#Electronic_Music_Archive} Electronic Music Archive

September 6 to November 9, 2003\
Preview Friday, September 5, 2003, 7 p.m.\

**A project by Norbert Möslang and Gianni Jetzer**

Visual signs have always been produced in connection with music. Music
can be expressed in pictures and it can also be stored in the form of
scores, writing, record covers, photographs, videos, clips or animation.
The Electronic Music Archive is focused on the question: \"What does
electronic music actually look like?\"

The exhibition Electronic Music Archive is designed to collect visual
materials on electronic music as a possible means of molding the
present. The generation and modulation of electronic sounds, often in
connection with pictures, has been a constant in the fine arts for some
time now. This open interface between art and music is the context of
the Electronic Music Archive.

However, the Electronic Music Archive is also a deliberately subjective
treatment of memory. The archive collects various artistic statements
and stores them in cultural memory in the form of exhibition. In an age
in which the capacity of electronic storage is steadily growing and yet
data is irrevocably lost day after day, this project represents an
attempt to arrest time for a brief moment through the medium of an
exhibition. The resulting cultural and also emotional archive will make
it possible to compare the present with the past and to discuss such
concepts as \"quality\" or \"innovation.\" The archive poses specific
questions, such as: What kind of archives are there for electronic
culture? How can electronic culture be stored? Can memory be equated
with storage? Why is electronic storage space growing so much faster
than the capacity for cultural memory? Is their future for archives as
cultural storage spaces?

Fifty artists worldwide will be asked to submit to parts of their
archives or a personal collection. The Electronic Music Archive shall be
a repository of archives.

[http://www.k9000.ch](http://www.k9000.ch) \
Text by Gianni Jetzer

------------------------------------------------------------------------

-   [RlaGlobul](/_/RlaGlobul)
-   [RecentLiveArchive](/_/RecentLiveArchive)

![rla_glob.gif](http://web.fm/fmki/pub/Fmext/RlaGlobul/rla_glob.gif)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Electronic%20*Music%20*Archive%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/ElectronicMusicArchive)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/ElectronicMusicArchive?template=oopsmore&param1=1.3&param2=1.3)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.ElectronicMusicArchive)


