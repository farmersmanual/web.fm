

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\> **BuckyMedia**** \${
[**Edit**](/_/BuckyMedia?t=1469037760)
}}+-C67

![walsall3-1.png](/i/BuckyMediaWalsall/walsall3-1.png)

![vooruit6.png](/i/SyneticsAtVooruit/vooruit6.png)

![WorkerBeesFg6.png](/i/WorkerBees/WorkerBeesFg6.png)

-   [BuckyMediaWalsall](/_/BuckyMediaWalsall)
    -   [FrozenMusic[^?^](/_/FrozenMusic?topicparent=Fmext.BuckyMedia)]{.twikiNewLink
        style="background : #5f2e20;"}
    -   [SixSites[^?^](/_/SixSites?topicparent=Fmext.BuckyMedia)]{.twikiNewLink
        style="background : #5f2e20;"}

```{=html}
<!-- -->
```
-   [BuckyMediaGhent](/_/BuckyMediaGhent)
    -   [SyneticsAtVooruit](/_/SyneticsAtVooruit)
    -   [WorkshopAtVooruit](/_/WorkshopAtVooruit)

```{=html}
<!-- -->
```
-   [BuckyMediaVienna](/_/BuckyMediaVienna)
    -   [WorkerBees](/_/WorkerBees)
    -   [WoundedFloor](/_/WoundedFloor)
    -   [HerzSchlag](/_/HerzSchlag)
    -   [HighFrequencyErrorCorrectingField](/_/HighFrequencyErrorCorrectingField)
    -   [FeeDoc](/_/FeeDoc)

------------------------------------------------------------------------

In our space are several geodesic, tensile or synetic structures in
various scales. They are at the same time an audio/video display space
and a physical interface. By moving the structures the viewers can
navigate through different interpretations of real and virtual spaces
and time. Metaphorically speaking these spheres represents the globe,
the net, and the circular communication of the web. As physical object
it is a space within a space, it is an offer of a different type of
architecture, one that is circular, expendable and infinite.
([YosiWanunu](http://web.fm/twiki/bin/view/Hiaz/YosiWanunu))

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Bucky%20*Media%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/BuckyMedia) \|
[More](http://web.fm/twiki/bin/oops/Fmext/BuckyMedia?template=oopsmore&param1=1.8&param2=1.8)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.BuckyMedia)


