

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwCoverReview**** \${
[**Edit**](/_/EwCoverReview?t=1469037760)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Rviewed by: Cover (USA)\
Date: September 1998\

------------------------------------------------------------------------

\"Farmer\'s Manual (sic) take listeners on an exploration of unfamiliar
sounds. Explorers_We begins slowly with the white noise of machinery
humming, adding fans whirring, what might be a vacuum cleaner, and
techno playing in another room. The volume and dischord rises throughout
the hour, divided arbitrarily into sixty tracks. The white noise builds
with feedback and interference. Soon an off-kilter beat appears over a
striated soundscape, followed by pulses like the swish of a brush. The
record has built up the expectation that no sound will be recognizable,
so it comes as a surprise when track 19 introduces a relatively familiar
techno beat, admittedly generated from bizarre sound samples. For noise
aficionados, Farmer\'s Manual (sic) offer a more diverse palette than
the usual steady stream of white noise. By track 27, the volume and
discord has built to industrial noise band levels - Merzbow-style white
noise at attack force, with strangulated cymbals and abbreviated
percussion. The searing high frequency of track 37 sand papers the ear
drums, mutating into pendulum beats and pops. Explorers_We is like
tuning into radio emissions from another planet.\"

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Cover%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwCoverReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwCoverReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwCoverReview)


