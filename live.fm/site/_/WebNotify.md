

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****WebNotify**** \${
[**Edit**](/_/WebNotify?t=1469037739)
}}+-C67

WebNotify is a subscription service to be automatically notified by
email when topics change in the **TWiki.Fmext** web. This is a
convenient service, so you do not have to come back and check all the
time if something has changed. To subscribe to the service, please put
yourself on the list below. The format is:
`3 spaces * Main.yourWikiName - yourEmailAddress`

-   [JuanChanson](http://web.fm/twiki/bin/view/Main/JuanChanson)
-   [HiazHhzz](http://web.fm/twiki/bin/view/Main/HiazHhzz)

***Note:*** It is helpful to insert your name in alphabetical order (by
first name \-- ignore the \"Main.\") \-- then you can find your name (or
not) more easily if you wish to remove it or confirm that you are on the
list.

***Related topics:***
[TWikiUsers](http://web.fm/twiki/bin/view/Main/TWikiUsers),
[TWikiRegistration](http://web.fm/twiki/bin/view/TWiki/TWikiRegistration)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Web%20*Notify%5B%5EA-Za-z%5D)

Revision r1.1 - 08 Jul 2005 - 11:16 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/WebNotify) \|
[More](http://web.fm/twiki/bin/oops/Fmext/WebNotify?template=oopsmore&param1=1.4&param2=1.4)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.WebNotify)


