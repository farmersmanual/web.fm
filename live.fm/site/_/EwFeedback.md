

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **EwFeedback**** \${
[**Edit**](/_/EwFeedback?t=1469037739)
}}+-C67

### []{#exploring_feedback_we} exploring feedback, we

\"No sooner is something starting to take shape from the scatterings
than it catches on itself and falls to pieces, only to be replaced by
another juttering collection of broken sounds. It\'s all very abstract
and in a very intriguing way you\'re never quite sure what\'s going to
happen next.\"
[EwIncursionReview](/_/EwIncursionReview)

\"Explorers_We is like tuning into radio emissions from another
planet.\"
[EwCoverReview](/_/EwCoverReview)

\"Warum die Firma Apple Farmers Manual nicht an vorderster Front ihrer
\'Think different\' Kampagne anfuehrt, ist mindestens so
unverstaendlich\"
[EwDebugReview1](/_/EwDebugReview1)

\"Now create a continuous loop to play the oscillator. dspAdd({ \--
Start the signal processing loop. outval = value(osc); \-- Get osc\'s
value (a buffer full of samples). out(outval, L); \-- Send it to the
left output. out(outval, R); \-- Send it to the right output. }); \--
End the DSP loop. } \-- That\'s it! (End the start function, and the
program.)\"
[EwDebugReview2](/_/EwDebugReview2)

\"One constantly changing track with 60 PQ start points, making full use
of your dusty SHUFFLE button\...\"
[EwGenericText](/_/EwGenericText)

\"pop music for the year 4000\"
[EwPtuchReview](/_/EwPtuchReview)

\"It is a Chinese puzzle of unfathomable, evil, random-generated
nonsense which strands the hapless listener in the furthest reaches of
the Back of Beyond.\"
[EwSoundProjectorReview](/_/EwSoundProjectorReview)

\"Care to have your short-term memory completely and utterly fucked with
without the aid of mind-bending chemicals??\"
[EwSurveillanceReview](/_/EwSurveillanceReview)

\"throbbing, pulsing, crackling structures are possessed of an
intoxicating, and at times almost incoherent, power that testifies to a
creativity far id advance of the over-hyped doyens of new Electronica.\"
[EwWireReview](/_/EwWireReview)

\"it\'s the crushed rhythm patterns that make this music such a
pleasure, plus seeing the horrified expression on your best chum\'s face
when his speakers rupture and disembowel themselves through the bass
ports like so many Ebola victims. It\'s a bit like connecting two power
cables to the same machine.\"
[EwVitalReview](/_/EwVitalReview)

Synopsis: A two disk set, disk one (marked \"dead\") being 60 short 1
minute tracks (or one 60 minute track with tons of start points) which
is apparently meant to be shuffled, and disk 2 (marked \"live\")
containing 2 live tracks. According to the printing on disc 2: i
recorded on the boat 9/97, ii recorded indoors 12/97.
[EwKelseyDamas](/_/EwKelseyDamas)

Synopsis: A two disk set, disk one (marked \"dead\") being 60 short 1
minute tracks (or one 60 minute track with tons of start points) which
is apparently meant to be shuffled, and disk 2 (marked \"live\")
containing 2 live tracks. According to the printing on disc 2: i
recorded on the boat 9/97, ii recorded indoors 12/97.
[EwKelseyDamas](/_/EwKelseyDamas)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Feedback%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:18 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwFeedback) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwFeedback?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwFeedback)


