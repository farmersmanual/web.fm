

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****DoesNotCompute**** \${
[**Edit**](/_/DoesNotCompute?t=1469037773)
}}+-C67

### []{#does_not_compute} does not compute

    >> well out of the usual corridors of flight:
    >> spinners emerge from hooded dark inside the cowlings,
    >> spinning props invisible,
    >> the light of the sky catching all vulnerable
    >> surfaces a uniform bleak grey.

#### []{#New_Powers_Canada} []{#New_Powers_Canada_} New Powers (Canada)

\"Ash International\'s Tray label: Farmer\'s Manual Does Not Compute -
Tray is a new label run by Ash International (from London) and Mego
(from Vienna - whose band Pita was featured on the Mesmer Variations
compilation). Farmers Manuel is one of the groups behind Mego and here
they present two entirely different pieces. The A-side is a drum and
bass thing with jazzy guitar samples and breakbeat rhythms. The other
side is a flowing piece of synths and sounds whirling in and out of the
mix. Towards the end even the sound of Macintosh is heard. Hard chill
music at its best.\"

#### []{#The_Wire_UK} []{#The_Wire_UK_} The Wire (UK)

\"Further evidence of illegal additions to Vienna\'s water supply. These
Mego label stalwarts have created an utterly undanceable drum \'n\' bass
track for new label Tray, an appendage of Ash International, skipping
around but never entirely losing the plot. Like all the wonderfully
baffling Mego output, it manages to be both entertaining and completely
pointless.\" (Rob Young)

#### []{#m_cano_UK} []{#m_cano_UK_} m袡no (UK)

\"Innovative drum and bass coupled with a track of electronic loops and
other inhuman sound sources, all on top of grinding, low bass
frequencies. Farmers Manual also have a CD and LP out on Mego Records -
all warmly recommended.\" \[AK\]

#### []{#ennui_USA} []{#ennui_USA_} ennui (USA)

\"Farmers manual is perhaps one of the strangest and most original
projects to be released in some time. Ultra-synthetic sounds and drum
\'n\' bass rhythms are altered beyond belief, creating a hybrid of
jungle that at times crosses over to noise. To be clear, let me state
that while drum & bass influences are there, the album is in no way
danceable, and it only borders on listenable the first time you hear it.
Incredibly crazy, Farmers Manual will really appeal to fans of
experimental mauic more than techno fans. fsck has to be heard to be
believed. Recommended.\" Rating: 8.5

![dnc.gif](/i/DoesNotCompute/dnc.gif){width="313"
height="323"}![dnc2.gif](/i/DoesNotCompute/dnc2.gif){width="349"
height="303"}\
![dnc5.gif](/i/DoesNotCompute/dnc5.gif){width="150"
height="263"}![dnc3.gif](/i/DoesNotCompute/dnc3.gif){width="219"
height="248"}![dnc4.gif](/i/DoesNotCompute/dnc4.gif){width="288"
height="248"}

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Does%20*Not%20*Compute%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/DoesNotCompute) \|
[More](http://web.fm/twiki/bin/oops/Fmext/DoesNotCompute?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.DoesNotCompute)


