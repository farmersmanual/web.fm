<post-metadata>
  <post-title>farmersmanual remix of Rian Treanor & Ocen James</post-title>
  <post-date>2023-01-23</post-date>
  <post-tags>fm, remix, NNT, Rian Treanor, Ocen James, release, review, 2023</post-tags>
</post-metadata>

## _/

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=4219333449/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/track=3916757759/transparent=true/" seamless><a href="https://nyegenyegetapes.bandcamp.com/album/saccades">Saccades by Rian Treanor &amp; Ocen James</a></iframe>

# farmersmanual remix Rian Treanor & Ocen James

Turns out we have our remix traveling on that album, [Remo Rom (Farmers
Manual
remix)](https://web.archive.org/web/20230608111600/https://nyegenyegetapes.bandcamp.com/track/remo-rom-farmers-manual-remix) with great honor and pleasure on this magnificent album ðŸ™ŒðŸŽ‡


[Saccades by Rian Treanor & Ocen James](https://web.archive.org/web/20230608111600/https://nyegenyegetapes.bandcamp.com/album/saccades)


# Reviews

[pitchfork](https://web.archive.org/web/20230608111600/https://pitchfork.com/reviews/albums/rian-treanor-ocen-saccades/)
and
[quietus](https://web.archive.org/web/20230608111600/https://thequietus.com/articles/32515-saccades-rian-treanor-ocen-james-review).


