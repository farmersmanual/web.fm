<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaGaffaDkJuly03**** \${
[**Edit**](/_/RlaGaffaDkJuly03?t=1469037743)
}}+-C67

Farmersmanual [RLA](/_/RLA)
CD (Mego)

Dato: 21/7-2003

Farmersmanual har siden 1995 lavet improviseret og eksperimenterende
komputer musik. Det østrigske kunstner-kollektiv interesserer sig for,
hvorledes mennesker individuelt vælger at benytte sig af maskiners
muligheder - og nu giver gruppen os alle en chance for at studere,
hvordan de har brugt deres maskineri.
[RLA](/_/RLA) er som
DVD-Video/Rom et sandt sammensurium af multimedie indhold. Alle kendte
optagelser af gruppens koncerter fra deres debut i en fodgængertunnel
tilbage i 1995 og indtil maj 2002 er inkluderet. Der er også tekst,
billeder og video - og alt i alt har disken en spilletid på 3 dage 21
timer 38 minutter og 3 sekunder! Det er nok de færreste, der ønsker at
vide så meget om Farmersmanual, men som ide og projekt er
[RLA](/_/RLA) et
uovertruffet sanseligt overflødighedshorn. Den indeholder masser af
nørdet materiale som f.eks. flere udgaver af de samme koncerter optaget
fra forskellige outputs. At udforske
[RLA](/_/RLA) er således
lidt som at smugkigge i gruppens private logbog over deres
live-karriere. Og jo mere man klikker rundt og ser, hører og læser - jo
mere betagende bliver det. Check projektets webside
[http://rla.web.fm](http://rla.web.fm)  for mere info.

Af Jakob Rosenbak

[http://www.gaffa.dk/anmeldelser/view.php?mreview_id=26422](http://www.gaffa.dk/anmeldelser/view.php?mreview_id=26422) 

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Gaffa%20*Dk%20*July%20*03%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaGaffaDkJuly03) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaGaffaDkJuly03?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaGaffaDkJuly03)


