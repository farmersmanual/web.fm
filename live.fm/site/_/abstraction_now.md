<post-metadata>
  <post-title>Abstraction Now</post-title>
  <post-date>2005-07-05</post-date>
  <post-tags>test, fm</post-tags>
</post-metadata>

## The Official Blob

-   [http://www.abstraction-now.net](http://www.abstraction-now.net)
-   [ABSTRACTION-NOW.pdf](/i/AbstractionNow/ABSTRACTION-NOW.pdf)


## press
    - [http://kultur.orf.at/030828-12407/index.html](http://kultur.orf.at/030828-12407/index.html)
    - [http://derstandard.at/?id=1403326](http://derstandard.at/?id=1403326)
    - [http://www.textezurfotografie.net/text.php?TitelID=22](http://www.textezurfotografie.net/text.php?TitelID=22)
    - [http://nyartsmagazine.com/bbs2/messages/1469.html](http://nyartsmagazine.com/bbs2/messages/1469.html)

---- 
Revision r1.1 - 05 Jul 2005 - 13:17
