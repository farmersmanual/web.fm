

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwPtuchReview**** \${
[**Edit**](/_/EwPtuchReview?t=1469037844)
}}+-C67

------------------------------------------------------------------------

Review of: EXPLORERS_WE\
Reviewed by: (?)\
Date: summer \'98\
Additional Link:
[http://www.ptuch.ru/muznews.html](http://www.ptuch.ru/muznews.html) \

------------------------------------------------------------------------

page 1\>

A new label OR announced its appearance by re-release of \"Ufo
Bobahtungen\" by STUTZPUNKT WIEN 12, which was originally released as a
double album on the most underground Wienese label MEGO. Remastered,
this today a classical minimalistic techno recording has been made in a
12th district in Wien by a ELIN, whom you may know from his previous
project Auto Repeat.

Specialist\'s corner: noise, noise, noise. The same OR has put out
\"SPL\" by MAZK. MAZK is a new formation consisting of MERZBOW\'s Masami
Akita and a Tokyo resident Zbigniew Karkowski, who is also known for his
earlier work with The Hafler Trio and Phauss. This release contains 3
tracks recorded at ICC/NTT studio in Tokyo and is limited to 1000
copies.

TOUCH has issued a third in the series of samplers, inventively named
\"SAMPLER 3\".

Names such as FARMER\`S MANUAL (www.farmersmanual.co.at), PANASONIC,
CHRIS WATSON, PHILIP JECK, BRU򃟇ILBERT and SKALA are found in the
section \"site recording at the banks of Zambezi River, with required
sounds of gamelan, bizarre dialogues, disorienting overtones and
recordings from the British Library\". Relax at the sound of the pages
turned over! No doubt, this sampler will be a true gift for those
interested in \"ambient music\" (ambient in a sense of particular sound,
not of circle-painted T-shirts). They will immediately recognize this
cold alien electronics. Anything can happen within the next 30
seconds\... They call themselves sound-explorers, which is, basically,
the meaning of the release. Surely, it is not a pop music, and much of
what you hear is not music in the common sense. Yet, it could serve as a
perfect soundtrack, as well as a good starting point to further
exploration on the capabilities of sound. \>\

page 2\>

Farmers Manual present their esoteric exercises on the new record called
\"Explorers_We\". In addition to the 60 (!) tracks or buzzing, cracking
and beeping there\'s a free CD with 2 (!) tracks of buzzing, cracking
and beeping (again). Pop-music for the year 4000.

------------------------------------------------------------------------

![ptuch.gif](/i/EwPtuchReview/ptuch.gif){width="484"
height="79"}

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Ptuch%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwPtuchReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwPtuchReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwPtuchReview)


