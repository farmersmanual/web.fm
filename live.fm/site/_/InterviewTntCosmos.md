<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **InterviewTntCosmos**** \${
[**Edit**](/_/InterviewTntCosmos?t=1469037863)
}}+-C67

### []{#tnt_cosmos} []{#_tnt_cosmos_} / tnt cosmos /

    _My name is michel comte, I make a quarterly magazine called tnt cosmos
    _(as well as a site on http://www.altern.org/tntcosmos). I was at
    _"büro" concert here, but didn't get a chance to meet you^Ê Here are a
    _few questions - Some of them I'll ask also to the other musicians of
    _that night.
    _
    _1 Who come through the idea of gathering o' rourke, rehberg, fennesz
    _and you on a tour? Will this lead to a collective release?

    mere accident. rourke, pita & fenn were on tour and we did the
    paris-surplus. no release planned though...

    _2 What kind of reactions did you get in the different countries you've
    _been through?

    positive excitement - ignorance - hate

    _3 It is clear that it is collective improvisation - each one providing
    _his own files. But how far each musician improvises with his own sound
    _sources? What modifications can be made "live" on a mac or not and
    _which softs do you use? Does it depend on the memory ressources of the
    _computers?

    on macs we re using supercollider.
    there is virtually no limitation to what modifications can be made
    to either incoming sound, files and synthesized sound.
    the only one there is is the processing power and number of analogue outs.
    the amount of memory only limits the size of your audiobuffers...

    _4 Do you only perform with electronics in your studio, and did you do
    _a choice between pc and mac, if yes why?

    electronics only, yes, though i wouldnt call it "perform"
    anyway, our "real" instruments bear a massive load of dust behind the heap
    of other ancient crap...
    ad choice mac/pc:
    we did a choice for mac and linux and against windoze9x for i think
    obvious reasons...


    _5 What have you heard that turned you into musicians (other
    _musicians'work, noises, surrounding atmospheres, etc.)

    hm...
    E.T.
    blade runner
    the man who fell to earth
    .
    .
    .

    _6 What would be the shape of your most achieve major work - what kind
    _of musical act that would be (length, nature, etc.) - but maybe this
    _means nothing to you?

    this would be a piece of ultra-intelligent distributed software which
    could technically and conceptually continue and ultimately supersede the
    artistical output of farmersmnl...

    _7 where do you get your samples, what kind of athmosphere do you try
    _to create with them?

    samples: no particular cornucopia of audio-pasta
    possible sources include: internet, record-cd-file-collection, friend's
    audio DAT's, TV, ...

    atmosphere: happiness, contentment, unworriedness, optimism, warmth,
    order, sanity, clarity ..., ;)

    _8 Are you fond of analogic sounds also?

    just another source for samples?

    _9 About your personnal work, in what kind of shops is it available
    _across the world, and under which denomination?

    available in a lot of mailorder and small specialized shops.
    though major distribution through roughtrade in german-speaking area
    at the moment...
    no clue about the denomination...noize? electro? transgressive? (haha),
    experi-metal, ...


    _10 What have you recently released, and what are you going to release?
    _Can you describe those works, your conceptual attitude if you have
    _one?

    recent: explorers_we, 60 minutes of random-madness
    vague only future plans (cd on mego, a live cd(rom))


    _11 Do you have a personnal label or plan to have one?

    sure, buts its not so soon going to manifest itself within the physical
    domain...
    its probably called http://live.fm
    though the distinction between internetradiostation and (online)label is
    not quite clear...

    _12 Do you plan some other tours or concerts?

    all the time...

    _13 Do you have time to listen to other's music, for you what has been
    _remarkable for the last 20 years?

    sure.
    its more that we got too little material to listen to...
    the "last 20 years list" would be quite exhaustive and its not even our
    birthdays within...



    _14 What do you think about your music taken by dj's mixing it with
    _whatever they can think about?

    i think you can count on one hand the djs who play our "m,usic" anywhere
    but at home...

    _15 What are the musicians or artists, in austria or elsewhere, you'd
    _like to collaborate with?

    madonna

    _16 If you had to define austria through a noise, what would it be
    _produced by?

    a dying cockroach

    _17 Is there anything else you'd like to say?



    _
    _Please add a complete discography


    http://gargamel.com/frankie/fm.html

    _
    _also mention your address so I can send you the magazine.



    farmersmnaual
    franzensgasse 6
    1050 vienne
    autriche


    jo. hope sufficient data submitted...
    talk back...
    o

------------------------------------------------------------------------

Interview with: Oswald Berthold\
Interviewer: Michel Comte\
Date: ?\
Additional Link:
[http://www.altern.org/tntcosmos](http://www.altern.org/tntcosmos) \

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Interview%20*Tnt%20*Cosmos%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/InterviewTntCosmos) \|
[More](http://web.fm/twiki/bin/oops/Fmext/InterviewTntCosmos?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.InterviewTntCosmos)


