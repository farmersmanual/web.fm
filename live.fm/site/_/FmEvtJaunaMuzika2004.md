

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmDates](/_/FmDates) \>
**FmEvtJaunaMuzika2004**** \${
[**Edit**](/_/FmEvtJaunaMuzika2004?t=1469037763)
}}+-C67

farmersmanual performance at
[JaunaMuzika](http://www.mic.lt/fest_jaunamuzika04.htm) 
festival, 16.04.2004, contemporary art centre, vilnius, lithuania.

entitled **ø-weoweo (Øscmnägw - We\'ll Watch Each Other Watch Each
Other)**

location:
[http://www.cac.lt/en.php/](http://www.cac.lt/en.php/) 

**content**

farmersmanual comes down in a standard triplet configuration this time.
in the usual manner the byproducts of social and technological
networking are being picked up instead of discarded and after sufficient
transformation projected into audible space as a symphony of
concurrency. for the uninitiated, a piece whose score gets written by
the activities and interaction of a multitude of known and unknown
persons and at least the equal amount of machines. specifity arises from
a mapping of event-type to sonic process and selection of event sources,
where an event can be anything along the lines of someone fetching their
email, a portscan being run against a server, a temperature change at
vilnius airport or a news headline regarding EC patent law making it\'s
round.

**technical specification**

-   3 computers
-   DSL internet
-   5 channel setup
-   1 video projector

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Fm%20*Evt%20*Jauna%20*Muzika%20*2004%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/FmEvtJaunaMuzika2004)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/FmEvtJaunaMuzika2004?template=oopsmore&param1=1.3&param2=1.3)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.FmEvtJaunaMuzika2004)


