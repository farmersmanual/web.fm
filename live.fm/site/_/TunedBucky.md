

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****TunedBucky**** \${
[**Edit**](/_/TunedBucky?t=1469037732)
}}+-C67

To do:
[TunedBuckyTechraider](/_/TunedBuckyTechraider)\
\

\* Map of the proposed area for the day:\
![Map of the proposed area for the
day](/i/TunedBucky/Bild2.png){width="797"
height="922"}

the area is between:\
\
- tv tower to NE\
- red cityhall to S (not in picture)\
- extreme kitsch neptun fountain from GDR to SW (circular structure)\
- berlins oldest church st.marien to NW.\
\
red is the space available for the performance, flipped E.\
the image is from 2006, so the construction work and cars are now gone
and the fountains (green) are in order.\
the blues cross indicates source of power and possible location of PA &
lab tables.\
\
yellow is the position of a possible huge beanbag (partly covered by the
spiderlegs as roofs) for conferences, but might be somewhere offpic as
well.\
\

Onsite imgs by Verena:

-   Main arm, view towards fountain:\
    ![Main arm, view towards
    fountain](/i/TunedBucky/RIMG0171.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Red City Hall:\
    ![Red City
    Hall](/i/TunedBucky/RIMG0173.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Tower Base & part of fountains NE:\
    ![Tower Base & part of fountains
    NE](/i/TunedBucky/RIMG0178.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Church St. Mariens NW:\
    ![Church St. Mariens
    NW](/i/TunedBucky/RIMG0179.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Hi:\
    ![Hi](/i/TunedBucky/RIMG0182.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Lower arm, view towards tower:\
    ![Lower arm, view towards
    tower](/i/TunedBucky/RIMG0185.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Main arm, view towards tower:\
    ![Main arm, view towards
    tower](/i/TunedBucky/RIMG0187.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   Main arm w/ tower vanishing point:\
    ![Main arm w/ tower vanishing
    point](/i/TunedBucky/RIMG0188.jpg){width="480"
    height="640"}

```{=html}
<!-- -->
```
-   Upper arm, view towards tower:\
    ![Upper arm, view towards
    tower](/i/TunedBucky/RIMG0189.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   View from church, possible PA & lab position:\
    ![View from church, possible PA & lab
    position](/i/TunedBucky/RIMG0191.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   St. Mariens:\
    ![St.
    Mariens](/i/TunedBucky/RIMG0192.jpg){width="640"
    height="480"}

```{=html}
<!-- -->
```
-   skeleton of Palast der Republik on the opposite:\
    ![skeleton of Palast der Republik on the
    opposite](/i/TunedBucky/RIMG0195.jpg){width="640"
    height="480"}

\-- Main.gert - 13 May 2008

::: twikiAttachments
  **[Attachment](http://web.fm/twiki/bin/view/TWiki/FileAttachment)**                                                                                 **Action**                                                                                                                                          **Size** **Date**              **Who**     **Comment**
  --------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------- ---------- --------------------- ----------- -------------------------------------------------
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0171.jpg](/i/TunedBucky/RIMG0171.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0171.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       38.5 K 13 May 2008 - 21:12   Main.gert   Main arm, view towards fountain
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0173.jpg](/i/TunedBucky/RIMG0173.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0173.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       36.9 K 13 May 2008 - 21:12   Main.gert   Red City Hall
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0178.jpg](/i/TunedBucky/RIMG0178.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0178.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       42.6 K 13 May 2008 - 21:13   Main.gert   Tower Base & part of fountains NE
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0179.jpg](/i/TunedBucky/RIMG0179.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0179.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       58.8 K 13 May 2008 - 21:14   Main.gert   Church St. Mariens NW
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0182.jpg](/i/TunedBucky/RIMG0182.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0182.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       61.1 K 13 May 2008 - 21:14   Main.gert   Hi
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0185.jpg](/i/TunedBucky/RIMG0185.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0185.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       52.9 K 13 May 2008 - 21:15   Main.gert   Lower arm, view towards tower
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0187.jpg](/i/TunedBucky/RIMG0187.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0187.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       56.3 K 13 May 2008 - 21:16   Main.gert   Main arm, view towards tower
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0188.jpg](/i/TunedBucky/RIMG0188.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0188.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       27.2 K 13 May 2008 - 21:18   Main.gert   Main arm w/ tower vanishing point
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0189.jpg](/i/TunedBucky/RIMG0189.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0189.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       52.7 K 13 May 2008 - 21:18   Main.gert   Upper arm, view towards tower
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0191.jpg](/i/TunedBucky/RIMG0191.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0191.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       52.0 K 13 May 2008 - 21:19   Main.gert   View from church, possible PA & lab position
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0192.jpg](/i/TunedBucky/RIMG0192.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0192.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       84.7 K 13 May 2008 - 21:20   Main.gert   St. Mariens
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [RIMG0195.jpg](/i/TunedBucky/RIMG0195.jpg)   [manage](http://web.fm/twiki/bin/attach/Fmext/TunedBucky?filename=RIMG0195.jpg&revInfo=1 "change, update, previous revisions, move, delete...")       59.6 K 13 May 2008 - 21:21   Main.gert   skeleton of Palast der Republik on the opposite
:::

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Tuned%20*Bucky%5B%5EA-Za-z%5D)

Revision r1.1 - 16 May 2008 - 10:11 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/TunedBucky) \|
[More](http://web.fm/twiki/bin/oops/Fmext/TunedBucky?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.TunedBucky)


