

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwDebugReview1**** \${
[**Edit**](/_/EwDebugReview1?t=1469037843)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE**\
Reviewed by: DE:BUG\
Date: \~ 1998\
Additional Link:
[http://www.de-bug.de/](http://www.de-bug.de/) \

------------------------------------------------------------------------

Wurde von Pita ja schon letzten Monat eingehend gewuerdigt, muss an
dieser Stelle aber noch einmal Erwaehnung finden. Auch wenn man es sich
waehrend der einleitenden, sehr ruhigen 10 Minuten kaum vorzustellen
vermag, erreichen FM auf Explorers_We eine Informationsdichte, die der
von Drum\'n\'Bass zumindest ebenbuertig ist, aber natuerlich auf einem
ganz anderen, naemlich geraeuschartigen Level funktioniert. Ist der
Anfang noch ruhig und vom Gefuehl her so wie sich eines sonnigen
Spaetnachmittags auf einem Boots- anleger luemmelnd den
Geraeuschfragmenten der Stadt hinzu- geben, die vom Wind daher getragen
werden, geht es anschliessend ab auf die Achterbahn und man merkt
schnell, dass die minuetlich gesetzten Start-ID\'s Sinn machen. Mit
schier unglaublichem Tempo tragen sich Veraenderungen zu, so dass es
einem den Boden unter den Fuessen wegreisst. Das ist wild und erinnert
stellenweise an Stock, Hausen and Walkman, wenn auch der Aufbau
weitestgehend homogener gehalten ist. Als Bonus liegt noch eine 50
minuetige Live-CD mit einem Mitschnitt ihrer letztjaehrigen Perforamnce
auf dem Mego-Boot und einem schlicht \"indoor\" benannten Auftritt bei.
Auch hier wird deutlich, dass FM einfach die Standards setzen. Genau so
muss Atonalitaet heute grooven. Hart ohne weh zu tun oder anstrengend zu
sein und die Grenzen zwischen Praezision und Zufall verschwimmen
lassend. Warum die Firma Apple Farmers Manual nicht an vorderster Front
ihrer \"Think different\" Kampagne anfuehrt, ist mindestens so
unverstaendlich wie Bill Gates\' Versaeumnis das Internet zu kaufen.

Can you stand the FM-Test?

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Debug%20*Review%20*1%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwDebugReview1) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwDebugReview1?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwDebugReview1)


