<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

# Barely Alive

[http://barely.a.live.fm](http://barely.a.live.fm)  is a site focussed on Pdwiki:PureData and Pdwiki:OpenSoundControl. it features a lot of hand-selected links to other relevant sites, as well as some custom externals for free download.

Revision r1.1 - 05 Jul 2005 - 13:17
