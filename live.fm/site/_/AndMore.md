<post-metadata>
  <post-title>text 3</post-title>
  <post-date>2007-01-04</post-date>
  <post-tags>test, fm, text</post-tags>
</post-metadata>

# text 3

physical reality, as modeled by many temporally local theories, seems to
be fundamentally linked to a zoo of oscillation processes. these
processes in a way help synthesizing our experiences, be it sonic
events, phenomena of light, sight at distance, sight of enclosed spaces,
wireless communication, plain matter and much more. as indicated by the
order given, the sonic poses a very accessible and rewarding way to
explore wave phenomena but this is not where it must end, to say the
least of hearing range.

engineering activities have uncovered the electromagnetic within certain
limits, or to particular purposes although to a great extent in
disregard or disinterest of secondary effects on physiology and
consciousness, in other words, the medium is not neutral and on its own
commences sending messages of unintended content to unintended
receivers.

what is left is the establishement of a musical use of a broader band of
waves, both regarding frequency range and media.

a system regulating frequency, duration, event-clustering and
arbitrarily more parameters is at least a musical system. in a
stochastic system this means choice of probabilities, in a
process-coupled system this means filling the system with life by choice
of governing events.

Revision r1.1 - 04 Jan 2007 - 11:48 -
