<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaPhosphorReview**** \${
[**Edit**](/_/RlaPhosphorReview?t=1469037740)
}}+-C67

### []{#26082003_Phosphor_Magazine} 26082003 Phosphor Magazine

Farmers Manual: DVD

This unusual DVD starts off by offering something quite promising with
interesting snippets of noise distortion sound and visual computer
graphics for approximately half a minute. However this led into a video
piece showing the groups efforts to break through the wall of their
studio. Simple repetitive synth sounds play in the background as they
discuss the various ways of destroying the wall. Amusing and all as it
is to watch a group of boys smashing their way through a wall as part of
their renovations, the sense of such a DVD offering is hard to
appreciate. Once they have broken through the wall they celebrate with a
bottle of champagne and after wandering through the newly adjoined rooms
they relax and play with the cat. The packaging and opening of this DVD
show that this group is potentially interesting. A little more incite
would have been good.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Phosphor%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaPhosphorReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaPhosphorReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaPhosphorReview)


