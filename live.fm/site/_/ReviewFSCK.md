<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FSCK](/_/FSCK) \>
**ReviewFSCK**** \${
[**Edit**](/_/ReviewFSCK?t=1469037909)
}}+-C67

### []{#fsck_reviews} fsck reviews

------------------------------------------------------------------------

Review of: [FSCK](/_/FSCK)\
Reviewed by: ennui (USA)\
Date: \~ 1998\

------------------------------------------------------------------------

\"Farmers manual is perhaps one of the strangest and most original
projects to be released in some time. Ultra-synthetic sounds and drum
\'n\' bass rhythms are altered beyond belief, creating a hybrid of
jungle that at times crosses over to noise. To be clear, let me state
that while drum & bass influences are there, the album is in no way
danceable, and it only borders on listenable the first time you hear it.
Incredibly crazy, Farmers Manual will really appeal to fans of
experimental mauic more than techno fans. fsck has to be heard to be
believed. Recommended.\" Rating: 8.5

------------------------------------------------------------------------

Improjazz (France)\
Date: \~ 1998\

------------------------------------------------------------------------

\"C\'est clair : entre le minimalisme technoide des nordique Panasonic
et les ruptures d\'Aphex Twin et Squraepusher, les résidents du label
autrichien Mego Farmers Manual ne choisissent pas, préférant aux aussi
l\'experimentation électronique tous azimuts. A tel point qu\'ils ont
choisi, pas facile, d\'explorer méthodiquement les abstractions générées
par les dérèglements digitaux de l\'univers machinesques pour élaborer
leur jungle si particulière, mixage de résonances cosmiques aux drones
extatiques avec des crêtes énervées aux relents drum \'n\' bass.
Qu\'ajouter sinon que sollicitant la lecture aléatoire de la touche
\"random play\" de votre combi hi-fi, l\'hallucinant \"FSCK\" ajoute à
son écoute une lecture rhizomique inspirée par le \"Mille Plateaux\" du
duo Deleuze/Guattari. Autant dire de quoi danser cérèbral!.\" (Philippe
Robert)

------------------------------------------------------------------------

Reviewer: Manu HOLTERBACH\
Date: \~ 1998\

------------------------------------------------------------------------

Aux frontieres de la techno, parmis ceux qui savent etre captivant, il y
a, entre autres, Oval, Panasonic ou Squarepusher. Il faudra desormais
compter, semblerait-il, avec FARMERS MANUAL. Ils sont Autrichiens, mais
la pochette de ce disque ne vous racontera que peu de choses si ce
n\'est leur interet pour Ies crashs de vieux avions de la derniere
guerre. Ils ne proposent aucune information supplementaire, si ce n\'est
une invitation a les rencontrer au www.farmersmanual.co.at/fm/fsck. Ce
qui les rend particulierement passionnant, c\'est l\'intelligence avec
laquelle ils utilisent leurs materiaux. Leur musique, evoquant un
univers de machine a l\'interieur duquel on assisterait a un total
dereglement, est faite de ruptures, d\'accidents electroniques, de
disruptions, de virus digitaux, de propositions rythmiques, jungle ou
autres vouees a une destruction, a une deconstruction systematique. Et
puis il y a la structure de ce disque, I\'enchainement des morceaux qui
ressemble a un jeu de piste, a une proposition. D\'emblee le lecteur CD
affiche 99 morceaux, ce qui n\'est pas courant\... En fait les 15
premiers sont \"normaux\". Apres, chaque segment offre trois secondes de
craquements electroniques aux variatons imperceptibles, jusqu\'au
quatre-vingt-dix-neuvieme qui reprend une duree \"habituelle\". Sans que
ce soit noté nulle part, il me semble comprendre que là il y a une
proposition de lecture aléatoire. Et effiectivement, ca fonctionne. Les
morceaux semblent avoir été construits à cette fin. Ainsi ils respirent,
prennent de l\'espace. La touche \"random play\" existe sur presque tous
les lecteurs et les FARMERS MANUAL semblent l\'avoir pris en compte. Et
par cette lecture, ce disque prend un intérêt encore plus grand, car
dans ce cas, sa forme est ouverte. Et l\`écoute qu\`on peut en avoir ne
cesse de varier puisque même si on peut reconnaître à force les
\"morceaux\", leur enchaînement reste hasardeux. Conservant ainsi à la
musique de FARMERS MANUAL, son instabilité savoureuse percue aux
premières écoutes. On croit assister aux bégaiements d\'une machinerie
autonome et détraquée. Totalement inhumaine. \"FSCK\" doit être lu, il
me semble, dans cette structure ryzhomique, et les FARMERS
[MANllAL[^?^](/_/MANllAL?topicparent=Fmext.ReviewFSCK)]{.twikiNewLink
style="background : #5f2e20;"} sont par là bien plus proche des idées
évoquées dans \"Mille Plateaux\" de Deleuze et Gattari, sans s\'en
réclamer à l\'instar d\'Oval qui n\'applique cette idée de complexité
qu\'à la structure de leur morceaux et non pas à l\'ensemble de leurs
disques. Chez FARMERS MANUAL la prise en compte des relations qui
existent entre les musiciens, le médium, I\'objetdisque/le support, le
lecteur CD et l\'auditeur est complète. Résultat, voilà un disque
passionnant, une forme d\'approche de la musique plutôt nouvelle. Plus
aucune trace de ce romantique geste physique qui produit du son. Pas
même de complaisance dans un rythme qui pourrait faire danser, non,
juste la radicalité d\'une structure sonore quasi autonome. Il y a là
aussi quelque chose de trés cérébral, comme la facon dont le cerveau
doit fonctionner à un haut niveau d\'abstraction. Une excellente
découverte.

------------------------------------------------------------------------

Reviewer: sean cooper for ELECTRO, (US)\
Date: \~ 1997\

------------------------------------------------------------------------

It\'s perhaps redundant to say that Farmer\'s Manual, one of the most
active of the Austrian Mego label\'s stable of artists, is just a bit
off. Mego have made an identity campaign out of offness, with snide,
off-kilter emaciations of electronica raising mistakes and studio
tongue-slips to the level of technique. While maybe not the weirdest of
Mego\'s artists, Farmers Manual have been most fastidious in their
attacks on discreet genres, roping in electro, techno, ambient, and,
with much of fsck, drum\'n\'bass for pissy, dirt-smeared reworkings that
have, apparently, never heard the word \"sacred.\"

It\'s probably misleading to mentiongenre at all, however, since fsck
(like the group\'s other recordings) bears really only a superficial
resemblance to the focus of its lampoons: this music comes literally
nowhere near either the dancefloor or the laboratory, ditching both
lycra and whitecoat for a naked run through suburban shopping malls or
luxury car dealerships. In other words, it doesn\'t make much musical
sense at all, which is also part of its appeal; fsck, while not Mego
product proper (it was released on Ash International sublabel Tray), is
just as wily and perturbed as the group\'s debut, No Backup, and will
appeal to fans of that recording regardless of the fact the two albums\'
points of departure lie often chasms apart. Oh, and the last 82 tracks
(most capping at 2-3 ,seconds) resemble something like the dying throws
of a beyond-useless dot matrix printer, with the final five minutes a
skipping choke of fuzz and line noise.

Par for the course, then. Rating: 7.5

------------------------------------------------------------------------

Reviewer: VITAL by \[FdW\], (The Netherlands)\
Date: \~ 1997\

------------------------------------------------------------------------

\"This is the second release by Tray and again we welcome Farmers
Manual. The first 12\" was a weird drum & bass track backed by a
likewise weird hard-chill track. Indexed with 99 points this is an
extension of that 12\". Weird tracks, owing in some ways to d&b as much
as it does to industrial music. If I say a lot of this sounds fucked,
believe me this is truly fucked. The more accessible (if that is an
appropiate word in this context) are in the beginning of the CD, but it
gradually grows noiser and weirder throughout. The noisy bits and pieces
at the end are suitable for random play or looping mode. Again fun for
home-DJs (as you would be thrown at outof the club if you play this on
the dance floor) and bedroom composers. Industrial music for the digital
generation!\"
([FdW[^?^](/_/FdW?topicparent=Fmext.ReviewFSCK)]{.twikiNewLink
style="background : #5f2e20;"})

------------------------------------------------------------------------

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Review%20*FSCK%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/ReviewFSCK) \|
[More](http://web.fm/twiki/bin/oops/Fmext/ReviewFSCK?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.ReviewFSCK)


