<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmEsoterica](/_/FmEsoterica)
\> **NextLevelArticle**** \${
[**Edit**](/_/NextLevelArticle?t=1469037730)
}}+-C67

(originally published in dwukropek magazine, 2002)

(originally published in dwukropek magazine, 2002)

##### []{#farmersmanual_Take_Futurism_to_t} farmersmanual Take Futurism to the Next Level

They\'ve been called \"media art at its most anarchistic\" and \"pop
music for the year 4000\": Who are farmersmanual and what do they want
with your IP packet headers?

##### []{#The_Next_Step_to_Attaining_Cult} []{#The_Next_Step_to_Attaining_Cult_} The Next Step to Attaining Cult Status

This fall Farmers Manual, the formerly Austria-based electronic-music
and multimedia collective, take their uniquely bent vision of The Order
of Things one step further.

The imminent release of their DVD-Video/ROM will only serve to add to
their outré appeal, because they\'re essentially giving away something
for free. Like the Grateful Dead (the ultimate cult band), who not only
allowed their fans to tape their shows, but even encouraged them by
reserving the best section of the concert hall for those who wanted to
do so, FM are not only issuing all of their extant live recordings ?
over 100 hours\' worth! ? in the form of a DVD, but will be making all
of the individual concerts available for download from their website.
This gives rise to another parallel with the Dead: since more of the
video material will be offered over the Net than on the DVD itself, one
can foresee a time when hardcore FM fans will be sending files back and
forth to each other in much the same way that Deadheads still exchange
tapes of \"Tampa \'71\" for \"Tacoma \'82\".

Busted!

Message: 24

Date
:   Thu, 16 Mar 2000 23:30:03 -0500

From
:   XXX

Subject

my cd player was sabotaged by farmers manual

I was doing my regular half-assed remix of farmers manual (for fun) and
now my beat transmission software is busted. what can i do to fix it.
this has never happened to me. does anyone know anything about this
program?

##### []{#History_and_Disinformation} History and Disinformation

The story apparently begins when some first-year students get kicked out
of the prestigious Institut für elektroakustik, experimentelle und
angewandte Musik, part of Vienna\'s renowned Universität für Musik und
darstellende Kunst (\"If we had known ahead of time that they were going
to apply attendance as a criterion, we would have gone to class.\") and
then join up with fellow dropouts from other visual arts programs. They
spend years holed up in a former motorcycle garage on the edge of
Vienna\'s 5th District, kept company only by a cat named \"Wurst\"
(Austrian dialect for \"ma?e piwo\", i.e. \"no big deal\"), later to be
joined by mysterious characters from the local underground techno scene
who go by names like \"Dextro\", \"Pure\", and \"Glow\".

Late one night, while in the latter stages of a \"group biochemical
experiment\", the lads are listening to techno demigod Jeff Mills\'
\"Live at the Liquid Room, Tokyo\". As Mills\' loop from \"Untitled B\"
repeats over and over, the secret underlying message eventually takes
shape in the mind: \"Here\'s the laptop, Here\'s the laptop\...\" The
very next day, as though by command, they purchase their first
Powerbooks and pledge to abandon their primitive analogue ways and lead
an all-digital life. Soon thereafter, they change the name of the group
to its present form, \"farmersmanual\" ? one word, no space, no initial
capitals ? \"for full Internet compatability\".

Also, around this time, FM take their cue from Daniël Baranoff-Rossiné
(1888-1942), Dada sculptor and painter, who developed a synaesthetic
piano (the \"piano optophonique\") which projected a different colour
onto a screen for each note. Convinced that the visual and auditory
parts of the brain are capable of working together in peace and harmony,
FM begin applying these advances to their live performances.

##### []{#Chaos_Theory_in_Action} Chaos Theory in Action

As late as 1998 in group interviews, farmersmanual were still openly
disagreeing as to whether \"it\'s all improvised\" or not, and likewise
whether their sounds are mostly generated or collected. To this day
there has never been any clear consensus as to the number of members
actually in the group at any given time. To add to the confusion, the
four compatriots who all hail from the \".at\" web domain recently added
a fifth, from \".au\". (cf. the common American question: \"Austria ?
isn\'t that where they have the koala bears?\") As to his role in FM,
this Australian only enigmatically explains, \"I help people connect
things.\"

There are persistent rumours that a second Australian has recently
joined. If in eight years FM has gone from conventional rock instruments
to a full-on techno setup to laptop trio, and in turn added a full-time
video artist/web designer and professional Net expert, one shudders to
think what this latest person\'s function might be.

Since \"Farmers Manual\" has thus far been used as a group name, track
title, and album title, the name and concept seem to transcend the
individual members. The explanation offered by veteran British
noise-improv group AMM about their own \"name\" might well also apply to
FM ? \"It was there a few minutes before we thought of it.\"

Perhaps, in order to avoid an overly didactic interpretation of the name
\"farmersmanual\", instead of \"instructions for use\", one should think
along the lines of \"serving suggestions\".

Moreover, they\'ve stated their objective in a live situation as being
the setting up of a laboratory or workshop situation to be experienced
jointly by the group members and the audience. The word \"installation\"
is a useful reminder for those who fail to be thrilled by a
\"performance\" which consists of four or five people staring into
computer screens for several hours at a time.

##### []{#An_Ordinary_Evening_with_farmers} An Ordinary Evening with farmersmanual

The following report from \"konfrontationen \'99 ? 20th international
festival for jazz & improved music\" in Nickelsdorf, Austria was posted
on \"The Wire\" mailing list:

\[\...\] 2 a.m. On the left side of stage, a screen with a projected
text (\"I never used it.\"); at right, four chairs and a long table
holding a bank of laptops and terminals. After many minutes of a
Throbbing Gristle-type throb, the quartet of ostensible computer jocks
finally files onstage. Visuals evolve into heavily Russian Futurist and
Constructivist motifs, numerals (cf. Charles Demuth\'s \"I Saw the
Number Five in Gold\"), maybe buildings, maybe railroad cars. The
equivalent of shortwave numbers stations for the eyes, an eerie,
unsettling hyper-technological vibe for the ears; on the level of the
Hafler Trio\'s best. In attempting to determine whether the rhythms of
the visuals are coinciding with the rhythms of the cracklings and the
shifting drone patterns, the mind is lured along some previously
uncharted perceptual interface. The audience begin to behave strangely:
people get up to leave, two trip in otherwise adequately lighted areas;
bottles clink; someone drops a glass; some begin to eat compulsively or
fidget; a drunk starts hollering. In the next 20 minutes, at least ten
people will fall asleep. Words on the screen constantly morph into new
ones, then the visuals return to images which include (1) strips of
film, or are they really insect chromosomes; (2) a highly treated human
face, maybe casually taken from a hidden-camera site, morphing slo-mo
into a mask of agony, like Munch\'s famous angst-face as the impact of
the nuclear blast hits; (3) motifs seemingly from the band\'s
\"Explorers_We\" web page; (4) colorful Czech industrial/commercial
typography from the 1920s, like from a matchbox or carton of detergent,
going by too quickly for the mind to retain; and (5) the German phrases
\"don\'t smoke poisonous tobacco \... don\'t drink poisonous alcohol\".
When red and black dominate the screen, the atmosphere is
non-threatening (cf. Kraftwerk\'s \"Man-Machine\"), but when purple
\"gothic\" colors appear, the audience usually somehow reacts badly. A
slow but steady stream of departures accompanies the industrial noise
and fleeting orange abstract visuals which shift back to
black-and-white. Occasional heckling: The Bespectacled Geekish Member
(with a really sharp 5-o\'clock-shadow goatee!) either heckles back or
confidently flashes the Wa??sa sign of victory. The Autistic-Looking
Member often lights up cigarettes \-- according to one rumour, this
usually means that the new subroutine or software or maybe entire
operating system that he\'s been perfecting is working, so he can relax
(he\'s been known to devise new patches even while onstage during group
performances). The Tall Goofy-Looking Member occasionally slips offstage
to confirm that the video projections are working properly. At times The
One With Bad Skin seems to have a hand in both visuals and music, and
confers with The Bespectacled Member. All intermittently crack open soft
drinks, pull new cigarettes out of packs, eyes glued to VDTs. The music
settles down into a loop: the members leave the stage: no applause. Loop
continues, but with an occasional glitch where it\'s interrupted and
replaced by a different one, then back to the first. Punters continue to
file out, oblivious to the fact that the interruptions that redirect the
program to a different loop are becoming more frequent (the band is
probably utilizing a weighted random operation here). When the loop
glitches reach a critical point, the music suddenly changes. Members
return to stage. After their return to approximately twenty audience
members, the visuals often tear down the fourth wall, displaying one of
the computer screens, and the various pointing-and-clicking is visible
for all to see. Hints of a live feed or stream somehow involved. The
music now seems to have begun as samples ? but what from? This is jazz
improvisation as reconstructed by some future civilization or alien
life-form, keeping the spontaneity and fun, but sound-wise reminiscent
of David Tudor, Merzbow, even zoviet\*france. Always instantaneous
variety. After another hour, as pre-dawn deep blue skylight comes
through the open portions of the courtyard roof, the music world\'s
answer to a hacker convention quietly adjourns. Tedium factor: \"Query
undefined \-- check your settings.\" Drug of choice: black Afghan hash,
Hawaiian mushrooms, Isostar®

And the above report elicited a humble response from an FM member: \> it
was the machines. we done no thing.\
\> just victims in a vacuum\

##### []{#Other_Artistic_Pranks_and_Provoc} Other Artistic Pranks and Provocations

At the 2001 Venice Bienniale, for two days FM cruised between Giardini
and S.Zaccaria in their \"Ship of Fools\", bombarding, bewildering, and
bemusing passing boats and onshore pedestrians with strange sounds that
could have come from beneath the water itself, and at night, adding
projections to their self-described \"esoteric exercise in mobile
computing\".

This year, at the Biennale Internationale Arte Giovani in Torino, they
took their abstruse machinations to a whole new level ? by monitoring
the Internet traffic to and from the cybercafé in the same building, and
then using that data to generate the music and visuals for their
concert. Essentially, the audience in the hall was experiencing a pure
\"sonification\" and \"visualization\" of the information contained in
the headers produced by Internet activity (i.e. websurfing, email) to
and from the building ? namely, the Internet protocol numbers, length of
packets, and time stamps.

##### []{#Music_as_Information_and_Vice_Ve} Music as Information and Vice Versa

In the 1970s world of dub and reggae, when King Tubby\'s records
included the sound of tape rewinding, to what extent did this reference
the rest of the music? After all, the listener can hear a portion of the
tune played at high speed, backwards. Does this sound constitute music,
or carry information in any other way?

Similarly, is FM presenting music, or information? Which conceptions of
music and of information are presupposed in the question?

Their music thus produces an unsettling quality not only on the basis of
sensory stimuli, but on epistemological grounds: it\'s not just the
frequent visceral sense of foreboding, like that moment on the
roller-coaster just before it drops and accelerates, but the listener\'s
never being completely sure when the various telegraphic beepings and/or
rude rushes of digital noise constitute music, and what input is causing
it. In the meantime, the FM people continue to talk to their machines,
the machines talk to each other, the machines talk back to the people,
and sometimes the people even talk to each other.

And in Torino this view of \"information\" culminated in their new
approach. No longer satisfied with the random processes which produced
their last release, \"Explorers_We\" (which at the time was called \"the
Sgt. Pepper\" and \"first masterpiece\" of this particular subgenre), FM
has now taken the hard line: If a series of digits conveys any
information whatsoever, then strictly speaking, it is no longer
\"random\".

##### []{#Perpetuum_Mobile} Perpetuum Mobile

At their best, FM present both the glee and the glitz of a carousel ?
which, come to think of it, also constitutes a machine, was also
technologically showy in its own time, can also be set into motion
fairly indefinitely, and is also something which must be experienced
live rather than via recording.

This machinic aspect also extends to the farmersmanual CDs, which often
lead a dual existence as both musical recordings and little machines. At
one point, for instance, fsck, their second full release, offers 150
seconds of a harmless noise loop (\"dzz \... dz-dzzt!\"), divided into
73 tracks. The result? When the CD is played on shuffle, it\'s as though
a harmless mechanized mosquito has been inserted into your CD tray, or
even somehow programmed right into the player.

Why the fascination with machines and processes set in motion like
automatons? Is it just cybernetics? Well, we are after all in Central
Europe; these Golem-like things are expected to occur now and then.

As for human intervention and authorship, what matters are the
parameters, the criteria, and the presets. Mark Sinker has written, \"As
lo-fi electronics improvisor Gordon Mumma once said, \'I consider that
my designing and building of circuits is really \"composing\".\' The
Xenakis-blueprint for the Doomsday Composing Machine, the global
computer-synth with presets for order and chaos, choice and chance,
interwove scores as maps with circuits as maps with recordings as
maps\...\"

##### []{#Fun_Facts_to_Know_and_Tell} Fun Facts to Know and Tell

This year, the ever-trendsetting Ars Electronica festival awarded the FM
founding member who calls himself the \"ostructor\" with an Honorable
Mention in the category for best computer music. The \"artist\" listed
for the CD in question is \"pxp\"; its title is \"while(p){print\".\",\"
\"x\$p++}\".

When they tour Japan, FM often hang out with Polish expatriate composer
Zbigniew Karkowski, perhaps the severest of them all!!

One FM member confesses that for the better part of a year, his morning
\"wake-up music\" was Kurt Schwitters\' \"Ursonate\", that famous Dada
masterpiece of poetical nonsense.

Each day by email each member of FM receives: a \"number of the day\"
(usually 10 digits to the left of the decimal and 5 digits to the
right); a \"word of the day\" (recent examples include: protowziarwblt;
schblsch.iøep; verrömkrömk); and a \"gargoyle of the day\" which is a
random nonsequitur utterance (e.g. \"not every problem can be solved
with salad\") produced by a \"virtual papier-maché gargoyle\" in a
secret game room somewhere on the Internet.

The personal websites of two of them include links to the Surrealist
Compliment Generator, the Dada Engine, and various sites related to
[OuLiPo[^?^](/_/OuLiPo?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} and \'pataphysics!

Several of them are reputed to be accomplished amateur mycologists!

##### []{#All_the_Specs} All the Specs

\"Off the top your head, could you tell me some of the software that you
use?\" \"Oh no! You\'re not really going to write about that stuff, are
you?!\" \"But someone reading the article will want to know, and that\'s
the only information they\'ll want.\" \"Well OK, but can I at least send
it to you later in electronic form? I really don\'t want to think about
this.\"

\[later:\] \>Applications (Mac OS 9): Absynth 1.0.2; Acrobat Reader 4.0;
Adaptec Jam 3.0; BBEdit 6.0; Digital Performer 3.0;
[DiskWarrior[^?^](/_/DiskWarrior?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}®; Fetch 4.0.2; Final Cut Pro;
[FreeMIDI[^?^](/_/FreeMIDI?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"};
[GraphicConverter[^?^](/_/GraphicConverter?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} US PPC; iTunes;
[MacArmyKnife[^?^](/_/MacArmyKnife?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}; MOTU Audio/AudioDesk?;
niftytelnet-1.1-ssh-r3; Nord Modular; OMS; Peak? DV 3.0; Peak? TDM 2.5;
[PulsarGenerator2001[^?^](/_/PulsarGenerator2001?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}.demo;
[ReCycle[^?^](/_/ReCycle?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}; Reel-Eyes?; SC2.2.11f; SH8888_PPC;
[SonicWorx[^?^](/_/SonicWorx?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} PBundle 2.0.5; sonicWORX Studio 1.51?;
[SoundAppPPC[^?^](/_/SoundAppPPC?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} 2.7.2;
[SoundMaker[^?^](/_/SoundMaker?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} 1.0.3; SPARKle; Super
[ResEdit[^?^](/_/ResEdit?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"} 2.4; Web Retriever 2.0b3 PPC. Note: this
list does not include algorithms like Granular Synthesis, which are
included with various software.

\[still later\] \"What about
[ProTools[^?^](/_/ProTools?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}?\" \"Everyone starts out with
[ProTools[^?^](/_/ProTools?topicparent=Fmext.NextLevelArticle)]{.twikiNewLink
style="background : #5f2e20;"}. It\'s still on my machine only because I
never bothered to take it off.\"

##### []{#Looking_to_the_Mysterious_East} Looking to the Mysterious East

As far as intellectual influences are concerned, FM seem to hold a
serious appreciation for a certain strain of culture in the Slavic
world, as can be seen by the reverence given to Lem, Tarkovsky, and the
Strugatskys, as well as contemporary Slovenian \"Noordung Zero Gravity
Art\".

And as the best hint to understanding the group\'s enterprise in
general, one member expresses admiration for the early Soviet \"cinema
train\". These were production facilities which would travel via rail in
order to film documentaries of large construction and industrial
projects; then while on site, the film crew would also develop and edit
the film specifically for the purpose of screening it on the train
itself to the workers, for the edification and inspiration of all
involved.

One regrets that FM didn\'t follow the technology all the way through to
the vision of Stalin, who ordered the development of a giant projector
called \"The Rocket\": it was to be the size of a railway car, with film
reels the size of six-story buildings, and capable of projecting films
of didactic worth onto passing clouds.

Or maybe that\'s the next step for FM.

? John Wójtowicz

some relevant web pages:

-   [http://web.fm/releases/](http://web.fm/releases/) 
-   [http://snudd.sil.at/fm_exp/](http://snudd.sil.at/fm_exp/) 

Farmers Manual recordings may be ordered from: www.mdos.at

re the upcoming release, check with: www.mego.at

-   [http://www.avantofestival.com/](http://www.avantofestival.com/) ,
    2001
-   [http://www.ptuch.ru/muznews.html](http://www.ptuch.ru/muznews.html) 
-   now strewn among Vienna, Berlin, Hamburg, Brussels, and various
    points Down Under.
-   from the Internet; information deleted to protect anonymity.
-   Burham Tufail, \"The noise of art?\", N01SE (Kettle\'s Yard, London,
    2000)
-   [http://groups.yahoo.com/group/thewire/](http://groups.yahoo.com/group/thewire/) ,
    Digest No.436, report credited to \"Alex Scheibus\"
-   Note the reference to p.414 of Gravity\'s Rainbow.
-   [http://www.wfmu.org/\~kennyg/popular/articles/glitchwerks.html](http://www.wfmu.org/~kennyg/popular/articles/glitchwerks.html) 
-   Except that as any chaos theorist can tell you, a carousel would be
    classified as a \"point attractor\", whereas the FM multimedia
    machine in action qualifies as a \"strange attractor\".
-   Mark Sinker, \"The Rise and Sprawl of Horrible Noise\" at
    [http://web.pitas.com/tashpile/noise2.html](http://web.pitas.com/tashpile/noise2.html) 

see:

-   [http://noordung.telekom.si/ang/grav_zero.htm](http://noordung.telekom.si/ang/grav_zero.htm) .
-   [http://www.rezoweb.com/forum/technologie/aaaroskoforum/97.shtml](http://www.rezoweb.com/forum/technologie/aaaroskoforum/97.shtml) 

[http://www.bushparty.com/htms/ironcurtain02.htm](http://www.bushparty.com/htms/ironcurtain02.htm) 

\- -

Friday September 20, 2002

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Next%20*Level%20*Article%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:18 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/NextLevelArticle) \|
[More](http://web.fm/twiki/bin/oops/Fmext/NextLevelArticle?template=oopsmore&param1=1.2&param2=1.2)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.NextLevelArticle)


