<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmReleases](/_/FmReleases)
\>
[RecentLiveArchive](/_/RecentLiveArchive)
\>
[RlaFeedBack](/_/RlaFeedBack)
\> **RlaFunbroxDotCom**** \${
[**Edit**](/_/RlaFunbroxDotCom?t=1469037744)
}}+-C67

Farmers Manual -
[RLA](/_/RLA) label: Mego \|
style: experimental

------------------------------------------------------------------------

Info: DVD-Video/ROM, MEGO 777, 2003

------------------------------------------------------------------------

3 days, 21 hours, 38 minutes and 3 seconds. That is the total playing
time of the new Farmers Manual release on Mego. The release comes as a
hybrid DVD, in a lovely big DVD box. The Box contains every locatable
live recording by Farmers Manual since 1995. You must excuse me for not
having heard each single live recording on this DVD\... So over 3 days
of music, all live recordings since 1995, plus more it says in the promo
letter. So curious as I was, I put the disc in my player and pushed the
play button and waited. All I got to see was a short film, about 15
minutes long, showing three guys making an extra door in their studio.
No matter how hard I tried, no menu, nothing but this movie.
Disappointed and confused I concluded it was a DVD rom, that should be
used in a DVD rom player, one that I don\'t have(the DVD players in your
computer will play the DVD fine).\
But nothing was lost, all the content of the DVD is mirrored on a
website, so the 3 days Farmers Manual live experience was not lost after
all. So what can you expect? A total overload of live recordings, giving
you an amazing amount of Farmers Manual music. But in the sheer quantity
of the music lies the problem of this DVD. It\'s simply too much. It\'s
virtually impossible to hear everything and remember what concert was
what. But this negative side isn\'t all that negative. Use the DVD when
ever you feel like hearing some Farmers Manual, but don\'t feel like
playing that album you\'ve heard so many times already. Simply select a
live recording you, and enjoy. You won\'t be disapointed.\
This DVD is a must have for Farmers Manual fans, and for everyone that
enjoyed a Farmers Manual concert at any point. A great initiative by
Mego, and an almost endless source of great music (if you like Farmers
Manual that is of course).

This cd was reviewed by
[BvG[^?^](/_/BvG?topicparent=Fmext.RlaFunbroxDotCom)]{.twikiNewLink
style="background : #5f2e20;"}.

Posted on 26-5-2003.

[http://www.funprox.com/judgment/review.asp?show=514](http://www.funprox.com/judgment/review.asp?show=514) 

\-- [HiazHhzz](http://web.fm/twiki/bin/view/Fm/HiazHhzz) -
03 Jul 2003

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Rla%20*Funbrox%20*Dot%20*Com%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/RlaFunbroxDotCom) \|
[More](http://web.fm/twiki/bin/oops/Fmext/RlaFunbroxDotCom?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.RlaFunbroxDotCom)


