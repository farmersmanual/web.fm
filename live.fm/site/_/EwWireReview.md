

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\>
[EwFeedback](/_/EwFeedback)
\> **EwWireReview**** \${
[**Edit**](/_/EwWireReview?t=1469037845)
}}+-C67

------------------------------------------------------------------------

Review of: **EXPLORERS_WE** Reviewed by: John Everall for The Wire (UK)
Date: june or july 98

------------------------------------------------------------------------

\"Austrian soundscapers Farmers Manual have evolved a sound of such
delirious intensity and refreshing originality that listeners are not so
much surfing on sinewaves as plunged into a state of total immersion.
Few of their contemporaries are able to achieve such a potent seduction:
Pan Sonic and
[ELpH[^?^](/_/ELpH?topicparent=Fmext.EwWireReview)]{.twikiNewLink
style="background : #5f2e20;"}, perhaps, or even early Boyd Rice. If
cerebral aridity is generally perceived as a defining characteristic of
computer generated sound, then Farmers Manual provide incontrovertible
evidence to the contrary. Seeing them live recently I was struck by the
tremendous physical presence of their sound. Explorers_We demonstrates
they can pull the same thing off in the studio. A single hour-long track
is divided into 60 PQ start points, and listening in shuffle mode is
recommended. What emerges is a montage of fractured, febrile rhythms
intercut with bursts of searing noise and disembodied voices, which fuse
into a rich and radical sonology. The first 1000 copies contain a second
CD of live recordings, containing two lengthy pieces that further
illustrate Farmers Manual\'s idiosyncratic methodology. Their throbbing,
pulsing, crackling structures are possessed of an intoxicating, and at
times almost incoherent, power that testifies to a creativity far id
advance of the over-hyped doyens of new Electronica.\" (John Everall)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Ew%20*Wire%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/EwWireReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/EwWireReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.EwWireReview)


