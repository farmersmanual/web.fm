

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **TouchSampler3Review**** \${
[**Edit**](/_/TouchSampler3Review?t=1469037741)
}}+-C67

------------------------------------------------------------------------

Review of: VARIOUS \| Touch Sampler.3 (Touch), CD\
Reviewed by: ASG, Aaron\
Date: \~ 1998\

------------------------------------------------------------------------

Installation number three for the Touch gallery presents 27 new
\'tracks\' of everything from various (listed and unlisted) field
recordings to odd \'sounds\' to historical and scientific content
channeled from Touch Radio to various instrumental percussive samples
(can you say marimba?) to band rehearsals to birds chirping to exclusive
full-length \'songs\' by the likes of Biosphere, Farmers Manual,
Panasonic, Rehberg & Bauer and Scala. In essence, a motley array of
sound \-- be it music or not. Even Geir Jenssen, the (one and only)
mastermind behind the cool, ambient waves of the Biosphere machine,
provides one of his most experimental pieces to date with a track called
Knives in Hens \-- a stuttery, rough slurry of grey noise, rainy waves
and deep-sea pulsation that eventually give in (but not entirely) to the
soothing sounds of beautiful strings. While it may have found a home on
Substrata (Geir\'s latest work of pure brilliance and one of 97\'s
finest ambient moments), Knives serves a perfect interlude amongst the
bizarre field recordings and other head-scratchingly twisted snippets of
sound. Exclusive tracks from the other artists listed seem to follow
form as the robotic bass throbbing of Panasonic (who now call themselves
Pan Sonic after 4+ years of using the popular electronic manufacturer\'s
name and logo) pumps and circulates to a destination unknown much like
anything from Vakio or Kulma. And, of course, that zany Austrian
contingent (Rehberg & Bauer, Farmers Manual) are up to their usual
tricks of time-manipulated sampling and regurgitated sound frequencies.
Wrapping up one of the wildest collections of the year 98 (thus far) is
a lovely vocal track by Scala (entitled Fuser) \-- a spacey,
string-laden, hypnotic tune with some truly incorrigible lyrics, namely
\"It\'s nice to be objectified / You never know, you might like it.\"
Without question, a compilation that will change every time you listen
to it.

------------------------------------------------------------------------

Review of: VARIOUS \| Touch Sampler.3 (Touch), CD\
Rviewed by: Sound Projector (UK)\
Date: \~ 1998\

------------------------------------------------------------------------

\"Terrific compilation of aural exotica-philes and sonic scientists
associated with Mr. Mike Harding\'s Touch Label, here\'s a stimulating
combination of musical and environmental recordings, all spliced
together with strange and interesting fragments in between. It\'s a
library of exotic and unusual documents, a species of aural voyeurism.
Quite simply, these sounds are amazing; they could reawaken even the
most entropic braindead moron to the wonders and mysteries of the
world - so many things which we simply take for granted or overlook,
capable of producing such astonishing sound events. One of my personal
favourites is the environmental taping here of the British Library round
reading room, a document that comes with an added dimension of sadness
since this particular feature of British life, history and heritage is
now a thing of the past. So it is meet that someone had the foresight to
add this to our collective archive of memories. This is the end piece
from a tripartite collage by Jon Wozencroft, starting with the old man
in his attic finding a \'convertor\', leading to the found poetry of a
language school lesson from the airwaves.

The unassuming Chris Watson is here with three minutes of atmospheric
recording from Zambesi, filled with sunlight and recommended listening
to start the day with; he later resurfaces with \'Demonic Laughter\'
courtesy of a lively magpie jay. Chris has travelled extensively on
account of his career in cinema production; the unadulterated recordings
he fetches back from his adventures are, strictly speaking, almost
peripheral to his purpose, yet they amount to more than a taped diary -
their utter vividness makes the listener into a traveller too. Likewise,
the two central segments of ethnic music on this disc - one heavenly
episode of Temple Gamelan music (recorded in 1983), and six tracks of
African music from the Bagamoyo Group of Tanzania, recorded (at Holland
Park in London) in 1984. Given the currently hep status of this strain
of \'World Music\' just now, you\'d be foolish to pass up a listen to
these irresistible rhythms, and the deeply pleasing sound of the
9-string iseze here.

After the African solace the CD goes wild - entering a noisy, dirty
chaos zone as embodied by the near- incoherence of Rehberg and Bauer,
Farmers Manual and Bruce Gilbert\'s horrifying \'Voice\' cut-ups. Truly,
these are manifestations of electronic glossolalia from possessed
spirits. These are spliced either side of Joe Banks performing
Disinformation \'live\' from an event at the Museum of of Installation,
a heavy bass drone in all probability generated by a National Grid.

Linking fragments are possibly taken from domestic objects, familiar
everyday items going mad before our very ears. The TV set (obviously)
beams out strange messages; the refrigerator hums ominously or
comfortingly. This latter sound phenomenon has been noticed by Masami
Akita as \'interesting\' that people have recently discovered to be
music, and by Robert Crumb as a potent reminder of one\'s mortality in
his Existentialist one-page comic strip. I\'d be disappointed to learn
its not a fridge at all, so let me cherish my illusions. This
domesticity-subverted factor extends to the old retired couple clearing
out the attic (see above), a similar document not heard since Alvaro
recorded his German wife Hildegard reciting her recipe while baking
brown bread, as a filler for side two of his second LP.

Unofficial title for this comp has to be \'Teleform\' - analysed to mean
something new, Tele = \'recording at a distance\' and Form = \'having
the shape of\'. These pieces were all recorded from the margins of life,
by quiet and unassuming artists observing the miracles of life from the
borderlines. This mix carefully selects items that display the most
worrisome and alarming emotions alongside the most reassuring and
relaxing, with very little in between. Touch releases are among the
gentlest and least aggressive in the world, surely a welcome balm to the
torrent of banal MTV-styled youth culture that is increasingly becoming
inescapable.\"

The World is so full of a number of things\...I\'m sure we ahould all be
happy as Kings.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Touch%20*Sampler%20*3Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/TouchSampler3Review) \|
[More](http://web.fm/twiki/bin/oops/Fmext/TouchSampler3Review?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.TouchSampler3Review)


