<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **ReviewTestOne**** \${
[**Edit**](/_/ReviewTestOne?t=1469037863)
}}+-C67

### []{#Test_One_Sheffield} []{#Test_One_Sheffield_} Test One, Sheffield:

\"In a perfect illustration of Virilio\'s \'law of least action\',
Viennese Powerbookworms Farmers Manual are spitting their fidgety noises
over the churning pistons and hissing convection currents of a 1905
River Don steam engine, high as a terraced house. Every hour, the 25,000
horsepower engine cranks into action, foaming and whirring the Farmers
do little more than prod their keyboards and idly scratch their noses,
but volume for volume, the grease monster can\'t compete.\"

\"The Wire\", June 1998 (issue 172)

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Review%20*Test%20*One%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/ReviewTestOne) \|
[More](http://web.fm/twiki/bin/oops/Fmext/ReviewTestOne?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.ReviewTestOne)


