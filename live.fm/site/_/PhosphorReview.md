<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>


[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **PhosphorReview**** \${
[**Edit**](/_/PhosphorReview?t=1469037864)
}}+-C67

#### []{#20030518_phosphor_review_of_RAND} 20030518: phosphor review of RANDOM.X on 20030514

\[\...\] The most known band of the evening Farmers Manual played next.
That was rather obvious, because the volume went up right away. This duo
presented a wall of noise and cut-up rhythms. The rhythms are hammering,
pounding quite fast and in the higher domain. Screaming tones, cutting
beats, sudden dhort breaks, fragmented bits of vocals are the main
ingredients.

The music is extreme, full of activity and energy. Staccato structures
with sudden breaks and explostion of sounds are Farmer Manual\'s key
issues this evening.

They released a DVD on Mego just recently. \[\...\]

[http://www.xs4all.nl/\~phosphor](http://www.xs4all.nl/~phosphor) 

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Phosphor%20*Review%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/PhosphorReview) \|
[More](http://web.fm/twiki/bin/oops/Fmext/PhosphorReview?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.PhosphorReview)


