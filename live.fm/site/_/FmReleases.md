

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****FmReleases**** \${
[**Edit**](/_/FmReleases?t=1469037732)
}}+-C67

### []{#fm_releases} fm releases

#### []{#standalone_full} standalone / full

  **title**                                                                                       **format**                                                       **label/cat#**                                               **year**
  ----------------------------------------------------------------------------------------------- ---------------------------------------------------------------- ------------------------------------------------------------ ----------
  [RecentLiveArchive](/_/RecentLiveArchive) aka rla   hybrid DVD ROM and [website](http://rla.web.fm)    [mego 777](http://www.mego.at/mego777.html)    2003
  [ExplorersWe](/_/ExplorersWe)                       CDDA                                                             OR squish 4                                                  1998
  [FsCk](/_/FsCk)                                     CDDA                                                             tray slump 8                                                 1997
  [DoesNotCompute](/_/DoesNotCompute)                 VINYL EP                                                         tray                                                         1997
  [NoBackup](/_/NoBackup)                             CD+                                                              mego 008                                                     1996
  [fm](/_/Fm)                                         VINYL                                                            mego 017                                                     1996

#### []{#available_online} available online

-   from the
    [iTunesStore](/_/ITunesStore)
-   [RecentLiveArchive](/_/RecentLiveArchive)
    -\> [website](http://rla.web.fm) 

#### []{#compilations} compilations

mostly exclusive (yet, as of this writing) tracks marked by \*

-   melvins remix 2002?
-   touch :: ringtones 2001 /
    [Tone14](http://www.touch.demon.co.uk/releases/Tone14.html),
    [local](http://web.fm/twiki/bin/view/00_releases/2001_touch_ringtones/)
-   \*avanto 2001 compilation / www.avantofestival.com / 2001
-   \*garage-g 2001 compilation / www.garage-g.de / 2001
-   [OrMinidiscCompilation](/_/OrMinidiscCompilation)
    / OR / 2003
-   \*ORsome computermusic ii compilation / 2001
-   \*teleform T_0 compilation / touch / 2000
-   \*nature is perverse, live recording / fylkingen / 1999-2000

```{=html}
<!-- -->
```
-   \*communication breakdown compilation / vibragun / 2000
-   \*subrosa underwood compilation / subrosa / 199 8/9?
-   \*sluts\'n\'strings rmx / cheap / 1999 ?
-   \*marteau de verre compilation/marteau de ferre / 1999?
-   \*david kristian rmx / alien8 / 1999
-   \*gnusic ii compilation / gnusic (unreleased yet) / 1999

```{=html}
<!-- -->
```
-   \*people like us remix / 1998/9?
-   \*chillout compilation (fmvsjl) / subetage / 1998?
-   \*five compilation/sabotage / 1997 ?
-   \*institut f. feinmotorik compi / 199x / iff
-   \*loops2 loop-vinyl serotonin (usa)
-   viennatone compilation K7 (de)

```{=html}
<!-- -->
```
-   alt.frequencies compilation worm_interface (uk)

#### []{#video} video

-   the mego videos / mego 023v / 96-98
-   franzensgasse druchbruch doku
-   fm on tour in 199x? [FMrla02.mov](http://web.fm/rla/FMrla02.mov)
-   fm in paris: [fm in paris / centre georges
    pompidou](http://web.fm/rla/paris-lo.mov)
-   fm in venice: [venice short qt sorensen
    small](http://web.fm/rla/venice_short_qt-sor_small.mov)
-   stockholm soft evidence

#### []{#pr_nt} pr!nt

-   timing zero, japanese grafics pseudo regular
-   five magazine / subetage / 1997
-   72dpi anime book (by die gestalten, berlin, 2001)
-   schirn catalogue (2002)

#### []{#related} related

-   gcttctat
-   pxp
-   cd_slopper

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Fm%20*Releases%5B%5EA-Za-z%5D)

Revision r1.1 - 26 Aug 2008 - 02:28 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/FmReleases) \|
[More](http://web.fm/twiki/bin/oops/Fmext/FmReleases?template=oopsmore&param1=1.8&param2=1.8)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.FmReleases)


