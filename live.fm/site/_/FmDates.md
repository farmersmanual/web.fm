

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

****FmDates**** \${
[**Edit**](/_/FmDates?t=1469037733)
}}+-C67

::: twikiToc
-   [OBSOLETED](/_/FmDates#OBSOLETED)
    -   -   -   [upcoming
                events](/_/FmDates#upcoming_events)
            -   [eventlog](/_/FmDates#eventlog)
:::

# []{#OBSOLETED} OBSOLETED

please check here:
[http://rla.web.fm/twiki-bin/view/Rla/EventIndex](http://rla.web.fm/twiki-bin/view/Rla/EventIndex) 

#### []{#upcoming_events} upcoming events

%CALENDAR{year=\"2004\" month=\"7\" showweekdayheaders=\"1\"}%
%CALENDAR{year=\"2004\" month=\"8\" showweekdayheaders=\"1\"}%
%CALENDAR{year=\"2004\" month=\"9\" showweekdayheaders=\"1\"}%

------------------------------------------------------------------------

#### []{#eventlog} eventlog

rather go to see
[rla](/_/RecentLiveArchive).

-   26 Jan 2005 -
    [PrutalDruth](http://web.fm/twiki/bin/view/Xdv/PrutalDruth)
-   11 Feb 2005 -
    [FmAtCRASHsymp2005](/_/FmAtCRASHsymp2005)
-   25 Nov 2004 - 26 Nov 2004 -
    [FmAtUltrasound2004[^?^](/_/FmAtUltrasound2004?topicparent=Fmext.FmDates)]{.twikiNewLink
    style="background : #5f2e20;"}, [ultrasound
    festival](http://www.ultrasound.ws) , huddersfield, UK
-   27 Nov 2004 -
    [FmAtSensoryClinic[^?^](/_/FmAtSensoryClinic?topicparent=Fmext.FmDates)]{.twikiNewLink
    style="background : #5f2e20;"}, UK
-   20 Sep 2004 -
    [FmAtInmerso2004](/_/FmAtInmerso2004),
    ciudad de mexico, mexico
-   7 Jul 2004 -
    [PrutalDruth](http://web.fm/twiki/bin/view/Xdv/PrutalDruth),
    pd meating in berlin
-   9 Jun 2004 -
    [PdMeatingBerlin[^?^](/_/PdMeatingBerlin?topicparent=Fmext.FmDates)]{.twikiNewLink
    style="background : #5f2e20;"}
-   13 May 2004 - 16 May 2004 - moving patterns - Bukaresti
-   8 May 2004 - 12 May 2004 - basics festival - Salzburg
-   26 Apr 2004 - 3 May 2004 - birmingham -\> Walsall
-   14 Apr 2004 - 15 Apr 2004 -
    [FmEvtJaunaMuzika2004](/_/FmEvtJaunaMuzika2004),
    jauna muzika festival - Vilnius
-   28 Mar 2004 - 18 Apr 2004 -
    [Pinklight](http://pinklight.net)  Exhibition, Vienna
-   20 Feb 2004 -
    [ZbigniewKarkowski](/_/ZbigniewKarkowski)
    and
    [EmanuelFrakt](/_/EmanuelFrakt)
    at
    [AuslandBerlin](/_/AuslandBerlin)
-   16 Feb 2004 - 17 Feb 2004 - gent: workshop
-   14 Feb 2004 - gent, art center vooruit
-   18 Oct 2003 - livedigitally / singapore / phuture //
-   14 May 2003 - fm.brln touch down at heeresbaeckerei among others for
    RANDOM.X
-   16 Apr 2003 - 20 Apr 2003 -
    [RlaReleaseParty](/_/RlaReleaseParty).
    location and lineup tba.
-   12 Apr 2003 - fm appearance at f0.am closing event
-   4 Apr 2003 - 6 Apr 2003 - fm tabledance at
    [http://www.alltomorrowsparties.co.uk](http://www.alltomorrowsparties.co.uk) ,
    south of england
-   3 Apr 2003 - fm sonicarts / ICA london -
    [CutnSplice[^?^](/_/CutnSplice?topicparent=Fmext.FmDates)]{.twikiNewLink
    style="background : #5f2e20;"}
-   16 Jan 2003 - fm live at Le Mus�e d\'Art moderne et contemporain de
    Strasbourg
-   30 Dec 2002 - v93r / pxp at
    [ZwischenDenTagen[^?^](/_/ZwischenDenTagen?topicparent=Fmext.FmDates)]{.twikiNewLink
    style="background : #5f2e20;"}
    ([http://www.zwischendentagen.de](http://www.zwischendentagen.de) )
-   30 Nov 2002 - v93r / -z / pxp / +x at NBI
    ([http://www.neue-berliner-initiative.de](http://www.neue-berliner-initiative.de) )
    at
    [UnderscanNite](/_/UnderscanNite)
-   10 Nov 2002 - farmersmanual at NBI
    ([http://www.neue-berliner-initiative.de](http://www.neue-berliner-initiative.de) ).
    also playing: kent
-   3 Oct 2002 - dsp meet-in at Pdwiki:PrutalDruth

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Fm%20*Dates%5B%5EA-Za-z%5D)

Revision r1.1 - 15 Jan 2007 - 22:27 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/FmDates) \|
[More](http://web.fm/twiki/bin/oops/Fmext/FmDates?template=oopsmore&param1=1.19&param2=1.19)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.FmDates)


