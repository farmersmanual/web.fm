

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\>
[GracefulCrystallisation](/_/GracefulCrystallisation)
\> **FeeDoc**** \${
[**Edit**](/_/FeeDoc?t=1469037899)
}}+-C67

### []{#setup_for_the_abstraction_now_st} setup for the abstraction now stuff:

#### []{#fanman} fanman

-   gsrv2: 5151

```{=html}
<!-- -->
```
-   OSC_data.pl
-   OSC_lkp.pl
-   OSC_chatbot.pl

```{=html}
<!-- -->
```
-   system_stats

#### []{#ost} ost

-   packetsniffer
-   urlsniffer
-   system_stats

#### []{#axoih} axoih

-   weather, fetches weather info from
    -   williams field (antarctica)
    -   adelaide, darwin, alice springs (aualia)
    -   graz/wien (austria)
    -   bruxelles (belgium)
    -   tegel, tempelhof (berlin, germany)

data is:

temperature, humidity, windspeed, winddirection, dew_point, locationcode
(empty), altitude (empty)

#### []{#cerf} cerf

-   x,y,z accel

------------------------------------------------------------------------

### []{#usage} usage

open dumpOSC on 5251

send / to 192.168.0.30 (fanman) and see what returns.

after that you choose your messages:

/sub,myportnumber(e.g. 5251),/message/name

if you say

/sub,5251,/\*

you ll get all messages sent.

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Fee%20*Doc%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/FeeDoc) \|
[More](http://web.fm/twiki/bin/oops/Fmext/FeeDoc?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.FeeDoc)


