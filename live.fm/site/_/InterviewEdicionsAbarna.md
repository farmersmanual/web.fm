<post-metadata>
  <post-title> </post-title>
  <post-date>2000-01-01</post-date>
  <post-tags> </post-tags>
</post-metadata>

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmArchive](/_/FmArchive)
\> **InterviewEdicionsAbarna**** \${
[**Edit**](/_/InterviewEdicionsAbarna?t=1469037862)
}}+-C67

### []{#Edicions_Abarna_S_C_P_2} []{#Edicions_Abarna_S_C_P_2_} Edicions_Abarna_S.C.P..2 /


    > 1*typical one:vienna scene (it is supposed to be a very fruitful and
    > growing scene but most of the viennese artists are quite esceptic about
    > it...). can you picture it?

    there is a bunch of people intensively involved with (mostly) sound, but
    also visuals, "art", digital media-technology etc... though it seems to be
    divided into
    different subscenes and for us/fm we re located on the more
    technology/internet oriented side of it all
    its not surprising anyhow that viewed from "outside", vienna scene appears
    to be a hippiesque, idea-sharing and each other fertilizing
    "all-together-community", but geographical coexistence doesnt mean
    ideal/ideell coexstnce.

    >
    > 2*since the very first fm record there has been a quite obvious
    > transgression of musical genres. something like a musical anti-purism.

    technically a move from hybrid analogue/digital production to
    almost entire digtal production, exploration of more sophisticated
    software, and loads of additional external media inputted


    >
    > 3*you have released stuff in mego, tray, touch, and now or. different but
    > quite close labels. do you feel part of this kind of "scene"? what about
    > sub rosa? (there's a track on the last compilation... )

    hm, tray-touch-or is in fact almost the same labels, there are sligfhtly
    different concepts behind each, but no organisational differences...
    sub_rosa: its just one track, no longlasting obligations yet with them...
    anyway, i think we do feel quite "at home" within the touch/mego way of
    understanding and presenting sounds/ideas, though of course our ideas
    arent 100% overlaid and there's times we d have decided different on how
    doing this or that, nevertheless i doubt there's many other labels around
    which woulkd fit fm-way of making music that much, except our "virtual"
    own label, ... ;)

    >
    > 4*i think mathias is involved in skot. is there any other parallel project
    > (musical or not) besides fm?
    > any links/connection in your plastic world and music world to comment?

    jes, mathias is about half of skot in fact
    and jes again, there are quite a few other parallel projects going on but
    its the same as it was with farmers initially, when the project was already
    runnning for some time without any "label" or name until the first release
    on mego was due so we had to htink some up..

    generally all these projects orbit around sound-optix-video/film,
    future-media, advanced computer aided design of the former and networking
    in all aspects...
    we re also seeking to work within a larger international network of
    people concerned with similar problems...

    >
    > 5*anonimity thing. when u conatcted mego for th firts time i think you send
    > tapes without any info about you. in the electronica potent festival you
    > only plugged the computer. comment it.

    we met the mego-crowd in a viennese club and started talking. so in fact
    there was some pre-info for those tapes we sent them...
    and anyway, the sound is THE info, if you know how to decode it, obviously
    they knew, though the code is probably ambiguous and can attain different
    meanings...

    its just not always necessary to have humans involved within a musical
    performance. when we do play "ourselves" we just decide on when a change
    in params occurs, but we have no control over how exactly it is changed.
    so if you randomize/automatize the time structure theres nothing left to
    do.

    >
    > 6*sonar concert, have u planned it yet.

    no

    >
    > 7*filiations/connectios that you feel towards other musiscians.

    je, totally. and not only musicians but generally the more radical
    and original participants in the global idea-market... ;)
    but i dont start the effort involved with an exact listing here...

    >
    > 6*future plans

    sampling god (as pita does already), a gig in hell, a journey to
    another galaxy and total immersion into cyberspace

    > more ultra-short-not-very-peace-disturbing questions:
    >
    > ****...names of each FM member. I got some info but i'm sure it's wrong
    > cause its handwrited and your names are qute unusual in spanien.

    http://www.sra.at/sr/bands/991.htm
    this should do...

    >
    > ***** what can you tell me about this "virtual" label?

    the will of putting out records against the financial/organisational
    disabiltiy of doing it
    further: the ability to publish music on the web without the
    productional overheads (cd pressing, print, filling, distribution, etc...)
    against
    the not existing acceptance of web-music by the "audience"
    due to overprized/insufficient connectivity on the client-side
    and connectivity correlating to sound-quality

    blrz. 

Edicions_Abarna_S_C\_..

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Interview%20*Edicions%20*Abarna%5B%5EA-Za-z%5D)

Revision r1.1 - 05 Jul 2005 - 13:17 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\|
[Attach](http://web.fm/twiki/bin/attach/Fmext/InterviewEdicionsAbarna)
\|
[More](http://web.fm/twiki/bin/oops/Fmext/InterviewEdicionsAbarna?template=oopsmore&param1=1.1&param2=1.1)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.InterviewEdicionsAbarna)


