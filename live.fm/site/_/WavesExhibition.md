

[![fm
home](/i/HiazHhzz/2F-pole-logo5.gif)](http://web.fm)

**[FmProjects](/_/FmProjects)
\> **WavesExhibition**** \${
[**Edit**](/_/WavesExhibition?t=1469037733)
}}+-C67

# []{#WavesExhibition} [WavesExhibition](/_/WavesExhibition)

![fm_riga_gcp.jpg](/i/WavesExhibition/fm_riga_gcp.jpg){width="398"
height="710"}

::: twikiToc
-   [WavesExhibition](/_/WavesExhibition#WavesExhibition)
    -   [catalogue
        text](/_/WavesExhibition#catalogue_text)
        -   [author(s)
            name(s)](/_/WavesExhibition#author_s_name_s)
        -   [precise title of the
            work](/_/WavesExhibition#precise_title_of_the_work)
        -   [short bio (about 150
            words)](/_/WavesExhibition#short_bio_about_150_words)
        -   [year of the
            work](/_/WavesExhibition#year_of_the_work)
        -   [description of the work (about 300
            words)](/_/WavesExhibition#description_of_the_work_about_30)
        -   [2-3 pictures of print quality of your
            work](/_/WavesExhibition#2_3_pictures_of_print_quality_of)
    -   [More
        Data](/_/WavesExhibition#More_Data)
:::

## []{#catalogue_text} catalogue text

### []{#author_s_name_s} []{#author_s_name_s_} author(s) name(s)

Farmersmanual

### []{#precise_title_of_the_work} precise title of the work

**title** Elektrosmogfreien (participation in the will and law of the
creator)

**subtitle** a study on statistical properties of multivariate
measurement series

### []{#short_bio_about_150_words} []{#short_bio_about_150_words_} short bio (about 150 words)

short bio (about 150 words) Farmersmanual is a distinguished,
pan-European, multisensory disturbance conglomerate. musical and
technological instruments for improvisation, network visualization and
sonification. performances and remote collaboration and forms of
documentation that prolong the idea of openness and reinterpretation.
continuously expanding. TOTAL AUTOMATION vs. HUMAN INTERACTION. seeking
to shift the local atmosphere from dissolution and clumsiness through
manual change and ecstatic fiddling into an imaginative state of complex
monotony, structured calm and chill, or endless associative babbling. so
that towards the end the authors become the audience and the audience
can be confronted with a stage populated by machines infinitely
rendering a stream of slight semantic madness. with the help of extreme
frequencies and distorted, flickering images. 3. Extracting a shadow
from the skeleton of network flow. A layer of technological reality
usually hidden becomes accessible through mediation into sound and
visual flicker. Erratically shifting from chittering machines into the
human realm. Replay in different Farmersmanual. multisensory disturbance
communication, electromagnetism, nuclear fusion and ecstatic ification,
tools for near instantaneous endless possible through local atmospace
delay. It is thetic and politich cant global Scalishifting from
chittext? the cause of all polycause. towards the echo of the
hypopulated only which move away from frequencies and stage created at
the end. the title for the fractures. linear shifts of the localing wave
(about 300 words) name(s)

### []{#year_of_the_work} year of the work

2006 eV {Anno Mung 3172, Season of Confusion}

### []{#description_of_the_work_about_30} description of the work (about 300 words)

Simply, farmersmanual proposes to answer the following question, as
posed by Hartmut Müller.

Hartmut Müller in 1982 postulated the existience of a universal
non-einsteinian gravitational standing wave along which nodes and
antinodes the universe is being structured. that is for example the
distribution of matter in cosmic space or the discrete set of sizes that
bio-organisms assume, the temporal distribution of events in radioactive
decay. We want to look at specific aspects of these phenomena:

Its has been suggested that spatially or contextuallly separated
processes show structural statistical similarities. Accordingly, even
physical noise sources are subject to periodic fluctuations. We will
track a set of different data sources, both global and local, prior to
and during the event and analyze the collected data with respect to
empiric evaluation of the proposed thesis.

The analytic process will in parts be visible and audible. Individual
assessment by members of the audience will be a valuable contribution to
the experiment. Samples can be taken home.

\"In the context of Global Scaling Theory the hypothesis of the Big Bang
appears in a new light. Not a propagating shock wave (pressure wave) in
linear space (the echo of the hypothetical primeval explosion) is the
cause of cosmic microwave background radiation, but a standing pressure
wave in logarithmic space. It is also responsible for the fractal and
logarithmic scale-invariant distribution of matter in the entire
universe. It created the universe as we know it and recreates it
continually. It is the cause of all physical interactions and forces --
gravitation, electromagnetism, nuclear fusion and nuclear decay. It is
the cause of topological 3-dimensionality of linear space, of
left-right-asymmetry, as well as of anisotropy of time. All of these
phenomena are physical effects which arise at the transition from
logarithmic into linear space. The standing wave in logarithmic space
now allows us to communicate across astronomical distances practically
without time delay. How is this possible?\"

\[http://217.160.88.14/ir_en_research_global_scaling/detail.php?nr=1110&kategorie=ir_en_research_global_scaling\]

global scaling overview -\>
[http://www.globalscalingtheory.com/](http://www.globalscalingtheory.com/) 

global scaling communication \[in german\] -\>
[http://www.info.global-scaling-verein.de/Global-Scaling/Documents/GlobalScalingCommunication.pdf](http://www.info.global-scaling-verein.de/Global-Scaling/Documents/GlobalScalingCommunication.pdf) 

fm.projects -\>
[http://web.fm/twiki-bin/view/Fmext/FmProjects](http://web.fm/twiki-bin/view/Fmext/FmProjects) 

### []{#2_3_pictures_of_print_quality_of} 2-3 pictures of print quality of your work

-   [histo_pingtimes.eps](/i/WavesExhibition/histo_pingtimes.eps) :
    Histo Pingtimes

```{=html}
<!-- -->
```
-   [histo_weather.eps](/i/WavesExhibition/histo_weather.eps) :
    Histo Weather

```{=html}
<!-- -->
```
-   [histo_photo.eps](/i/WavesExhibition/histo_photo.eps) :
    Histo Photoresistor

## []{#More_Data} More Data

-   [http://noosphere.princeton.edu/](http://noosphere.princeton.edu/) 
-   [http://www.hcrs.at/NOISE.HTM](http://www.hcrs.at/NOISE.HTM) 
-   [http://www.hcrs.at/ZYKLEN.HTM](http://www.hcrs.at/ZYKLEN.HTM) 
-   [http://arxiv.org/find/grp_q-bio,grp_cs,grp_physics,grp_math,grp_nlin/1/au:+shnoll/0/1/0/all/0/1](http://arxiv.org/find/grp_q-bio,grp_cs,grp_physics,grp_math,grp_nlin/1/au:+shnoll/0/1/0/all/0/1) 

::: twikiAttachments
  **[Attachment](http://web.fm/twiki/bin/view/TWiki/FileAttachment)**                                                                                                     **Action**                                                                                                                                                      **Size** **Date**              **Who**                                                                                                                                      **Comment**
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------- --------------------- -------------------------------------------------------------------------------------------------------------------------------------------- ---------------------
  ![](http://web.fm/twiki/pub/icn/else.gif){width="16" height="16" align="top" border="0"} [histo_pingtimes.eps](/i/WavesExhibition/histo_pingtimes.eps)   [manage](http://web.fm/twiki/bin/attach/Fmext/WavesExhibition?filename=histo_pingtimes.eps&revInfo=1 "change, update, previous revisions, move, delete...")       22.6 K 15 Jun 2006 - 12:06   [JuanChanson](http://web.fm/twiki/bin/view/Main/JuanChanson)                                                                     Histo Pingtimes
  ![](http://web.fm/twiki/pub/icn/else.gif){width="16" height="16" align="top" border="0"} [histo_weather.eps](/i/WavesExhibition/histo_weather.eps)       [manage](http://web.fm/twiki/bin/attach/Fmext/WavesExhibition?filename=histo_weather.eps&revInfo=1 "change, update, previous revisions, move, delete...")         15.7 K 15 Jun 2006 - 12:07   [JuanChanson](http://web.fm/twiki/bin/view/Main/JuanChanson)                                                                     Histo Weather
  ![](http://web.fm/twiki/pub/icn/else.gif){width="16" height="16" align="top" border="0"} [histo_photo.eps](/i/WavesExhibition/histo_photo.eps)           [manage](http://web.fm/twiki/bin/attach/Fmext/WavesExhibition?filename=histo_photo.eps&revInfo=1 "change, update, previous revisions, move, delete...")          109.2 K 16 Jun 2006 - 10:51   [JuanChanson](http://web.fm/twiki/bin/view/Main/JuanChanson)                                                                     Histo Photoresistor
  ![](http://web.fm/twiki/pub/icn/bmp.gif){width="16" height="16" align="top" border="0"} [fm_riga_gcp.jpg](/i/WavesExhibition/fm_riga_gcp.jpg)            [manage](http://web.fm/twiki/bin/attach/Fmext/WavesExhibition?filename=fm_riga_gcp.jpg&revInfo=1 "change, update, previous revisions, move, delete...")           70.8 K 01 Sep 2006 - 13:07   [HogSmith[^?^](http://web.fm/twiki/bin/edit/Main/HogSmith?topicparent=Fmext.WavesExhibition)]{.twikiNewLink style="background : #5f2e20;"}    
:::

[Ref-By](http://web.fm/twiki/bin/search/Fmext/SearchResult?scope=text&regex=on&search=Waves%20*Exhibition%5B%5EA-Za-z%5D)

Revision r1.1 - 16 Feb 2007 - 15:30 -
[TWikiGuest](http://web.fm/twiki/bin/view/Main/TWikiGuest)\
\| [Attach](http://web.fm/twiki/bin/attach/Fmext/WavesExhibition) \|
[More](http://web.fm/twiki/bin/oops/Fmext/WavesExhibition?template=oopsmore&param1=1.8&param2=1.8)\



(mailto:admin@web.fm?subject=TWiki%20Feedback%20on%20Fmext.WavesExhibition)


